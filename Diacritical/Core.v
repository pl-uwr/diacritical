(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains a generic definition of diacritical progress      *)
(* instantiated, e.g. in Definition 3.3.                                *)
(************************************************************************)

Require Import Utf8.
Require Import List.

(** Relations *)

Definition DiRel (E : Type) : Type :=
  E → Prop.

(** Functions on relations *)

Definition DiFunc (E : Type) : Type :=
  DiRel E → DiRel E.

(** Relation inclusion *)

Definition rel_sub {E} (R S : DiRel E) : Prop :=
  ∀ e, R e → S e.

Notation "R ⊆ S" := (rel_sub R S) (at level 70).

(** Function inclusion *)

Definition fun_sub {E} (f g : DiFunc E) : Prop :=
  ∀ R, f R ⊆ g R.

Notation "f (⊆) g" := (fun_sub f g) (at level 70).

(** Monotone functions *)

Class Monotone {E : Type} (f : DiFunc E) : Prop :=
  monotone : ∀ R S, R ⊆ S → f R ⊆ f S.

(** Continuous functions *)

Class Continuous {E : Type} (f : DiFunc E) : Prop :=
  continuous : ∀ R e,
    f R e → ∃ l : list E, Forall R l ∧ f (λ e', In e' l) e.

Arguments monotone   {E} f {_} [R S].
Arguments continuous {E} f {_} [R e].

(** A generic notion of diacritical progress. *)

Class DiElemProgressCore (E : Type) : Type :=
  { elem_progress : DiRel E → DiRel E → DiRel E }.

Class DiElemProgress (E : Type) {EP : DiElemProgressCore E} : Prop :=
  { elem_progress_monotone : ∀ Q₁ Q₂ S₁ S₂,
      Q₁ ⊆ Q₂ → S₁ ⊆ S₂ →
      elem_progress Q₁ S₁ ⊆ elem_progress Q₂ S₂
  }.

Record progress {E : Type} {EP : DiElemProgressCore E} (R Q S : DiRel E) :=
  { progress_sub_pas : R ⊆ Q
  ; progress_sub_act : R ⊆ S
  ; progress_rel     : R ⊆ elem_progress Q S
  }.

Arguments progress_sub_pas {E EP R Q S} _ [e].
Arguments progress_sub_act {E EP R Q S} _ [e].
Arguments progress_rel     {E EP R Q S} _ [e].

Notation "R ↣ Q & S" := (progress R Q S) (at level 70).

(** A generic notion of diacritical simulation. *)

Definition DiSimulation {E : Type} {EP : DiElemProgressCore E}
  (R : DiRel E) : Prop :=
  R ↣ R & R.

(** A generic notion of diacritical evolution and strong evolution. *)

Class DiEvolution {E : Type} {EP : DiElemProgressCore E}
  (f g h : DiFunc E) : Prop :=
  evolution : ∀ R S : DiRel E, R ↣ R & S → f R ↣ g R & h S.

Class DiStrongEvolution {E : Type} {EP : DiElemProgressCore E}
  (f g h : DiFunc E) : Prop :=
  strong_evolution : ∀ R Q S : DiRel E, R ↣ Q & S → f R ↣ g Q & h S.

Notation "f ↝ g & h" := (DiEvolution f g h) (at level 70).
Notation "f !↝ g & h" := (DiStrongEvolution f g h) (at level 70).

(** Basic properties of relation and function inclusion. *)

Require Import RelationClasses.

Section Properties.
Context {E : Type}.

Global Instance Reflexive_rel_sub : Reflexive (rel_sub (E:=E)).
Proof.
  intros R e; auto.
Qed.

Global Instance Transitive_rel_sub : Transitive (rel_sub (E:=E)).
Proof.
  intros R Q S; unfold rel_sub; auto.
Qed.

Global Instance Reflexive_fun_sub : Reflexive (fun_sub (E:=E)).
Proof.
  intros f R e; auto.
Qed.

Global Instance Transitive_fun_sub : Transitive (fun_sub (E:=E)).
Proof.
  intros f g h; unfold fun_sub, rel_sub; auto.
Qed.

End Properties.