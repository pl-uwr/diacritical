(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file gathers a number of useful Coq list related devices.       *)
(************************************************************************)

Require Import Utf8.

Inductive list_member {X : Type} : list X → Type :=
| LM_Here  : ∀ x xs, list_member (x :: xs)
| LM_There : ∀ x xs, list_member xs → list_member (x :: xs).

Fixpoint get_member {X : Type} {xs : list X} (M : list_member xs) : X :=
  match M with
  | LM_Here x _    => x
  | LM_There _ _ M => get_member M
  end.

Class ListMember {X : Type} (x : X) (xs : list X) : Type :=
  { listMember    : list_member xs
  ; listMember_ok : get_member listMember = x }.

Instance ListMember_here {X : Type} (x : X) (xs : list X) :
    ListMember x (x :: xs) :=
    {listMember := LM_Here _ _}.
Proof.
  reflexivity.
Defined.

Instance ListMember_there {X : Type} (x y : X) (xs : list X)
    {LM : ListMember x xs} : ListMember x (y :: xs) :=
    {listMember := LM_There _ _ listMember}.
Proof.
apply listMember_ok.
Defined.

Lemma list_member_append {X : Type} {xs ys : list X}
  (M : list_member (xs ++ ys)) :
  ListMember (get_member M) xs + ListMember (get_member M) ys.
Proof.
  induction xs; simpl.
  + right; exists M; reflexivity.
  + simpl in M; dependent inversion M as [ | z zs N ]; simpl.
    - left; apply ListMember_here.
    - specialize (IHxs N); destruct IHxs.
      * left; apply ListMember_there; assumption.
      * right; assumption.
Qed.

Lemma list_member_append_r {X : Type} xs {ys : list X}
  (M : list_member ys) :
  ListMember (get_member M) (xs ++ ys).
Proof.
  induction xs; simpl.
  + exists M; reflexivity.
  + apply ListMember_there; assumption.
Qed.

Class ListForall {X : Type} (F : X → Prop) (xs : list X) : Prop :=
  ListForall_elim : ∀ M : list_member xs, F (get_member M).

Instance ListForall_nil {X : Type} (F : X → Prop) : ListForall F nil.
Proof.
  intros M; inversion M.
Qed.

Instance ListForall_cons {X : Type} (F : X → Prop) (x : X) (xs : list X)
  {Fx : F x} {LF : ListForall F xs} : ListForall F (x :: xs).
Proof.
  intro M.
  dependent inversion M; simpl.
  + assumption.
  + apply LF.
Qed.

Lemma ListForall_elim_append_l {X : Type} {F : X → Prop} {xs ys : list X} :
  ListForall F (xs ++ ys) → ListForall F xs.
Proof.
  induction xs; simpl; intro L.
  + apply ListForall_nil.
  + apply ListForall_cons.
    - apply (L (LM_Here _ _)).
    - apply IHxs; intro M.
      apply (L (LM_There _ _ M)).
Qed.

Lemma ListForall_elim_append_r {X : Type} {F : X → Prop} {xs ys : list X} :
  ListForall F (xs ++ ys) → ListForall F ys.
Proof.
  induction xs; simpl; intro L.
  + assumption.
  + apply IHxs; intro M.
    apply (L (LM_There _ _ M)).
Qed.