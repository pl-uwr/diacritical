## Diacritical progress and compatibility

This directory contains a formalization of the notions related to
diacritical progress and compatibility, developed in [3], and recalled
in Section 3.2. The crucial ingredients are:

* The operations on functions are formalized in
  [Operations.v](Operations.v), [OperationProperties.v](OperationProperties.v), and
  [Evolution.v](Evolution.v).

* The notion of diacritical progress is formalized in [Core.v](Core.v).

* The notion of diacritical compatibility is formalized in [Combo.v](Combo.v).