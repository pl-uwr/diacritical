(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file provides a number of properties concerning                 *)
(* F^ω of Section 3.2 (di_combo).                                       *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Core.
Require Import Diacritical.ListMember.
Require Import Diacritical.Operations.
Require Import Diacritical.OperationProperties.

Section ComboTake.
Context {E : Type}.

Lemma combo_id {strong weak : list (DiFunc E)} :
  id (⊆) di_combo strong weak.
Proof.
  intros R e He; exists 0; assumption.
Qed.

Lemma combo_str_id {strong : list (DiFunc E)} :
  id (⊆) di_combo_str strong.
Proof.
  intros R e He; exists 0; assumption.
Qed.

Lemma combo_weak_id {strong weak : list (DiFunc E)} :
  id (⊆) di_combo_weak strong weak.
Proof.
  intros R e He; unfold di_combo_weak, compf.
  apply combo_str_id, sumf_take_l, combo_str_id; assumption.
Qed.

Lemma combo_id_sub {strong weak : list (DiFunc E)}
  (R S : DiRel E) :
  R ⊆ S → R ⊆ di_combo strong weak S.
Proof.
  intros Hsub e He; apply combo_id, Hsub, He.
Qed.

Lemma combo_str_id_sub {strong : list (DiFunc E)}
  (R S : DiRel E) :
  R ⊆ S → R ⊆ di_combo_str strong S.
Proof.
  intros Hsub e He; apply combo_str_id, Hsub, He.
Qed.

Lemma combo_weak_id_sub {strong weak : list (DiFunc E)}
  (R S : DiRel E) :
  R ⊆ S → R ⊆ di_combo_weak strong weak S.
Proof.
  intros Hsub e He; apply combo_weak_id, Hsub, He.
Qed.

Lemma combo1_take {strong weak : list (DiFunc E)}
  (f : DiFunc E) {Mem : ListMember f (strong ++ weak)} :
  f (⊆) di_combo1 strong weak.
Proof.
  intros R e He; exists listMember; rewrite listMember_ok; assumption.
Qed.

Lemma combo_str1_take {strong : list (DiFunc E)}
  (f : DiFunc E) {Mem : ListMember f strong} :
  f (⊆) di_combo_str1 strong.
Proof.
  intros R e He; exists listMember; rewrite listMember_ok; assumption.
Qed.

Lemma combo_weak1_take {weak : list (DiFunc E)}
  (f : DiFunc E) {Mem : ListMember f weak} :
  f (⊆) di_combo_weak1 weak.
Proof.
  intros R e He; exists listMember; rewrite listMember_ok; assumption.
Qed.

Lemma combo_weak1_take_sub {weak : list (DiFunc E)}
  (f : DiFunc E) {Mem : ListMember f weak}
  (R S : DiRel E) :
  R ⊆ f S → R ⊆ di_combo_weak1 weak S.
Proof.
  intros Hsub e He; apply (combo_weak1_take f), Hsub, He.
Qed.

Lemma combo_take {strong weak : list (DiFunc E)}
  (f : DiFunc E) {Mem : ListMember f (strong ++ weak)}
  {Mf : Monotone f} {Cf : Continuous f} :
  f ∘ di_combo strong weak (⊆) di_combo strong weak.
Proof.
  apply (omega_take f), (combo1_take f).
Qed.

Lemma combo_str_take {strong : list (DiFunc E)}
  (f : DiFunc E) {Mem : ListMember f strong}
  {Mf : Monotone f} {Cf : Continuous f} :
  f ∘ di_combo_str strong (⊆) di_combo_str strong.
Proof.
  apply (omega_take f), (combo_str1_take f).
Qed.

Lemma combo_weak_take_str {strong weak : list (DiFunc E)}
  (f : DiFunc E) {Mem : ListMember f strong}
  {Mf : Monotone f} {Cf : Continuous f} :
  f ∘ di_combo_weak strong weak (⊆) di_combo_weak strong weak.
Proof.
  intro R; apply (combo_str_take f).
Qed.

Lemma combo_weak_take_weak {strong weak : list (DiFunc E)}
  (f : DiFunc E) {Mem : ListMember f weak} :
  f ∘ di_combo_str strong (⊆) di_combo_weak strong weak.
Proof.
  intro R; apply combo_str_id_sub, sumf_take_r_sub, (combo_weak1_take_sub f).
  reflexivity.
Qed.

Lemma combo_take_sub {strong weak : list (DiFunc E)}
  (f : DiFunc E) {Mem : ListMember f (strong ++ weak)}
  {Mf : Monotone f} {Cf : Continuous f}
  (R S : DiRel E) :
  R ⊆ f (di_combo strong weak S) → R ⊆ di_combo strong weak S.
Proof.
  intros Hsub e He; apply (combo_take f), Hsub, He.
Qed.

Lemma combo_str_take_sub {strong : list (DiFunc E)}
  (f : DiFunc E) {Mem : ListMember f strong}
  {Mf : Monotone f} {Cf : Continuous f}
  (R S : DiRel E) :
  R ⊆ f (di_combo_str strong S) → R ⊆ di_combo_str strong S.
Proof.
  intros Hsub e He; apply (combo_str_take f), Hsub, He.
Qed.

Lemma combo_weak_take_str_sub {strong weak : list (DiFunc E)}
  (f : DiFunc E) {Mem : ListMember f strong}
  {Mf : Monotone f} {Cf : Continuous f}
  (R S : DiRel E) :
  R ⊆ f (di_combo_weak strong weak S) → R ⊆ di_combo_weak strong weak S.
Proof.
  intros Hsub e He; apply (combo_weak_take_str f), Hsub, He.
Qed.

Lemma combo_weak_take_weak_sub {strong weak : list (DiFunc E)}
  (f : DiFunc E) {Mem : ListMember f weak}
  (R S : DiRel E) :
  R ⊆ f (di_combo_str strong S) → R ⊆ di_combo_weak strong weak S.
Proof.
  intros Hsub e He; apply (combo_weak_take_weak f), Hsub, He.
Qed.

End ComboTake.

Tactic Notation "take" constr(f) :=
  match goal with
  | |- di_combo ?strong ?weak ?R ?e =>
    apply (combo_take f); change (f (di_combo strong weak R) e)
  | |- ?view1 (di_combo ?strong ?weak ?R) ?e1 =>
    apply (combo_take f);
    change (view1 (f (di_combo strong weak R)) e1)
  | |- ?view2 (di_combo ?strong ?weak ?R) ?e1 ?e2 =>
    apply (combo_take f);
    change (view2 (f (di_combo strong weak R)) e1 e2)
  | |- ?view3 (di_combo ?strong ?weak ?R) ?e1 ?e2 ?e3 =>
    apply (combo_take f);
    change (view3 (f (di_combo strong weak R)) e1 e2 e3)

  | |- di_combo_str ?strong ?R ?e =>
    apply (combo_str_take f); change (f (di_combo_str strong R) e)
  | |- ?view1 (di_combo_str ?strong ?R) ?e1 =>
    apply (combo_str_take f);
    change (view1 (f (di_combo_str strong R)) e1)
  | |- ?view2 (di_combo_str ?strong ?R) ?e1 ?e2 =>
    apply (combo_str_take f);
    change (view2 (f (di_combo_str strong R)) e1 e2)
  | |- ?view3 (di_combo_str ?strong ?R) ?e1 ?e2 ?e3 =>
    apply (combo_str_take f);
    change (view3 (f (di_combo_str strong R)) e1 e2 e3)

  | |- di_combo_weak ?strong ?weak ?R ?e =>
    first [ apply (combo_weak_take_str f)
          ; change (f (di_combo_weak strong weak R) e)
          | apply (combo_weak_take_weak f)
          ; change (f (di_combo_str strong R) e) ]
  | |- ?view1 (di_combo_weak ?strong ?weak ?R) ?e1 =>
    first [ apply (combo_weak_take_str f)
          ; change (view1 (f (di_combo_weak strong weak R)) e1)
          | apply (combo_weak_take_weak f)
          ; change (view1 (f (di_combo_str strong R)) e1) ]
  | |- ?view2 (di_combo_weak ?strong ?weak ?R) ?e1 ?e2 =>
    first [ apply (combo_weak_take_str f)
          ; change (view2 (f (di_combo_weak strong weak R)) e1 e2)
          | apply (combo_weak_take_weak f)
          ; change (view2 (f (di_combo_str strong R)) e1 e2) ]
  | |- ?view3 (di_combo_weak ?strong ?weak ?R) ?e1 ?e2 ?e3 =>
    first [ apply (combo_weak_take_str f)
          ; change (view3 (f (di_combo_weak strong weak R)) e1 e2 e3)
          | apply (combo_weak_take_weak f)
          ; change (view3 (f (di_combo_str strong R)) e1 e2 e3) ]

  | |- f ?R ⊆ di_combo ?strong ?weak ?S =>
    apply (combo_take_sub f); apply (monotone f)
  | |- f ?R ⊆ di_combo_str ?strong ?S =>
    apply (combo_str_take_sub f); apply (monotone f)
  | |- f ?R ⊆ di_combo_weak ?strong ?weak ?S =>
    first [ apply (combo_weak_take_str_sub f)
          | apply (combo_weak_take_weak_sub f)
          ]; apply (monotone f)
  end.

Tactic Notation "take" "in" hyp(H) :=
  match type of H with
  | ?f (di_combo ?strong ?weak ?R) ?e =>
    apply (combo_take f) in H
  | ?view1 (?f (di_combo ?strong ?weak ?R)) ?e1 =>
    apply (combo_take f) in H;
    change (view1 (di_combo strong weak R) e1) in H
  | ?view2 (?f (di_combo ?strong ?weak ?R)) ?e1 ?e2 =>
    apply (combo_take f) in H;
    change (view2 (di_combo strong weak R) e1 e2) in H
  | ?view3 (?f (di_combo ?strong ?weak ?R)) ?e1 ?e2 ?e3 =>
    apply (combo_take f) in H;
    change (view3 (di_combo strong weak R) e1 e2 e3) in H

  | ?f (di_combo_str ?strong ?R) ?e =>
    apply (combo_str_take f) in H
  | ?view1 (?f (di_combo_str ?strong ?R)) ?e1 =>
    apply (combo_str_take f) in H;
    change (view1 (di_combo_str strong R) e1) in H
  | ?view2 (?f (di_combo_str ?strong ?R)) ?e1 ?e2 =>
    apply (combo_str_take f) in H;
    change (view2 (di_combo_str strong R) e1 e2) in H
  | ?view3 (?f (di_combo_str ?strong ?R)) ?e1 ?e2 ?e3 =>
    apply (combo_str_take f) in H;
    change (view3 (di_combo_str strong R) e1 e2 e3) in H
  end.

Tactic Notation "take_done" :=
  match goal with
  | |- di_combo ?strong ?weak ?R ?e =>
    apply combo_id; change (id R e) with (R e)
  | |- ?view1 (di_combo ?strong ?weak ?R) ?e1 =>
    apply combo_id; change (view1 R e1)
  | |- ?view2 (di_combo ?strong ?weak ?R) ?e1 ?e2 =>
    apply combo_id; change (view2 R e1 e2)
  | |- ?view3 (di_combo ?strong ?weak ?R) ?e1 ?e2 ?e3 =>
    apply combo_id; change (view3 R e1 e2 e3)
  | |- di_combo_str ?strong ?R ?e =>
    apply combo_str_id; change (id R e) with (R e)

  | |- di_combo_str ?strong ?R ?e =>
    apply combo_str_id; change (id R e) with (R e)
  | |- ?view1 (di_combo_str ?strong ?R) ?e1 =>
    apply combo_str_id; change (view1 R e1)
  | |- ?view2 (di_combo_str ?strong ?R) ?e1 ?e2 =>
    apply combo_str_id; change (view2 R e1 e2)
  | |- ?view3 (di_combo_str ?strong ?R) ?e1 ?e2 ?e3 =>
    apply combo_str_id; change (view3 R e1 e2 e3)

  | |- di_combo_weak ?strong ?weak ?R ?e =>
    apply combo_weak_id; change (id R e) with (R e)
  | |- ?view1 (di_combo_weak ?strong ?weak ?R) ?e1 =>
    apply combo_weak_id; change (view1 R e1)
  | |- ?view2 (di_combo_weak ?strong ?weak ?R) ?e1 ?e2 =>
    apply combo_weak_id; change (view2 R e1 e2)
  | |- ?view3 (di_combo_weak ?strong ?weak ?R) ?e1 ?e2 ?e3 =>
    apply combo_weak_id; change (view3 R e1 e2 e3)

  | |- ?R ⊆ di_combo ?strong ?weak ?S =>
    apply combo_id_sub
  | |- ?R ⊆ di_combo_str ?strong ?S =>
    apply combo_str_id_sub
  | |- ?R ⊆ di_combo_weak ?strong ?weak ?S =>
    apply combo_weak_id_sub
  end.

Ltac pr_assumption :=
  match goal with
  | [ H: ?R ↣ ?Q & ?S |- _ ] =>
    solve [ assumption
          | apply (progress_sub_pas H); assumption
          | apply (progress_sub_act H); assumption
          ]
  end.