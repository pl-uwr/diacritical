(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file provides a number of properties of di_combo and cousins    *)
(* from Operations.v (F^ω in Section 3.2).                              *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Core.
Require Import Diacritical.Evolution.
Require Import Diacritical.ListMember.
Require Import Diacritical.Operations.
Require Import Diacritical.OperationProperties.
Require Import Diacritical.Take.

Section Combo.
Context {E : Type} {EP : DiElemProgressCore E} {DP : DiElemProgress E}.

Local Definition di_combo' (strong weak : list (DiFunc E)) : DiFunc E :=
  omega (di_combo_weak strong weak).

Global Instance Monotone_combo1 {strong weak : list (DiFunc E)}
  {FM : ListForall Monotone (strong ++ weak)} :
  Monotone (di_combo1 strong weak).
Proof.
  intros R S HRS e [ M He ].
  specialize (FM M); exists M.
  eapply monotone in He; eassumption.
Qed.

Global Instance Monotone_combo_str1 {strong : list (DiFunc E)}
  {FM : ListForall Monotone strong} :
  Monotone (di_combo_str1 strong).
Proof.
  intros R S HRS e [ M He ].
  specialize (FM M); exists M.
  eapply monotone in He; eassumption.
Qed.

Global Instance Monotone_combo_weak1 {weak : list (DiFunc E)}
  {FM : ListForall Monotone weak} :
  Monotone (di_combo_weak1 weak).
Proof.
  intros R S HRS e [ M He ].
  specialize (FM M); exists M.
  eapply monotone in He; eassumption.
Qed.

Global Instance Monotone_combo {strong weak : list (DiFunc E)}
  {FM : ListForall Monotone (strong ++ weak)} :
  Monotone (di_combo strong weak).
Proof.
  apply (omega_monotone _).
Qed.

Global Instance Monotone_combo_str {strong : list (DiFunc E)}
  {FM : ListForall Monotone strong} :
  Monotone (di_combo_str strong).
Proof.
  apply (omega_monotone _).
Qed.

Global Instance Monotone_combo_weak {strong weak : list (DiFunc E)}
  {FM : ListForall Monotone (strong ++ weak)} :
  Monotone (di_combo_weak strong weak).
Proof.
  specialize (ListForall_elim_append_l FM) as FMs.
  specialize (ListForall_elim_append_r FM) as FMw.
  apply comp_monotone; [ | apply Monotone_combo_str ].
  apply comp_monotone; [ apply Monotone_combo_str | ].
  apply (sumf2_monotone _ _).
Qed.

Global Instance Continuous_combo1 {strong weak : list (DiFunc E)}
  {FC : ListForall Continuous (strong ++ weak)} :
  Continuous (di_combo1 strong weak).
Proof.
  intros R e [ M He ]; specialize (FC M).
  apply (continuous _) in He; destruct He as [ l [ Hl He ] ].
  exists l; split; [ assumption | ].
  eexists; eassumption.
Qed.

Global Instance Continuous_combo_str1 {strong : list (DiFunc E)}
  {FC : ListForall Continuous strong} :
  Continuous (di_combo_str1 strong).
Proof.
  intros R e [ M He ]; specialize (FC M).
  apply (continuous _) in He; destruct He as [ l [ Hl He ] ].
  exists l; split; [ assumption | ].
  eexists; eassumption.
Qed.

Global Instance Continuous_combo {strong weak : list (DiFunc E)}
  {FM : ListForall Monotone (strong ++ weak)}
  {FC : ListForall Continuous (strong ++ weak)} :
  Continuous (di_combo strong weak).
Proof.
  apply (omega_continuous _).
Qed.

Lemma combo_idempotent (strong weak : list (DiFunc E))
  {FM : ListForall Monotone (strong ++ weak)}
  {FC : ListForall Continuous (strong ++ weak)} :
  (di_combo strong weak ∘ di_combo strong weak) (⊆) di_combo strong weak.
Proof.
  apply (omega_idempotent _).
Qed.

Lemma combo_str_idempotent (strong : list (DiFunc E))
  {FM : ListForall Monotone strong}
  {FC : ListForall Continuous strong} :
  (di_combo_str strong ∘ di_combo_str strong) (⊆) di_combo_str strong.
Proof.
  apply (omega_idempotent _).
Qed.

Lemma combo_weak_combo_str (strong weak : list (DiFunc E))
  {FM : ListForall Monotone (strong ++ weak)}
  {FC : ListForall Continuous strong} :
  (di_combo_weak strong weak ∘ di_combo_str strong) (⊆) 
  di_combo_weak strong weak.
Proof.
  specialize (ListForall_elim_append_l FM) as FMs.
  specialize (ListForall_elim_append_r FM) as FMw.
  unfold di_combo_weak; intro R; unfold compf.
  apply (monotone _), (sumf2_monotone _ _), (combo_str_idempotent _).
Qed.

Lemma combo_str_combo_weak (strong weak : list (DiFunc E))
  {FM : ListForall Monotone strong}
  {FC : ListForall Continuous strong} :
  (di_combo_str strong ∘ di_combo_weak strong weak) (⊆) 
  di_combo_weak strong weak.
Proof.
  unfold di_combo_weak; intro R; unfold compf.
  apply (combo_str_idempotent _).
Qed.

Lemma combo_str1_in_combo1 (strong weak : list (DiFunc E)) :
  di_combo_str1 strong (⊆) di_combo1 strong weak.
Proof.
  intros R e [ M He ]; induction M.
  + exists (LM_Here x (xs ++ _)); assumption.
  + specialize (IHM He); destruct IHM as [ M' He' ].
    exists (LM_There x _ M'); assumption.
Qed.

Lemma combo_str_in_combo (strong weak : list (DiFunc E))
  {FM : ListForall Monotone (strong ++ weak)} :
  di_combo_str strong (⊆) di_combo strong weak.
Proof.
  apply (omega_monotone_f _ _), combo_str1_in_combo1.
Qed.

Lemma combo_str_in_combo_weak (strong weak : list (DiFunc E)) :
  di_combo_str strong (⊆) di_combo_weak strong weak.
Proof.
  intros R e He; unfold di_combo_weak, compf.
  take_done; apply sumf_take_l; assumption.
Qed.

Lemma combo_weak_in_combo (strong weak : list (DiFunc E))
  {FM : ListForall Monotone (strong ++ weak)}
  {FC : ListForall Continuous (strong ++ weak)} :
  di_combo_weak strong weak (⊆) di_combo strong weak.
Proof.
  specialize (ListForall_elim_append_l FM) as FMs.
  specialize (ListForall_elim_append_r FM) as FMw.
  specialize (ListForall_elim_append_r FC) as FCw.
  intro R; unfold di_combo_weak.
  etransitivity; [ | apply (combo_idempotent strong weak) ]; unfold compf.
  etransitivity; [ | apply (combo_str_in_combo strong weak) ].
  apply (monotone _).
  etransitivity; [ apply sumf2_comp_distr_l_fwd | ].
  apply sub_sumf2; [ apply (combo_str_in_combo strong weak) | ].
  clear R; intro R; unfold compf.
  etransitivity; [ eapply (monotone _), (combo_str_in_combo strong weak) | ].
  apply (omega_take (di_combo_weak1 weak)).
  clear R; intros R e [ M He ].
  specialize (list_member_append_r strong M) as Mem.
  destruct Mem as [ N Heq ].
  exists N; rewrite Heq; assumption.
Qed.

Local Lemma combo_in_combo' (strong weak : list (DiFunc E))
  {FM : ListForall Monotone (strong ++ weak)}
  {FC : ListForall Continuous (strong ++ weak)} :
  di_combo strong weak (⊆) di_combo' strong weak.
Proof.
  apply (omega_monotone_f _ _).
  intros R e [ M He ].
  specialize (FM M); specialize (FC M).
  destruct (list_member_append M) as [ N | N ].
  + take (get_member M); eapply (monotone _); [ | eassumption ].
    take_done; reflexivity.
  + take (get_member M); eapply (monotone _); [ | eassumption ].
    take_done; reflexivity.
Qed.

Local Lemma combo'_in_combo (strong weak : list (DiFunc E))
  {FM : ListForall Monotone (strong ++ weak)}
  {FC : ListForall Continuous (strong ++ weak)} :
  di_combo' strong weak (⊆) di_combo strong weak.
Proof.
  intros R e [ n He ]; revert R e He; induction n; simpl; intros R e He.
  { take_done; assumption. }
  unfold compf in He; destruct He as [ [] He ].
  + apply (combo_idempotent strong weak); unfold compf.
    eapply (monotone _); [ exact (IHn R) | ].
    apply (combo_weak_in_combo _ _); assumption.
  + apply IHn; assumption.
Qed.

Lemma combo_str1_evolution (strong weak : list (DiFunc E))
  {Fstr : ListForall
    (λ f, f !↝ di_combo_str strong & di_combo strong weak) 
    strong } :
  di_combo_str1 strong !↝ di_combo_str strong & di_combo strong weak.
Proof.
  apply strong_evolution_sumf; intro M; specialize (Fstr M); exact Fstr.
Qed.

Lemma combo_weak1_evolution (strong weak : list (DiFunc E))
  {Fweak : ListForall
    (λ f, f ↝ di_combo_weak strong weak & di_combo strong weak) 
    weak } :
  di_combo_weak1 weak ↝
  di_combo_weak strong weak & di_combo strong weak.
Proof.
  apply evolution_sumf; intro M; specialize (Fweak M); exact Fweak.
Qed.

Lemma combo_str_evolution (strong weak : list (DiFunc E))
  {FM : ListForall Monotone (strong ++ weak)}
  {FC : ListForall Continuous (strong ++ weak)}
  {Fstr : ListForall
    (λ f, f !↝ di_combo_str strong & di_combo strong weak) 
    strong } :
  di_combo_str strong !↝ di_combo_str strong & di_combo strong weak.
Proof.
  specialize (ListForall_elim_append_l FM) as FMs.
  specialize (ListForall_elim_append_l FC) as FCs.
  apply strong_evolution_limit.
  induction n; simpl.
  + eapply strong_evolution_monotone23; [ | | apply Evolution_id ].
    - intro R; take_done; reflexivity.
    - intro R; take_done; reflexivity.
  + eapply strong_evolution_monotone1; [ apply sumf2_comp_distr_l_fwd | ].
    apply strong_evolution_sumf2; [ | assumption ].
    eapply strong_evolution_monotone23;
      [ apply (combo_str_idempotent _) | apply (combo_idempotent _ _) | ].
    apply strong_evolution_comp; [ | assumption ].
    apply (combo_str1_evolution strong weak).
Qed.

Lemma combo_weak_evolution (strong weak : list (DiFunc E))
  {FM : ListForall Monotone (strong ++ weak)}
  {FC : ListForall Continuous (strong ++ weak)}
  {Fstr : ListForall
    (λ f, f !↝ di_combo_str strong & di_combo strong weak) 
    strong }
  {Fweak : ListForall
    (λ f, f ↝ di_combo_weak strong weak & di_combo strong weak) 
    weak } :
  di_combo_weak strong weak ↝
  di_combo_weak strong weak & di_combo strong weak.
Proof.
  specialize (ListForall_elim_append_l FM) as FMs.
  specialize (ListForall_elim_append_l FC) as FCs.
  unfold di_combo_weak at 1.
  eapply evolution_monotone23;
    [ apply (combo_weak_combo_str strong weak)
    | apply (combo_idempotent strong weak) | ].
  apply evolution_comp;
    [ | apply weaken_evolution, (combo_str_evolution strong weak) ].
  eapply evolution_monotone23;
    [ apply (combo_str_combo_weak strong weak)
    | apply (combo_idempotent strong weak) | ].
  apply evolution_comp_s; [ apply (combo_str_evolution strong weak) | ].
  apply evolution_sumf2; [ | apply (combo_weak1_evolution strong weak) ].
  eapply evolution_monotone23; [ | | apply weaken_evolution, Evolution_id ].
  - intro R; take_done; reflexivity.
  - intro R; take_done; reflexivity.
Qed.

Local Lemma combo'_evolution (strong weak : list (DiFunc E))
  {FM : ListForall Monotone (strong ++ weak)}
  {FC : ListForall Continuous (strong ++ weak)}
  {Fstr : ListForall
    (λ f, f !↝ di_combo_str strong & di_combo strong weak) 
    strong }
  {Fweak : ListForall
    (λ f, f ↝ di_combo_weak strong weak & di_combo strong weak) 
    weak } :
  di_combo' strong weak ↝ di_combo' strong weak & di_combo strong weak.
Proof.
  apply evolution_limit.
  induction n; simpl.
  + eapply evolution_monotone3; [ | apply weaken_evolution, Evolution_id ].
    intro R; take_done; reflexivity.
  + eapply evolution_monotone3; [ apply (combo_idempotent strong weak) | ].
    apply evolution_comp; [ clear IHn | exact IHn ].
    apply evolution_sumf2.
    - eapply evolution_monotone2; [ | apply (combo_weak_evolution strong weak) ].
      apply sumf_take_l.
    - eapply evolution_monotone23; [ | | apply weaken_evolution, Evolution_id ].
      * apply sumf_take_r.
      * intro R; take_done; reflexivity.
Qed.

Lemma combo_evolution (strong weak : list (DiFunc E))
  {FM : ListForall Monotone (strong ++ weak)}
  {FC : ListForall Continuous (strong ++ weak)}
  {Fstr : ListForall
    (λ f, f !↝ di_combo_str strong & di_combo strong weak) 
    strong }
  {Fweak : ListForall
    (λ f, f ↝ di_combo_weak strong weak & di_combo strong weak) 
    weak } :
  di_combo strong weak ↝ di_combo strong weak & di_combo strong weak.
Proof.
  intros R S HRS.
  eapply evolution_monotone;
    [ | | | apply (combo'_evolution strong weak) | exact HRS ].
  + apply (combo_in_combo' strong weak).
  + apply (combo'_in_combo strong weak).
  + reflexivity.
Qed.

End Combo.