(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the definitions of operations on functions:       *)
(* union, composition and iteration as well as of F^ω (di_combo),       *)
(* of Section 3.2.                                                      *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Core.
Require Import Diacritical.ListMember.

Section Operations.

Context {E   : Type}.

Definition sumr {X : Type} (f : X → DiRel E) : DiRel E :=
  λ e, ∃ x, f x e.

Definition sumf {X : Type} (f : X → DiFunc E) : DiFunc E :=
  λ R, sumr (λ x, f x R).

Definition sumf2 (f g : DiFunc E) : DiFunc E :=
  sumf (λ b : bool, if b then f else g).

Definition compf (f g : DiFunc E) : DiFunc E :=
  λ R, f (g R).

Definition id : DiFunc E := λ R, R.

Notation "f ∪ g" := (sumf2 f g) (at level 50).
Notation "f ∘ g" := (compf f g) (at level 50).

Fixpoint iter (f : DiFunc E) (n : nat) : DiFunc E :=
  match n with
  | 0   => id
  | S n => (f ∪ id) ∘ iter f n
  end.

Definition omega (f : DiFunc E) : DiFunc E := sumf (iter f).

Definition di_combo_str1 (strong : list (DiFunc E)) :=
  sumf (λ M : list_member strong, get_member M).

Definition di_combo_str (strong : list (DiFunc E)) :=
  omega (di_combo_str1 strong).

Definition di_combo1 (strong weak : list (DiFunc E)) :=
  sumf (λ M : list_member (strong ++ weak), get_member M).

Definition di_combo (strong weak : list (DiFunc E)) :=
  omega (di_combo1 strong weak).

Definition di_combo_weak1 (weak : list (DiFunc E)) : DiFunc E :=
  sumf (λ M : list_member weak, get_member M).

Definition di_combo_weak (strong weak : list (DiFunc E)) : DiFunc E :=
  (di_combo_str strong ∘ (id ∪ di_combo_weak1 weak) ∘ di_combo_str strong).

End Operations.

Notation "f ∪ g" := (sumf2 f g) (at level 50).
Notation "f ∘ g" := (compf f g) (at level 50).