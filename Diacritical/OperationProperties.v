(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains proofs of some properties,                        *)
(* e.g., monotonicity and continuity preservation,                      *)
(* of the operations on functions from Section 3.2.                     *)
(************************************************************************)

Require Import Utf8.
Require Import List.

Require PeanoNat.
Import PeanoNat.Nat.
Import ListNotations.

Require Import Diacritical.Core.
Require Import Diacritical.Operations.

Section OperProperties.
Context {E : Type}.

Lemma sumf_take_l (f g : DiFunc E) :
  f (⊆) (f ∪ g).
Proof.
  intros R e He; exists true; assumption.
Qed.

Lemma sumf_take_r (f g : DiFunc E) :
  g (⊆) (f ∪ g).
Proof.
  intros R e He; exists false; assumption.
Qed.

Lemma sumf_take_r_sub (R S : DiRel E) (f g : DiFunc E) :
  R ⊆ g S → R ⊆ (f ∪ g) S.
Proof.
  intro Hsub; etransitivity; [ eassumption | apply sumf_take_r ].
Qed.

Lemma sub_sumf2 (f g h : DiFunc E) :
  f (⊆) h → g (⊆) h → f ∪ g (⊆) h.
Proof.
  intros Hf Hg R e [ [] He ].
  + apply Hf; assumption.
  + apply Hg; assumption.
Qed.

Lemma iter_monotone_n (n m : nat) (f : DiFunc E) :
  n ≤ m → ∀ R, iter f n R ⊆ iter f m R.
Proof.
  induction 1.
  + intro R; reflexivity.
  + intro R; etransitivity.
    - apply IHle.
    - simpl; unfold compf.
      apply sumf_take_r_sub; reflexivity.
Qed.

Lemma iter_in_omega (f : DiFunc E) (n : nat) R :
  iter f n R ⊆ omega f R.
Proof.
  intros e He; exists n; assumption.
Qed.

Lemma omega_limit (f : DiFunc E) R S :
  (∀ n, iter f n R ⊆ S) → omega f R ⊆ S.
Proof.
  intros Hf e [ n He ]; apply (Hf n); assumption.
Qed.

Lemma omega_take (f : DiFunc E) {Mf : Monotone f} {Cf : Continuous f}
  {g : DiFunc E} :
  f (⊆) g → f ∘ omega g (⊆) omega g.
Proof.
  intros Hfg R e He.
  apply (continuous f) in He; destruct He as [ l [ Hl He ] ].
  assert (Hn : ∃ n, ∀ e, In e l → iter g n R e).
  { clear e He; induction Hl as [ | e l [ m He ] Hl [ n IH ] ].
    + exists 0; intros _ [].
    + exists (max m n); intros e' [ Heq | ].
      - subst; eapply iter_monotone_n; [ | eassumption ].
        apply le_max_l.
      - eapply iter_monotone_n; [ | apply IH; assumption ].
        apply le_max_r.
  }
  destruct Hn as [ n Hn ].
  exists (S n); simpl.
  apply sumf_take_l, Hfg; eapply (monotone f); [ | eassumption ].
  clear e He; intros e He; auto.
Qed.

Lemma omega_idempotent (f : DiFunc E) {Mf : Monotone f} {Cf : Continuous f} :
  (omega f ∘ omega f) (⊆) omega f.
Proof.
  intros R e [ n He ].
  revert R e He; induction n; intros R e He; [ assumption | ].
  destruct He as [ [ ] He ]; [ | auto ].
  apply (omega_take f); [ reflexivity | ].
  eapply (monotone f); [ | eassumption ].
  exact (IHn R).
Qed.

Global Instance id_monotone : Monotone (id (E:=E)).
Proof.
  intros R S; auto.
Qed.

Lemma sumf2_monotone (f g : DiFunc E) {Mf : Monotone f} {Mg : Monotone g} :
  Monotone (f ∪ g).
Proof.
  intros R S HRS e [ [] He ].
  + apply sumf_take_l, (Mf _ _ HRS); assumption.
  + apply sumf_take_r, (Mg _ _ HRS); assumption.
Qed.

Lemma comp_monotone (f g : DiFunc E) {Mf : Monotone f} {Mg : Monotone g} :
  Monotone (f ∘ g).
Proof.
  intros R S HRS.
  apply Mf, Mg; assumption.
Qed.

Lemma iter_monotone (f : DiFunc E) n {Mf : Monotone f} : Monotone (iter f n).
Proof.
  induction n; intros R S HRS; simpl; [ assumption | ].
  apply comp_monotone; [ | assumption | assumption ].
  apply sumf2_monotone; [ assumption | apply id_monotone ].
Qed.

Lemma omega_monotone (f : DiFunc E) {Mf : Monotone f} : Monotone (omega f).
Proof.
  intros R S HRS e [ n He ]; exists n.
  eapply iter_monotone; eassumption.
Qed.

Lemma omega_monotone_f (f g : DiFunc E) {Mg : Monotone g} :
  f (⊆) g → omega f (⊆) omega g.
Proof.
  intros Hg R e [ n He ].
  exists n.
  revert R e He; induction n; intros R e He; [ assumption | ].
  simpl; simpl in He.
  unfold compf; unfold compf in He.
  destruct He as [ [] He ].
  + apply sumf_take_l.
    eapply (monotone g); [ exact (IHn R) | ].
    apply Hg; assumption.
  + apply sumf_take_r, IHn; assumption.
Qed.

Local Lemma Forall_to_In (A : Type) (P : A → Prop) (l : list A) :
  Forall P l → ∀ x, In x l → P x.
Proof.
  induction 1; simpl.
  + firstorder.
  + intros ? [ [] | ]; firstorder.
Qed.

Lemma iter_continuous (f : DiFunc E) n {Mf : Monotone f} {Cf : Continuous f} :
  Continuous (iter f n).
Proof.
  induction n.
  + intros R e He; simpl in He.
    exists ([ e ]%list); split; [ auto | ]; simpl.
    left; reflexivity.
  + intros R e He; simpl in He; simpl.
    destruct He as [ [ ] He ].
    - apply (continuous f) in He; destruct He as [ l [ Hl He ] ].
      assert (Hl' : ∃ l', Forall R l' ∧ Forall (iter f n (λ e', In e' l')) l).
      { clear He; induction Hl as [ | x l Hx Hl [ l' [ Hl' Hll' ] ] ].
        + exists nil; auto.
        + apply IHn in Hx; destruct Hx as [ l'' [ Hl'' Hx ] ].
          exists (l' ++ l''); split.
        - clear Hll'; induction Hl'; simpl; auto.
        - constructor.
          * eapply (iter_monotone f n); [ | eassumption ].
            intros y Hy; apply in_or_app; eauto.
          * eapply Forall_impl; [ | eassumption ].
            apply (iter_monotone f n).
            intros y Hy; apply in_or_app; eauto.
      }
      destruct Hl' as [ l' [ Hl' Hll' ] ].
      exists l'; split; [ assumption | ].
      apply sumf_take_l; eapply (monotone f); [ | eassumption ].
      intros e' He'; eapply Forall_to_In in Hll'; [ eassumption | ]; assumption.
    - apply IHn in He; destruct He as [ l [ Hl He ] ].
      exists l; split; [ assumption | ].
      apply sumf_take_r; assumption.
Qed.

Lemma omega_continuous (f : DiFunc E) {Mf : Monotone f} {Cf : Continuous f} : 
  Continuous (omega f).
Proof.
  intros R e [ n He ].
  apply (iter_continuous f n) in He.
  destruct He as [ l [ Hl He ] ].
  exists l; split; [ assumption | ].
  exists n; assumption.
Qed.

Lemma sumf2_comp_distr_l_fwd (f g h : DiFunc E) :
  (f ∪ g) ∘ h (⊆) (f ∘ h) ∪ (g ∘ h).
Proof.
  intros R e [ [] He ]; [ exists true | exists false ]; assumption.
Qed.

End OperProperties.