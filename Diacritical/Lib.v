(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file gathers and exports the general notions related            *)
(* to diacritical compatibility (Section 3.2).                          *)
(************************************************************************)

Require Export Diacritical.Combo.
Require Export Diacritical.Core.
Require Export Diacritical.Operations.
Require Export Diacritical.Take.