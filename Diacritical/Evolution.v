(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains proofs of properties relating evolution           *)
(* and operations on functions of Section 3.2.                          *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Core.
Require Import Diacritical.Operations.
Require Import Diacritical.OperationProperties.

Section Evolution.
Context {E : Type} {EP : DiElemProgressCore E} {DP : DiElemProgress E}.

Global Instance Evolution_id :
  id !↝ id & id.
Proof.
  intros R Q S HRQS; exact HRQS.
Qed.

Lemma weaken_evolution (f g h : DiFunc E) :
  f !↝ g & h → f ↝ g & h.
Proof.
  intros Hev R S HRS; apply Hev; assumption.
Qed.

Lemma evolution_limit (f h : DiFunc E) :
  (∀ n, iter f n ↝ iter f n & h) → omega f ↝ omega f & h.
Proof.
  intros Hn R S HRS; split.
  + reflexivity.
  + apply omega_limit; intro n.
    eapply progress_sub_act, Hn; assumption.
  + apply omega_limit; intro n; etransitivity.
    - eapply progress_rel, Hn; eassumption.
    - apply elem_progress_monotone; [ | reflexivity ].
      apply iter_in_omega.
Qed.

Lemma strong_evolution_limit (f g h : DiFunc E) :
  (∀ n, iter f n !↝ g & h) → omega f !↝ g & h.
Proof.
  intros Hn R Q S HRQS; split.
  + apply omega_limit; intro n.
    eapply progress_sub_pas, Hn; eassumption.
  + apply omega_limit; intro n.
    eapply progress_sub_act, Hn; eassumption.
  + apply omega_limit; intro n.
    eapply progress_rel, Hn; assumption.
Qed.

Lemma progress_monotone (R₁ R₂ Q₁ Q₂ S₁ S₂ : DiRel E) :
  R₂ ⊆ R₁ → Q₁ ⊆ Q₂ → S₁ ⊆ S₂ →
  R₁ ↣ Q₁ & S₁ → R₂ ↣ Q₂ & S₂.
Proof.
  intros HR HQ HS HRQS; split.
  + etransitivity; [ eassumption | ].
    etransitivity; [ | eassumption ].
    apply HRQS.
  + etransitivity; [ eassumption | ].
    etransitivity; [ | eassumption ].
    apply HRQS.
  + etransitivity; [ eassumption | ].
    etransitivity; [ | apply elem_progress_monotone; try eassumption ].
    apply HRQS.
Qed.

Lemma evolution_monotone (f₁ f₂ g₁ g₂ h₁ h₂ : DiFunc E) :
  f₂ (⊆) f₁ → g₁ (⊆) g₂ → h₁ (⊆) h₂ →
  f₁ ↝ g₁ & h₁ → f₂ ↝ g₂ & h₂.
Proof.
  intros Hf Hg Hh Hev R S HRS.
  eapply progress_monotone; [ apply Hf | apply Hg | apply Hh | ].
  apply Hev; assumption.
Qed.

Lemma strong_evolution_monotone (f₁ f₂ g₁ g₂ h₁ h₂ : DiFunc E) :
  f₂ (⊆) f₁ → g₁ (⊆) g₂ → h₁ (⊆) h₂ →
  f₁ !↝ g₁ & h₁ → f₂ !↝ g₂ & h₂.
Proof.
  intros Hf Hg Hh Hev R Q S HRQS.
  eapply progress_monotone; [ apply Hf | apply Hg | apply Hh | ].
  apply Hev; assumption.
Qed.

Lemma evolution_monotone1 (f₁ f₂ g h : DiFunc E) :
  f₂ (⊆) f₁ → f₁ ↝ g & h → f₂ ↝ g & h.
Proof.
  intro Hf; apply evolution_monotone; solve [ assumption | reflexivity ].
Qed.

Lemma strong_evolution_monotone1 (f₁ f₂ g h : DiFunc E) :
  f₂ (⊆) f₁ → f₁ !↝ g & h → f₂ !↝ g & h.
Proof.
  intro Hf; apply strong_evolution_monotone; solve [ assumption | reflexivity ].
Qed.

Lemma evolution_monotone2 (f g₁ g₂ h : DiFunc E) :
  g₁ (⊆) g₂ → f ↝ g₁ & h → f ↝ g₂ & h.
Proof.
  intro Hg; apply evolution_monotone; solve [ assumption | reflexivity ].
Qed.

Lemma strong_evolution_monotone2 (f g₁ g₂ h : DiFunc E) :
  g₁ (⊆) g₂ → f !↝ g₁ & h → f !↝ g₂ & h.
Proof.
  intro Hg; apply strong_evolution_monotone; solve [ assumption | reflexivity ].
Qed.

Lemma evolution_monotone3 (f g h₁ h₂ : DiFunc E) :
  h₁ (⊆) h₂ → f ↝ g & h₁ → f ↝ g & h₂.
Proof.
  intro Hh; apply evolution_monotone; solve [ assumption | reflexivity ].
Qed.

Lemma strong_evolution_monotone3 (f g h₁ h₂ : DiFunc E) :
  h₁ (⊆) h₂ → f !↝ g & h₁ → f !↝ g & h₂.
Proof.
  intro Hh; apply strong_evolution_monotone; solve [ assumption | reflexivity ].
Qed.

Lemma evolution_monotone23 (f g₁ g₂ h₁ h₂ : DiFunc E) :
  g₁ (⊆) g₂ → h₁ (⊆) h₂ → f ↝ g₁ & h₁ → f ↝ g₂ & h₂.
Proof.
  intros Hg Hh; apply evolution_monotone; solve [ assumption | reflexivity ].
Qed.

Lemma strong_evolution_monotone23 (f g₁ g₂ h₁ h₂ : DiFunc E) :
  g₁ (⊆) g₂ → h₁ (⊆) h₂ → f !↝ g₁ & h₁ → f !↝ g₂ & h₂.
Proof.
  intros Hg Hh; apply strong_evolution_monotone;
    solve [ assumption | reflexivity ].
Qed.

Lemma evolution_comp (f₁ f₂ g h₁ h₂ : DiFunc E) :
  f₁ ↝ g & h₁ → f₂ ↝ f₂ & h₂ → (f₁ ∘ f₂) ↝ (g ∘ f₂) & (h₁ ∘ h₂).
Proof.
  intros Hev₁ Hev₂ R S HRS.
  apply Hev₁, Hev₂; assumption.
Qed.

Lemma evolution_comp_s (f₁ f₂ g₁ g₂ h₁ h₂ : DiFunc E) :
  f₁ !↝ g₁ & h₁ → f₂ ↝ g₂ & h₂ → (f₁ ∘ f₂) ↝ (g₁ ∘ g₂) & (h₁ ∘ h₂).
Proof.
  intros Hev₁ Hev₂ R S HRS.
  apply Hev₁, Hev₂; assumption.
Qed.

Lemma strong_evolution_comp (f₁ f₂ g₁ g₂ h₁ h₂ : DiFunc E) :
  f₁ !↝ g₁ & h₁ → f₂ !↝ g₂ & h₂ → (f₁ ∘ f₂) !↝ (g₁ ∘ g₂) & (h₁ ∘ h₂).
Proof.
  intros Hev₁ Hev₂ R Q S HRQS.
  apply Hev₁, Hev₂; assumption.
Qed.

Lemma evolution_sumf {X : Type} (f : X → DiFunc E) (g h : DiFunc E) :
  (∀ x, f x ↝ g & h) → sumf f ↝ g & h.
Proof.
  intros Hev R S HRS; split.
  + intros e [ x He ]; apply (Hev x) in HRS; apply HRS, He.
  + intros e [ x He ]; apply (Hev x) in HRS; apply HRS, He.
  + intros e [ x He ]; apply (Hev x) in HRS; apply HRS, He.
Qed.

Lemma strong_evolution_sumf {X : Type} (f : X → DiFunc E) (g h : DiFunc E) :
  (∀ x, f x !↝ g & h) → sumf f !↝ g & h.
Proof.
  intros Hev R Q S HRQS; split.
  + intros e [ x He ]; apply (Hev x) in HRQS; apply HRQS, He.
  + intros e [ x He ]; apply (Hev x) in HRQS; apply HRQS, He.
  + intros e [ x He ]; apply (Hev x) in HRQS; apply HRQS, He.
Qed.

Lemma evolution_sumf2 (f₁ f₂ g h : DiFunc E) :
  f₁ ↝ g & h → f₂ ↝ g & h → (f₁ ∪ f₂) ↝ g & h.
Proof.
  intros H1 H2; apply evolution_sumf.
  intros [ ]; assumption.
Qed.

Lemma strong_evolution_sumf2 (f₁ f₂ g h : DiFunc E) :
  f₁ !↝ g & h → f₂ !↝ g & h → (f₁ ∪ f₂) !↝ g & h.
Proof.
  intros H1 H2; apply strong_evolution_sumf.
  intros [ ]; assumption.
Qed.

End Evolution.