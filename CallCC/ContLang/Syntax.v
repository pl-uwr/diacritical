(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file formalizes the syntax of the CBV λ-calculus                *)
(* with call/cc and abort, and with context variables (Section 5.2).    *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib.

Record VSig : Type :=
  { Var  : Set
  ; CVar : Set
  }.

Definition update_Var (f : Set → Set) (Σ : VSig) :=
  {| Var  := f (Var Σ)
  ;  CVar := CVar Σ
  |}.
Definition update_CVar (f : Set → Set) (Σ : VSig) :=
  {| Var  := Var Σ
  ;  CVar := f (CVar Σ)
  |}.

Definition incV := update_Var inc.
Definition incC := update_CVar inc.

Inductive term (Σ : VSig) : Type :=
| t_value : value Σ → term Σ
| t_app   : term Σ → term Σ → term Σ
| t_abort : program Σ → term Σ
with value (Σ : VSig) : Type :=
| v_var    : Var Σ → value Σ
| v_lam    : term (incV Σ) → value Σ
| v_callcc : value Σ
with program (Σ : VSig) : Type :=
| p_term : term Σ → program Σ
| p_cont : CVar Σ → term Σ → program Σ.

Arguments t_value {Σ}.
Arguments t_app   {Σ}.
Arguments t_abort {Σ}.

Arguments v_var    {Σ}.
Arguments v_lam    {Σ}.
Arguments v_callcc {Σ}.

Arguments p_term {Σ}.
Arguments p_cont {Σ}.

Coercion t_value : value >-> term.
Coercion p_term  : term  >-> program.

Inductive ectx (Σ : VSig) : Type :=
| ectx_hole : ectx Σ
| ectx_cont : CVar Σ → ectx Σ
| ectx_app1 : ectx Σ  → term Σ → ectx Σ
| ectx_app2 : value Σ → ectx Σ → ectx Σ.

Arguments ectx_hole {Σ}.
Arguments ectx_cont {Σ}.
Arguments ectx_app1 {Σ}.
Arguments ectx_app2 {Σ}.

Fixpoint eplug {Σ : VSig} (E : ectx Σ) (t : term Σ) : program Σ :=
  match E with
  | ectx_hole     => t
  | ectx_cont α   => p_cont α t
  | ectx_app1 E s => eplug E (t_app  t s)
  | ectx_app2 v E => eplug E (t_app  v t)
  end.

Record VMap (Σ Δ : VSig) : Type :=
  { map_v : Var Σ  → Var Δ
  ; map_c : CVar Σ → CVar Δ
  }.

Arguments map_v {Σ Δ}.
Arguments map_c {Σ Δ}.

Instance ArrowCore_VMap : ArrowCore VMap :=
  { arrow_id := λ _,
      {| map_v := λ x, x
      ;  map_c := λ α, α
      |}
  ; arrow_comp := λ _ _ _ Φ Ψ,
      {| map_v := λ x, map_v Φ (map_v Ψ x)
      ;  map_c := λ α, map_c Φ (map_c Ψ α)
      |}
  ; arrow_eq := λ _ _ Φ Ψ,
      (∀ x, map_v Φ x = map_v Ψ x) ∧ (∀ α, map_c Φ α = map_c Ψ α)
  }.

Instance ALiftableCore_VMap_incV : ALiftableCore VMap incV :=
  { liftA := λ _ _ Φ,
      {| map_v := inc_map (map_v Φ) : Var (incV _) → Var (incV _)
      ;  map_c := map_c Φ
      |}
  }.
Instance ALiftableCore_VMap_incC : ALiftableCore VMap incC :=
  { liftA := λ _ _ Φ,
      {| map_v := map_v Φ : Var (incC _) → Var (incC _)
      ;  map_c := inc_map (map_c Φ)
      |}
  }.

Instance ShiftableCore_incV : ShiftableCore VMap incV :=
  { mk_shift := λ Σ,
      {| map_v := VS : Var Σ → Var (incV Σ)
      ;  map_c := λ α, α
      |}
  }.
Instance ShiftableCore_incC : ShiftableCore VMap incC :=
  { mk_shift := λ Σ,
      {| map_v := λ x : Var Σ, x : Var (incC Σ)
      ;  map_c := VS : CVar Σ → CVar (incC Σ)
      |}
  }.

Fixpoint tmap {Σ Δ : VSig} (Φ : VMap Σ Δ) (t : term Σ) : term Δ :=
  match t with
  | t_value v   => vmap Φ v
  | t_app   t s => t_app   (tmap Φ t) (tmap Φ s)
  | t_abort p   => t_abort (pmap Φ p)
  end
with vmap {Σ Δ : VSig} (Φ : VMap Σ Δ) (v : value Σ) : value Δ :=
  match v with
  | v_var x  => v_var (map_v Φ x)
  | v_lam t  => v_lam (tmap (liftA Φ) t)
  | v_callcc => v_callcc
  end
with pmap {Σ Δ : VSig} (Φ : VMap Σ Δ) (p : program Σ) : program Δ :=
  match p with
  | p_term t   => tmap Φ t
  | p_cont α t => p_cont (map_c Φ α) (tmap Φ t)
  end.

Instance IsMapCore_tmap : IsMapCore (@tmap).
Instance IsMapCore_vmap : IsMapCore (@vmap).
Instance IsMapCore_pmap : IsMapCore (@pmap).

Fixpoint emap {Σ Δ : VSig} (Φ : VMap Σ Δ) (E : ectx Σ) : ectx Δ :=
  match E with
  | ectx_hole     => ectx_hole
  | ectx_cont α   => ectx_cont (map_c Φ α)
  | ectx_app1 E t => ectx_app1 (emap Φ E) (fmap Φ t)
  | ectx_app2 v E => ectx_app2 (fmap Φ v) (emap Φ E)
  end.

Instance IsMapCore_emap : IsMapCore (@emap).

Record VSub (Σ Δ : VSig) : Type :=
  { sub_v : Var Σ  → value Δ
  ; sub_c : CVar Σ → ectx Δ
  }.

Arguments sub_v {Σ Δ}.
Arguments sub_c {Σ Δ}.

Instance PreSubstCore_VSub : PreSubstCore VSub :=
  { subst_pure := λ _,
      {| sub_v := v_var
       ; sub_c := λ α, ectx_cont α
      |}
  ; subst_eq := λ _ _ Φ Ψ,
      (∀ x, sub_v Φ x = sub_v Ψ x) ∧ (∀ α, sub_c Φ α = sub_c Ψ α)
  }.

Instance LiftableCore_VSub_incV : SLiftableCore VSub incV :=
  { liftS := λ _ _ Φ,
      {| sub_v := λ x : Var (incV _),
           match x with
           | VZ   => v_var (VZ : Var (incV _))
           | VS y => shift (sub_v Φ y)
           end
      ;  sub_c := λ α, shift (sub_c Φ α)
      |}
  }.

Fixpoint tbind {Σ Δ : VSig} (Φ : VSub Σ Δ) (t : term Σ) : term Δ :=
  match t with
  | t_value v   => vbind Φ v
  | t_app   t s => t_app   (tbind Φ t) (tbind Φ s)
  | t_abort p   => t_abort (pbind Φ p)
  end
with vbind {Σ Δ : VSig} (Φ : VSub Σ Δ) (v : value Σ) : value Δ :=
  match v with
  | v_var x  => sub_v Φ x
  | v_lam t  => v_lam (tbind (liftS Φ) t)
  | v_callcc => v_callcc
  end
with pbind {Σ Δ : VSig} (Φ : VSub Σ Δ) (p : program Σ) : program Δ :=
  match p with
  | p_term t   => tbind Φ t
  | p_cont α t => eplug (sub_c Φ α) (tbind Φ t)
  end.

Instance IsBindCore_tbind : IsBindCore (@tbind).
Instance IsBindCore_vbind : IsBindCore (@vbind).
Instance IsBindCore_pbind : IsBindCore (@pbind).

Fixpoint ebind {Σ Δ : VSig} (Φ : VSub Σ Δ) (E : ectx Σ) : ectx Δ :=
  match E with
  | ectx_hole     => ectx_hole
  | ectx_cont α   => sub_c Φ α
  | ectx_app1 E t => ectx_app1 (ebind Φ E) (bind Φ t)
  | ectx_app2 v E => ectx_app2 (bind Φ v) (ebind Φ E)
  end.

Instance IsBindCore_ebind : IsBindCore (@ebind).

Instance SubstCore_VSub : SubstCore VMap VSub :=
  { arrow_subst_comp := λ _ _ _ Φ Ψ,
      {| sub_v := λ x, fmap Φ (sub_v Ψ x)
      ;  sub_c := λ α, fmap Φ (sub_c Ψ α)
      |}
  ; subst_comp := λ _ _ _ Φ Ψ,
      {| sub_v := λ x, bind Φ (sub_v Ψ x)
      ;  sub_c := λ α, bind Φ (sub_c Ψ α)
      |}
  }.

Instance SubstitutableCore_value_incV : SubstitutableCore VSub value incV :=
  { mk_subst := λ Σ v,
      {| sub_v := λ x : Var (incV Σ),
           match x with
           | VZ   => v
           | VS y => v_var y
           end
      ;  sub_c := λ α : CVar (incV Σ), ectx_cont (α : CVar Σ)
      |}
  }.

Instance SubstitutableCore_ectx_incC : SubstitutableCore VSub ectx incC :=
  { mk_subst := λ Σ E,
      {| sub_v := λ x : Var (incC Σ), v_var (x : Var Σ)
      ;  sub_c := λ α,
           match α with
           | VZ   => E
           | VS β => ectx_cont β
           end
       |}
  }.