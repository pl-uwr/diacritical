(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file provides a classification of terms and programs            *)
(* in the CBV λ-calculus with call/cc and abort, and context variables. *)
(************************************************************************)

Require Import Utf8.
Require Import CallCC.ContLang.Syntax.
Require Import CallCC.ContLang.Reduction.
Require Import CallCC.ContLang.ReductionProperties.

(** A term either makes a reduction step,
    or it is a value (canonical form),
    or it is an open stuck term.
*)

Inductive term_class {Σ : VSig} : term Σ → Type :=
| tc_step  : ∀ t : term Σ, (∀ E : ectx Σ, { p | red_in E t p }) →
    term_class t
| tc_value : ∀ v : value Σ, term_class v
| tc_open  : ∀ (F : rctx Σ) x (v : value Σ),
    term_class (rplug F (t_app (v_var x) v)).

Fixpoint classify_term {Σ : VSig} (t : term Σ) : term_class t.
Proof.
  destruct t as [ v | t₁ t₂ | p ].
  + apply tc_value.
  + destruct (classify_term _ t₁) as [ t₁ c | v₁ | F x v ].
    - apply tc_step; intro E.
      destruct (c (ectx_app1 E t₂)) as [ p Hred ].
      exists p; apply red_app1; assumption.
    - destruct (classify_term _ t₂) as [ t₂ c | v₂ | F x v ].
      * apply tc_step; intro E.
        destruct (c (ectx_app2 v₁ E)) as [ p Hred ].
        exists p; apply red_app2; assumption.
      * { destruct v₁ as [ x | t | ].
          + apply (tc_open rctx_hole).
          + apply tc_step; intro E; eexists; apply red_beta.
          + apply tc_step; intro E; eexists; apply red_callcc.
        }
      * apply (tc_open (rctx_app2 v₁ F)).
    - apply (tc_open (rctx_app1 F t₂)).
  + apply tc_step; intro E; eexists; apply red_abort.
Defined.

Inductive class {Σ : VSig} : program Σ → Type :=
| c_step   : ∀ p p', red p p' → class p
| c_result : ∀ v : value Σ, class v
| c_open_v : ∀ E x (v : value Σ), class (eplug E (t_app (v_var x) v))
| c_open_c : ∀ α (v : value Σ), class (p_cont α v).

Definition classify {Σ : VSig} (p : program Σ) : class p.
Proof.
  destruct p as [ t | α t ].
  + destruct (classify_term t) as [ t c | v | F x v ].
    - destruct (c ectx_hole) as [ p Hred ].
      apply (c_step t p Hred).
    - apply c_result.
    - change (class (eplug ectx_hole (rplug F (t_app (v_var x) v)))).
      rewrite <- eplug_rcomp.
      apply c_open_v.
  + destruct (classify_term t) as [ t c | v | F x v ].
    - destruct (c (ectx_cont α)) as [ p Hred ].
      apply (c_step (p_cont α t) p Hred).
    - apply c_open_c.
    - change (class (eplug (ectx_cont α) (rplug F (t_app (v_var x) v)))).
      rewrite <- eplug_rcomp.
      apply c_open_v.
Defined.