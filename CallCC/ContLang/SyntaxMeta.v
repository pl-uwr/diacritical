(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This files contains administrative lemmas related to the binding     *)
(* representation chosen for this formalization.                        *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib Binding.Auto.
Require Import CallCC.ContLang.Syntax.

Lemma pmap_eplug {A B : VSig} (Φ : VMap A B) (E : ectx A) (t : term A) :
  pmap Φ (eplug E t) = eplug (emap Φ E) (tmap Φ t).
Proof.
revert t; induction E; simpl; intro; try (rewrite IHE); reflexivity.
Qed.

Lemma pbind_eplug {A B : VSig} (Φ : VSub A B) (E : ectx A) (t : term A) :
  pbind Φ (eplug E t) = eplug (ebind Φ E) (tbind Φ t).
Proof.
revert t; induction E; simpl; intro; try (rewrite IHE); reflexivity.
Qed.

Hint Rewrite -> @pmap_eplug  : term_simpl_raw.
Hint Rewrite -> @pbind_eplug : term_simpl_raw.

Instance Arrow_VMap : Arrow VMap.
Proof.
split.
+ intros; split; reflexivity.
+ intros A B f g Heq; split; symmetry; apply Heq.
+ intros A B f g h Heq1 Heq2; split;
    etransitivity; (apply Heq1 || apply Heq2).
+ intros A B f; split; reflexivity.
+ intros A B f; split; reflexivity.
Qed.

Instance PreSubst_VSub : PreSubst VSub.
Proof.
split.
+ intros; split; reflexivity.
+ intros A B f g Heq; split; symmetry; apply Heq.
+ intros A B f g h Heq1 Heq2; split;
    etransitivity; (apply Heq1 || apply Heq2).
Qed.

Instance ALiftable_incV : ALiftable VMap incV.
Proof.
split.
+ intros A f Heq; split.
  - intros [ | x ]; simpl; [ reflexivity | ].
    apply f_equal, Heq.
  - intro α; simpl; apply Heq.
+ intros A B C f g h Heq; split.
  - intros [ | x ]; simpl; [ reflexivity | ].
    apply f_equal, Heq.
  - intro α; simpl; apply Heq.
Qed.

Fixpoint tmap_id {A : VSig} (f : VMap A A) (t : term A) :
  arrow_eq f arrow_id → tmap f t = t
with vmap_id {A : VSig} (f : VMap A A) (v : value A) :
  arrow_eq f arrow_id → vmap f v = v
with pmap_id {A : VSig} (f : VMap A A) (p : program A) :
  arrow_eq f arrow_id → pmap f p = p.
Proof.
+ auto_map_id.
+ auto_map_id.
+ auto_map_id.
Qed.

Fixpoint tmap_map_comp {A B C : VSig}
  (f : VMap B C) (g : VMap A B) h (t : term A) :
  arrow_eq (arrow_comp f g) h → tmap f (tmap g t) = tmap h t
with vmap_map_comp {A B C : VSig}
  (f : VMap B C) (g : VMap A B) h (v : value A) :
  arrow_eq (arrow_comp f g) h → vmap f (vmap g v) = vmap h v
with pmap_map_comp {A B C : VSig}
  (f : VMap B C) (g : VMap A B) h (p : program A) :
  arrow_eq (arrow_comp f g) h → pmap f (pmap g p) = pmap h p.
Proof.
+ auto_map_map_comp.
+ auto_map_map_comp.
+ auto_map_map_comp.
Qed.

Instance FMap_vmap : FMap (@vmap).
Proof.
split.
+ exact @vmap_id.
+ exact @vmap_map_comp.
Qed.

Instance FMap_tmap : FMap (@tmap).
Proof.
split.
+ exact @tmap_id.
+ exact @tmap_map_comp.
Qed.

Instance FMap_pmap : FMap (@pmap).
Proof.
split.
+ exact @pmap_id.
+ exact @pmap_map_comp.
Qed.

Fixpoint emap_id {A : VSig} (f : VMap A A) (E : ectx A) :
  arrow_eq f arrow_id → emap f E = E.
Proof. auto_map_id. Qed.

Fixpoint emap_map_comp {A B C : VSig}
  (f : VMap B C) (g : VMap A B) h (E : ectx A) :
  arrow_eq (arrow_comp f g) h → emap f (emap g E) = emap h E.
Proof. auto_map_map_comp. Qed.

Instance FMap_emap : FMap (@emap).
Proof.
split.
+ exact @emap_id.
+ exact @emap_map_comp.
Qed.

Instance LiftAShift_VMap_incV : LiftAShift VMap incV.
Proof. split; reflexivity. Qed.

Instance LiftAShift_VMap_incC : LiftAShift VMap incC.
Proof. split; reflexivity. Qed.

Instance ASLiftable_incV : ASLiftable VMap VSub incV.
Proof.
split.
+ intros [ | x ]; [ reflexivity | ].
  auto_ASLiftable.
+ auto_ASLiftable.
Qed.

Hint Rewrite -> @pmap_eplug : auto_bind_map_comp.

Fixpoint tbind_map_comp {A B B' C : VSig}
  (f : VSub B C) (g : VMap A B) (g' : VMap B' C) (f' : VSub A B')
  (t : term A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  tbind f (tmap g t) = tmap g' (tbind f' t)
with vbind_map_comp {A B B' C : VSig}
  (f : VSub B C) (g : VMap A B) (g' : VMap B' C) (f' : VSub A B')
  (v : value A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  vbind f (vmap g v) = vmap g' (vbind f' v)
with pbind_map_comp {A B B' C : VSig}
  (f : VSub B C) (g : VMap A B) (g' : VMap B' C) (f' : VSub A B')
  (p : program A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  pbind f (pmap g p) = pmap g' (pbind f' p).
Proof.
+ auto_bind_map_comp.
+ auto_bind_map_comp.
+ auto_bind_map_comp.
Qed.

Instance BindMap_vmap_vbind : BindMap (@vmap) (@vbind).
Proof.
split.
+ exact @vbind_map_comp.
Qed.

Instance BindMap_tmap_tbind : BindMap (@tmap) (@tbind).
Proof.
split.
+ exact @tbind_map_comp.
Qed.

Instance BindMap_pmap_pbind : BindMap (@pmap) (@pbind).
Proof.
split.
+ exact @pbind_map_comp.
Qed.

Fixpoint ebind_map_comp {A B B' C : VSig}
  (f : VSub B C) (g : VMap A B) (g' : VMap B' C) (f' : VSub A B')
  (E : ectx A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  ebind f (emap g E) = emap g' (ebind f' E).
Proof.
auto_bind_map_comp.
Qed.

Instance BindMap_emap_ebind : BindMap (@emap) (@ebind).
Proof.
split.
+ exact @ebind_map_comp.
Qed.

Instance SLiftable_incV : SLiftable VMap VSub incV.
Proof.
split.
+ intros A f Heq; split.
  - intros [ | x ]; simpl; [ reflexivity | ].
    rewrite (proj1 Heq); reflexivity.
  - intro; simpl; rewrite (proj2 Heq); reflexivity.
+ intros A B C f g h Heq; split.
  - intros [ | x ]; simpl; [ reflexivity | ].
    rewrite <- (proj1 Heq); simpl.
    apply bind_map_comp; split; reflexivity.
  - intro; simpl; rewrite <- (proj2 Heq); simpl.
    apply bind_map_comp; split; reflexivity.
Qed.

Fixpoint tbind_pure {A : VSig}
  (f : VSub A A) (t : term A) : subst_eq f subst_pure → tbind f t = t
with vbind_pure {A : VSig}
  (f : VSub A A) (v : value A) : subst_eq f subst_pure → vbind f v = v
with pbind_pure {A : VSig}
  (f : VSub A A) (p : program A) : subst_eq f subst_pure → pbind f p = p.
Proof.
+ auto_bind_pure.
+ auto_bind_pure.
+ auto_bind_pure.
  rewrite (proj2 Heq); reflexivity.
Qed.

Hint Rewrite -> @pbind_eplug : auto_bind_bind_comp.

Fixpoint tbind_bind_comp {A B C : VSig}
  (f : VSub B C) (g : VSub A B) h (t : term A) :
  subst_eq (subst_comp f g) h → tbind f (tbind g t) = tbind h t
with vbind_bind_comp {A B C : VSig}
  (f : VSub B C) (g : VSub A B) h (v : value A) : 
  subst_eq (subst_comp f g) h → vbind f (vbind g v) = vbind h v
with pbind_bind_comp {A B C : VSig}
  (f : VSub B C) (g : VSub A B) h (p : program A) : 
  subst_eq (subst_comp f g) h → pbind f (pbind g p) = pbind h p.
Proof.
+ auto_bind_bind_comp.
+ auto_bind_bind_comp.
+ auto_bind_bind_comp.
Qed.

Instance Bind_vbind : Bind (@vbind).
Proof.
split.
+ exact @vbind_pure.
+ exact @vbind_bind_comp.
Qed.

Instance Bind_tbind : Bind (@tbind).
Proof.
split.
+ exact @tbind_pure.
+ exact @tbind_bind_comp.
Qed.

Instance Bind_pbind : Bind (@pbind).
Proof.
split.
+ exact @pbind_pure.
+ exact @pbind_bind_comp.
Qed.

Fixpoint ebind_pure {A : VSig}
  (f : VSub A A) (E : ectx A) : subst_eq f subst_pure → ebind f E = E.
Proof.
auto_bind_pure.
Qed.

Fixpoint ebind_bind_comp {A B C : VSig}
  (f : VSub B C) (g : VSub A B) h (E : ectx A) :
  subst_eq (subst_comp f g) h → ebind f (ebind g E) = ebind h E.
Proof.
auto_bind_bind_comp.
Qed.

Instance Bind_ebind : Bind (@ebind).
Proof.
split.
+ exact @ebind_pure.
+ exact @ebind_bind_comp.
Qed.

Instance Subst_VMap_VSub : Subst VMap VSub.
Proof.
split.
+ intros; split; simpl; intro; apply map_id; split; reflexivity.
+ intros; split; simpl; intro; apply bind_pure; split; reflexivity.
+ intros; split; reflexivity.
Qed.

Instance LiftSShift_incV : LiftSShift VMap VSub incV.
Proof.
split; reflexivity.
Qed.

Instance SubstShift_value_incV : SubstShift VMap VSub value incV.
Proof.
split; reflexivity.
Qed.

Instance SubstShift_ectx_incC_ectx : SubstShift VMap VSub ectx incC.
Proof.
split; reflexivity.
Qed.

Instance SubstFMap_value_incV : SubstFMap VMap VSub value incV.
Proof.
split.
+ intros [ | x ]; reflexivity.
+ reflexivity.
Qed.

Instance SubstFMap_ectx_incC : SubstFMap VMap VSub ectx incC.
Proof.
split.
+ reflexivity.
+ intros [ | α ]; reflexivity.
Qed.

Instance SubstBind_value_incV : SubstBind VMap VSub value incV.
Proof.
split.
+ intros [ | x ]; term_simpl; reflexivity.
+ intros; term_simpl; reflexivity.
Qed.

(* ========================================================================= *)

Lemma liftA_incV_shift_incC_indep
  (F : VSig → Type) {map : ∀ A B : VSig, VMap A B → F A → F B}
  {MapF : IsMapCore map} {MF : FMap map}
  (Σ : VSig) (t : F (incV Σ)) :
  fmap (liftA (G:=incV) (mk_shift (Inc:=incC))) t = shift (Inc:=incC) t.
Proof.
apply map_equiv; split.
+ intros [ | x ]; reflexivity.
+ reflexivity.
Qed.

Lemma liftA_incC_shift_incV_indep
  (F : VSig → Type) {map : ∀ A B : VSig, VMap A B → F A → F B}
  {MapF : IsMapCore map} {MF : FMap map}
  (Σ : VSig) (t : F (incC Σ)) :
  fmap (liftA (G:=incC) (mk_shift (Inc:=incV))) t = shift (Inc:=incV) t.
Proof.
apply map_equiv; split.
+ reflexivity.
+ intros [ | α ]; reflexivity.
Qed.

Hint Rewrite -> (liftA_incV_shift_incC_indep _ (MF:=FMap_tmap)) : term_simpl.
Hint Rewrite -> (liftA_incC_shift_incV_indep _ (MF:=FMap_vmap)) : term_simpl.
Hint Rewrite -> (liftA_incC_shift_incV_indep _ (MF:=FMap_emap)) : term_simpl.

Lemma shiftCV_norm
  (F : VSig → Type) {map : ∀ A B : VSig, VMap A B → F A → F B}
  {MapF : IsMapCore map} {MF : FMap map}
  (Σ : VSig) (t : F Σ) :
  shift (Inc:=incV) (map:=map) (shift (Inc:=incC) (map:=map) t) =
  shift (Inc:=incC) (map:=map) (shift (Inc:=incV) (map:=map) t).
Proof.
unfold shift; rewrite map_map_comp', map_map_comp'; reflexivity.
Qed.

Hint Rewrite -> (shiftCV_norm _ (MF:=FMap_vmap)) : term_simpl.
Hint Rewrite -> (shiftCV_norm _ (MF:=FMap_tmap)) : term_simpl.