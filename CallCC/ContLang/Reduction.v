(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains a formalization of the reduction semantics        *)
(* of Section 5.1, in the presence of context variables (Section 5.2).  *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib.
Require Import CallCC.ContLang.Syntax.

Inductive red_in {Σ : VSig} (E : ectx Σ) : term Σ → program Σ → Prop :=
| red_beta   : ∀ (t : term (incV Σ)) (v : value Σ),
    red_in E (t_app (v_lam t) v) (eplug E (subst t v))
| red_callcc : ∀ (v : value Σ),
    red_in E (t_app v_callcc v) (eplug E (t_app v
      (v_lam (t_abort (eplug (shift E) (v_var (VZ : Var (incV _))))))))
| red_abort  : ∀ t,
    red_in E (t_abort t) t
| red_app1   : ∀ t s r,
    red_in (ectx_app1 E s) t r →
    red_in E (t_app t s) r
| red_app2   : ∀ v t r,
    red_in (ectx_app2 v E) t r →
    red_in E (t_app v t) r
.

Lemma red_beta' {Σ : VSig} {E : ectx Σ} {t : term (incV Σ)} {v : value Σ}
    {p : program Σ} :
  p = eplug E (subst t v) → red_in E (t_app (v_lam t) v) p.
Proof.
intro; subst; apply red_beta.
Qed.

Lemma red_callcc' {Σ : VSig} {E : ectx Σ} {v : value Σ} {p : program Σ} :
  p = (eplug E (t_app v
        (v_lam (t_abort (eplug (shift E) (v_var (VZ : Var (incV _)))))))) →
  red_in E (t_app v_callcc v) p.
Proof.
intro; subst; apply red_callcc.
Qed.

Definition red {Σ : VSig} (p₁ p₂ : program Σ) : Prop :=
  match p₁ with
  | p_term t₁   => red_in ectx_hole t₁ p₂
  | p_cont α t₁ => red_in (ectx_cont α) t₁ p₂
  end.

Require Import Relations.
Require Import RelationClasses.

Notation red_rtc p₁ p₂ := (clos_refl_trans_1n _ (@red _) p₁ p₂).

Instance Reflexive_red_rtc {Σ : VSig} :
  Reflexive (clos_refl_trans_1n _ (@red Σ)).
Proof.
constructor 1.
Qed.

Instance Transitive_red_rtc {Σ : VSig} :
  Transitive (clos_refl_trans_1n _ (@red Σ)).
Proof.
intros p₁ p₂ p₃; induction 1; [ auto | ]; intro H3.
econstructor 2; [ eassumption | ]; auto.
Qed.