(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file gathers and exports the notions related to the syntax      *)
(* and reduction semantics of the calculus with context variables       *)
(* (Section 5.2).                                                       *)
(************************************************************************)

Require Export Binding.Lib.

Require Export CallCC.ContLang.Classify.
Require Export CallCC.ContLang.Embed.
Require Export CallCC.ContLang.Reduction.
Require Export CallCC.ContLang.ReductionProperties.
Require Export CallCC.ContLang.Syntax.
Require Export CallCC.ContLang.SyntaxMeta.