(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file formalizes some basic properties of the reduction          *)
(* relation, tacitly assumed in Section 5.                              *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib.
Require Import CallCC.ContLang.Syntax.
Require Import CallCC.ContLang.SyntaxMeta.
Require Import CallCC.ContLang.Reduction.

Lemma red_rtc1 {Σ : VSig} (p₁ p₂ : program Σ) :
  red p₁ p₂ → red_rtc p₁ p₂.
Proof.
  intro Hred; econstructor 2; [ exact Hred | reflexivity ].
Qed.

Lemma eplug_value {Σ : VSig} (E : ectx Σ) (t : term Σ) (v : value Σ) :
  eplug E t = v → E = ectx_hole.
Proof.
  revert t; induction E as [ | α | E IHE s | u E IHE ]; simpl; intros t HE.
  + reflexivity.
  + discriminate.
  + rewrite (IHE _ HE) in HE; discriminate.
  + rewrite (IHE _ HE) in HE; discriminate.
Qed.

Lemma eplug_not_value {Σ : VSig} (E : ectx Σ) (t : term Σ) α (v : value Σ) :
  (∀ u : value Σ, t ≠ u) → eplug E t ≠ p_cont α v.
Proof.
  revert t; induction E as [ | β | E IHE s | u E IHE ]; intros t Ht; simpl.
  + discriminate.
  + injection; firstorder.
  + apply IHE; discriminate.
  + apply IHE; discriminate.
Qed.

Lemma eplug_not_result {Σ : VSig} (E : ectx Σ) (t : term Σ) (v : value Σ) :
  (∀ u : value Σ, t ≠ u) → eplug E t ≠ v.
Proof.
  revert t; induction E as [ | α | E IHE s | u E IHE ]; intros t Ht; simpl.
  + injection; firstorder.
  + discriminate.
  + apply IHE; discriminate.
  + apply IHE; discriminate.
Qed.

Inductive rctx (Σ : VSig) : Type :=
| rctx_hole : rctx Σ
| rctx_app1 : rctx Σ → term Σ → rctx Σ
| rctx_app2 : value Σ → rctx Σ → rctx Σ.

Arguments rctx_hole {Σ}.
Arguments rctx_app1 {Σ}.
Arguments rctx_app2 {Σ}.

Fixpoint rplug {Σ : VSig} (F : rctx Σ) (t : term Σ) : term Σ :=
  match F with
  | rctx_hole     => t
  | rctx_app1 E s => t_app (rplug E t) s
  | rctx_app2 v E => t_app v (rplug E t)
  end.

Fixpoint rcomp {Σ : VSig} (E : ectx Σ) (F : rctx Σ) : ectx Σ :=
  match F with
  | rctx_hole     => E
  | rctx_app1 F t => rcomp (ectx_app1 E t) F
  | rctx_app2 v F => rcomp (ectx_app2 v E) F
  end.

Lemma eplug_rcomp {Σ : VSig} (E : ectx Σ) F (t : term Σ) :
  eplug (rcomp E F) t = eplug E (rplug F t).
Proof.
  revert E; induction F as [ | F IHF s | v F IHF ]; simpl; intro E.
  + reflexivity.
  + rewrite IHF; reflexivity.
  + rewrite IHF; reflexivity.
Defined.

Lemma ectx_rev {Σ : VSig} (E : ectx Σ) :
  (∃ F : rctx Σ, E = rcomp ectx_hole F) ∨
  (∃ α F, E = rcomp (ectx_cont α) F).
Proof.
  assert (∀ F',
             (∃ F : rctx Σ, rcomp E F' = rcomp ectx_hole F) ∨
             (∃ α F, rcomp E F' = rcomp (ectx_cont α) F)).
  { induction E as [ | α | E IHE t | v E IHE ]; intro F'; simpl.
    + left; exists F'; reflexivity.
    + right; exists α; exists F'; reflexivity.
    + apply (IHE (rctx_app1 F' t)).
    + apply (IHE (rctx_app2 v F')).
  }
  apply (H rctx_hole).
Qed.

Lemma open_stuck_r_is_stuck {Σ : VSig}
    (E : ectx Σ) (F : rctx Σ) (x : Var Σ) (v : value Σ) p :
  red_in E (rplug F (t_app (v_var x) v)) p → False.
Proof.
  revert E; induction F as [ | F IHF t | u F IHF ]; intro E; simpl.
  + intro H.
    repeat match goal with
           | H: red_in _ _ _ |- _ => inversion_clear H
           end.
  + intro H; inversion H; clear H; subst.
    - destruct F; discriminate.
    - destruct F; discriminate.
    - eapply IHF; eassumption.
    - destruct F; discriminate.
 + intro H; inversion H; clear H; subst.
    - destruct F; discriminate.
    - destruct F; discriminate.
    - match goal with
      | H: red_in _ _ _ |- _ => inversion_clear H
      end.
    - eapply IHF; eassumption.
Qed.

Lemma open_stuck_is_stuck {Σ : VSig} (E : ectx Σ) (x : Var Σ) (v : value Σ) p :
  red (eplug E (t_app (v_var x) v)) p → False.
Proof.
  unfold red.
  destruct (ectx_rev E) as [ [ F Heq ] | [ α [ F Heq ] ] ]; subst;
    rewrite eplug_rcomp; simpl.
  + apply open_stuck_r_is_stuck.
  + apply open_stuck_r_is_stuck.
Qed.

Lemma open_stuck_r_unique {Σ : VSig}
    (F₁ F₂ : rctx Σ) (x₁ x₂ : Var Σ) (v₁ v₂ : value Σ) :
  rplug F₁ (t_app (v_var x₁) v₁) = rplug F₂ (t_app (v_var x₂) v₂) →
  F₁ = F₂ ∧ x₁ = x₂ ∧ v₁ = v₂.
Proof.
  revert F₂; induction F₁ as [ | F₁ IHF₁ t | v F₁ IHF₁ ]; simpl; intros F₂ Heq.
  + destruct F₂ as [ | F₂ t' | v' F₂ ]; simpl in Heq.
    - injection Heq as []; auto.
    - destruct F₂; discriminate.
    - destruct F₂; discriminate.
  + destruct F₂ as [ | F₂ t' | v' F₂ ]; simpl in Heq.
    - destruct F₁; discriminate.
    - injection Heq; clear Heq; intros Ht Heq.
      apply IHF₁ in Heq; destruct Heq as [ ? [ ] ]; subst; auto.
    - destruct F₁; discriminate.
  + destruct F₂ as [ | F₂ t' | v' F₂ ]; simpl in Heq.
    - destruct F₁; discriminate.
    - destruct F₂; discriminate.
    - injection Heq; clear Heq; intros Heq Hv.
      apply IHF₁ in Heq; destruct Heq as [ ? [ ] ]; subst; auto.
Qed.

Lemma open_stuck_unique {Σ : VSig}
    (E₁ E₂ : ectx Σ) (x₁ x₂ : Var Σ) (v₁ v₂ : value Σ) :
  eplug E₁ (t_app (v_var x₁) v₁) = eplug E₂ (t_app (v_var x₂) v₂) →
  E₁ = E₂ ∧ x₁ = x₂ ∧ v₁ = v₂.
Proof.
  destruct (ectx_rev E₁) as [ [ F₁ Heq₁ ] | [ α₁ [ F₁ Heq₁ ] ] ]; subst;
    destruct (ectx_rev E₂) as [ [ F₂ Heq₂ ] | [ α₂ [ F₂ Heq₂ ] ] ]; subst;
      intro Heq; repeat rewrite eplug_rcomp in Heq; simpl in Heq.
  + injection Heq; clear Heq; intro Heq.
    apply open_stuck_r_unique in Heq.
    destruct Heq as [ ? [] ]; subst; auto.
  + discriminate.
  + discriminate.
  + injection Heq; clear Heq; intros Heq Hα.
    apply open_stuck_r_unique in Heq.
    destruct Heq as [ ? [] ]; subst; auto.
Qed.

Ltac discr_red :=
  match goal with
  | H: red_in _ (t_value _) _ |- _ => inversion H
  end.

Lemma red_in_determ {Σ : VSig} {E : ectx Σ} {t} {p₁ p₂ : program Σ} :
  red_in E t p₁ → red_in E t p₂ → p₁ = p₂.
Proof.
  induction 1 as [ E t v | E v | E p | E t s p₁ Hred₁ IH | E v t p₁ Hred₁ IH ].
  + inversion 1; try discr_red.
    reflexivity.
  + inversion 1; try discr_red.
    reflexivity.
  + inversion 1; reflexivity.
  + inversion 1; subst; try discr_red.
    auto.
  + inversion 1; subst; try discr_red.
    auto.
Qed.

Lemma red_determ {Σ : VSig} {p p₁ p₂ : program Σ} :
  red p p₁ → red p p₂ → p₁ = p₂.
Proof.
  destruct p; unfold red; apply red_in_determ.
Qed.

Lemma red_eplug {Σ : VSig} (E : ectx Σ) t p :
  red_in E t p → red (eplug E t) p.
Proof.
  revert t; induction E as [ | α | E IHE s | v E IHE ]; simpl; intros t Hred.
  + assumption.
  + assumption.
  + apply IHE, red_app1; assumption.
  + apply IHE, red_app2; assumption.
Qed.

Lemma red_in_fmap {Σ Δ : VSig} (Φ : VMap Σ Δ) {E t p} :
  red_in E t p → red_in (fmap Φ E) (fmap Φ t) (fmap Φ p).
Proof.
  induction 1 as [ E t v | E v | E p | E t s p₁ Hred₁ IH | E v t p₁ Hred₁ IH ];
    term_simpl.
  + apply red_beta.
  + apply red_callcc.
  + apply red_abort.
  + apply red_app1; assumption.
  + apply red_app2; assumption.
Qed.

Lemma red_in_bind {Σ Δ : VSig} (Φ : VSub Σ Δ) {E t p} :
  red_in E t p → red_in (bind Φ E) (bind Φ t) (bind Φ p).
Proof.
  induction 1 as [ E t v | E v | E p | E t s p₁ Hred₁ IH | E v t p₁ Hred₁ IH ];
    term_simpl.
  + apply red_beta.
  + apply red_callcc.
  + apply red_abort.
  + apply red_app1; assumption.
  + apply red_app2; assumption.
Qed.

Lemma red_fmap {Σ Δ : VSig} (Φ : VMap Σ Δ) {p p' : program Σ} :
  red p p' → red (fmap Φ p) (fmap Φ p').
Proof.
  destruct p as [ t | α t ]; unfold red; term_simpl.
  + apply (red_in_fmap Φ (E:=ectx_hole)).
  + apply (red_in_fmap Φ (E:=ectx_cont α)).
Qed.

Lemma red_bind {Σ Δ : VSig} (Φ : VSub Σ Δ) {p p' : program Σ} :
  red p p' → red (bind Φ p) (bind Φ p').
Proof.
  destruct p as [ t | α t ]; unfold red; term_simpl.
  + apply (red_in_bind Φ (E:=ectx_hole)).
  + intro Ht.
    apply (red_in_bind Φ) in Ht; term_simpl in Ht.
    apply red_eplug in Ht; assumption.
Qed.

Lemma red_substv {Σ : VSig} (v : value Σ) {p p' : program (incV Σ)} :
  red p p' → red (subst p v) (subst p' v).
Proof.
  apply red_bind.
Qed.

Lemma red_substc {Σ : VSig} (E : ectx Σ) {p p' : program (incC Σ)} :
  red p p' → red (subst p E) (subst p' E).
Proof.
  apply red_bind.
Qed.

Lemma red_rtc_fmap {Σ Δ : VSig} (Φ : VMap Σ Δ) {p p' : program Σ} :
  red_rtc p p' → red_rtc (fmap Φ p) (fmap Φ p').
Proof.
  induction 1; [ reflexivity | ].
  econstructor 2; [ | eassumption ].
  apply red_fmap; assumption.
Qed.

Lemma red_rtc_bind {Σ Δ : VSig} (Φ : VSub Σ Δ) {p p' : program Σ} :
  red_rtc p p' → red_rtc (bind Φ p) (bind Φ p').
Proof.
  induction 1; [ reflexivity | ].
  econstructor 2; [ | eassumption ].
  apply red_bind; assumption.
Qed.

Lemma red_rtc_substv {Σ : VSig} (v : value Σ) {p p' : program (incV Σ)} :
  red_rtc p p' → red_rtc (subst p v) (subst p' v).
Proof.
  apply red_rtc_bind.
Qed.

Lemma red_rtc_substc {Σ : VSig} (E : ectx Σ) {p p' : program (incC Σ)} :
  red_rtc p p' → red_rtc (subst p E) (subst p' E).
Proof.
  apply red_rtc_bind.
Qed.