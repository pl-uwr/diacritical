(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file provides na embedding of the language of Section 5.1       *)
(* in the extended syntax of Section 5.2.                               *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib Binding.Set.
Require Import CallCC.ContLang.Syntax.
Require Import CallCC.ContLang.Reduction.
Require CallCC.Lang.Syntax CallCC.Lang.Reduction.

Fixpoint embed {V : Set}
    (t : Syntax.term V) : term {| Var := V ; CVar := ∅ |} :=
  match t with
  | Syntax.t_value v   => embed_value v
  | Syntax.t_app   t s => t_app (embed t) (embed s)
  | Syntax.t_abort t   => t_abort (embed t)
  end
with embed_value {V : Set}
    (v : Syntax.value V) : value {| Var := V ; CVar := ∅ |} :=
  match v with
  | Syntax.v_var x  => v_var (x : Var {| Var := V ; CVar := ∅ |})
  | Syntax.v_lam t  =>
      v_lam (embed t : term (incV {| Var := V ; CVar := ∅ |}))
  | Syntax.v_callcc => v_callcc
  end.

Fixpoint embed_ectx {V : Set}
    (E : Syntax.ectx V) : ectx {| Var := V ; CVar := ∅ |} :=
  match E with
  | Syntax.ectx_hole     => ectx_hole
  | Syntax.ectx_app1 E t => ectx_app1 (embed_ectx E) (embed t)
  | Syntax.ectx_app2 v E => ectx_app2 (embed_value v) (embed_ectx E)
  end.

Lemma embed_eplug {V : Set} (E : Syntax.ectx V) (t : Syntax.term V) :
  p_term (embed (Syntax.eplug E t)) = eplug (embed_ectx E) (embed t).
Proof.
  revert t; induction E; intro; simpl; try rewrite IHE; reflexivity.
Qed.

Fixpoint embed_fmap {A B : Set} (f : A [→] B) (t : Syntax.term A) :
  embed (fmap f t) =
  fmap 
    {| map_v := f : Var {| Var := A |} → Var {| Var := B |} 
    ;  map_c := λ α, α
    |} (embed t)
with embed_fmap_value {A B : Set} (f : A [→] B) (v : Syntax.value A) :
  embed_value (fmap f v) =
  fmap 
    {| map_v := f : Var {| Var := A |} → Var {| Var := B |} 
    ;  map_c := λ α, α
    |} (embed_value v).
Proof.
  + destruct t as [ v | t₁ t₂ | t ]; simpl.
    - rewrite embed_fmap_value; reflexivity.
    - rewrite embed_fmap, embed_fmap; reflexivity.
    - rewrite embed_fmap; reflexivity.
  + destruct v as [ x | t | ]; simpl.
    - reflexivity.
    - rewrite embed_fmap; reflexivity.
    - reflexivity.
Qed.

Fixpoint embed_fmap_ectx {A B : Set} (f : A [→] B) (E : Syntax.ectx A) :
  embed_ectx (fmap f E) =
  fmap 
    {| map_v := f : Var {| Var := A |} → Var {| Var := B |} 
    ;  map_c := λ α, α
    |} (embed_ectx E).
Proof.
  destruct E as [ | E t | v E ]; simpl.
  + reflexivity.
  + rewrite embed_fmap_ectx, embed_fmap; reflexivity.
  + rewrite embed_fmap_value, embed_fmap_ectx; reflexivity.
Qed.

Lemma embed_shift_ectx {V : Set} (E : Syntax.ectx V) :
  embed_ectx (shift E) = shift (embed_ectx E).
Proof.
  apply embed_fmap_ectx.
Qed.

Fixpoint embed_bind {A B : Set} (f : A [⇒] B)
  (Φ : VSub {| Var := A ; CVar := ∅ |} {| Var := B ; CVar := ∅ |})
  (t : Syntax.term A) :
  (∀ x, embed_value (f x) = sub_v Φ x) →
  embed (bind f t) = bind Φ (embed t)
with embed_bind_value {A B : Set} (f : A [⇒] B)
  (Φ : VSub {| Var := A ; CVar := ∅ |} {| Var := B ; CVar := ∅ |})
  (v : Syntax.value A) :
  (∀ x, embed_value (f x) = sub_v Φ x) →
  embed_value (bind f v) = bind Φ (embed_value v).
Proof.
  + intro Hf; destruct t as [ v | t₁ t₂ | t ]; simpl.
    - rewrite (embed_bind_value _ _ _ Φ _ Hf); reflexivity.
    - rewrite (embed_bind _ _ _ Φ _ Hf).
      rewrite (embed_bind _ _ _ Φ _ Hf); reflexivity.
    - rewrite (embed_bind _ _ _ Φ _ Hf); reflexivity.
  + intro Hf; destruct v as [ x | t | ]; simpl.
    - apply Hf.
    - erewrite embed_bind; [ reflexivity | ].
      intros [ | x ]; simpl; [ reflexivity | ].
      rewrite <- Hf; apply embed_fmap_value.
    - reflexivity.
Qed.

Lemma embed_subst {V : Set} (t : Syntax.term (inc V)) (v : Syntax.value V) :
  embed (subst t v) = subst (embed t) (embed_value v).
Proof.
  apply embed_bind; intros [ | x ]; reflexivity.
Qed.

Lemma red_in_embed {V : Set} (E : Syntax.ectx V) (t : Syntax.term V) p :
  red_in (embed_ectx E) (embed t) p →
  ∃ t', p = embed t' ∧ Reduction.red_in E t t'.
Proof.
  remember (embed_ectx E) as E₀.
  remember (embed t) as t₀.
  intro Hred; revert E HeqE₀ t Heqt₀.
  induction Hred as [ E t v | E v | E t | E t s p Hred IH | E v t p Hred IH ];
    intros E₀ HE₀ t₀ Ht₀; subst.
  + destruct t₀ as [ | t₀ s₀ | ]; try discriminate; simpl in Ht₀.
    destruct t₀ as [ u₀ | | ]; try discriminate; simpl in Ht₀.
    destruct u₀ as [ | t₀ | ]; try discriminate; simpl in Ht₀.
    destruct s₀ as [ v₀ | | ]; try discriminate; simpl in Ht₀.
    injection Ht₀ as Ht₀ Hv₀; subst.
    eexists; split; [ | constructor 1 ].
    rewrite embed_eplug, embed_subst; reflexivity.
  + destruct t₀ as [ | t₀ s₀ | ]; try discriminate; simpl in Ht₀.
    destruct t₀ as [ u₀ | | ]; try discriminate; simpl in Ht₀.
    destruct u₀ as [ | | ]; try discriminate; simpl in Ht₀.
    destruct s₀ as [ v₀ | | ]; try discriminate; simpl in Ht₀.
    injection Ht₀ as Hv₀; subst.
    eexists; split; [ | constructor 2 ].
    rewrite embed_eplug; simpl.
    rewrite embed_eplug; simpl.
    rewrite embed_shift_ectx; reflexivity.
  + destruct t₀ as [ | | t₀ ]; try discriminate; simpl in Ht₀.
    injection Ht₀ as Ht₀; subst.
    eexists; split; [ | constructor 3 ].
    reflexivity.
  + destruct t₀ as [ | t₀ s₀ | ]; try discriminate; simpl in Ht₀.
    injection Ht₀ as Ht₀ Hs₀; subst.
    destruct (IH (Syntax.ectx_app1 _ _) eq_refl _ eq_refl) as [ t' [ Hp Hri ] ].
    exists t'; split; [ assumption | constructor 4; assumption ].
  + destruct t₀ as [ | s₀ t₀ | ]; try discriminate; simpl in Ht₀.
    destruct s₀ as [ v₀ | | ]; try discriminate; simpl in Ht₀.
    injection Ht₀ as Hv₀ Ht₀; subst.
    destruct (IH (Syntax.ectx_app2 _ _) eq_refl _ eq_refl) as [ t' [ Hp Hri ] ].
    exists t'; split; [ assumption | constructor 5; assumption ].
Qed.

Lemma red_embed {V : Set} (t : Syntax.term V) (p : program _) :
  red (embed t) p → ∃ t', p = embed t' ∧ Reduction.red t t'.
Proof.
  intro; apply red_in_embed; assumption.
Qed.

Lemma red_rtc_embed {V : Set} (t : Syntax.term V) (p : program _) :
  red_rtc (embed t) p → ∃ t', p = embed t' ∧ Reduction.red_rtc t t'.
Proof.
  remember (p_term (embed t)) as t₀; intro Hred.
  revert t Heqt₀; induction Hred as [ p | p₁ p₂ p₃ Hred Hred' IH ].
  + intros t' H; exists t'; split; [ assumption | constructor ].
  + intros t₁ H; subst.
    apply red_embed in Hred; destruct Hred as [ t₂ [ H₂ Hred ] ].
    destruct (IH _ H₂) as [ t₃ [ H₃ Hred₃ ] ].
    eexists; split; [ eassumption | ].
    econstructor; eassumption.
Qed.

Lemma embed_red_in {V : Set} (E : Syntax.ectx V) (t p : Syntax.term V) :
  Reduction.red_in E t p → red_in (embed_ectx E) (embed t) (embed p).
Proof.
  induction 1; simpl.
  + rewrite embed_eplug, embed_subst; constructor 1.
  + rewrite embed_eplug; simpl.
    rewrite embed_eplug, embed_shift_ectx; constructor 2.
  + constructor 3.
  + constructor 4; assumption.
  + constructor 5; assumption.
Qed.

Lemma embed_red {V : Set} (t₁ t₂ : Syntax.term V) :
  Reduction.red t₁ t₂ → red (embed t₁) (embed t₂).
Proof.
  intro; apply (embed_red_in Syntax.ectx_hole); assumption.
Qed.