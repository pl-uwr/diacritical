## An extensional normal-form bisimilarity for the CBV λ-calculus with call/cc and abort

This directory contains a formalization of Sections 5.1–5.3, and it is
structured as follows:

* The [Lang](Lang/) directory contains a formalization of the original
  calculus (Section 5.1), using the library [Binding](../Binding/).

* The [ContLang](ContLang/) directory contains a formalization of the
  the calculus extended with context variables (Section 5.2), using
  the library [Binding](../Binding/).

* The [Simulation](Simulation/) directory contains a formalization of
  the simulations for the lambda calculus with call/cc and abort
  extended with context variables (Sections 5.2 and 5.3), using the
  library [Diacritical](../Diacritical/).
