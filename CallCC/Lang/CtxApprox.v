(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file formalizes a notion of contextual approximation.           *)
(* Its symmetric closure yields the contextual equivalence relation     *)
(* mentioned in Section 5.                                              *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib Binding.Set.
Require Import CallCC.Lang.Syntax.
Require Import CallCC.Lang.Reduction.

Inductive ctx : Set → Type :=
| ctx_empty : ctx ∅
| ctx_map   : ∀ A B : Set, (A [→] B) → ctx B → ctx A
| ctx_app1  : ∀ V : Set, ctx V → term V → ctx V
| ctx_app2  : ∀ V : Set, term V → ctx V → ctx V
| ctx_abort : ∀ V : Set, ctx V → ctx V
| ctx_lam   : ∀ V : Set, ctx V → ctx (inc V).

Arguments ctx_map   {A B} f C.
Arguments ctx_app1  {V}   C t.
Arguments ctx_app2  {V}   t C.
Arguments ctx_abort {V}   C.
Arguments ctx_lam   {V}   C.

Fixpoint plug {V : Set} (C : ctx V) : term V → term ∅ :=
  match C in ctx V return term V → term ∅ with
  | ctx_empty     => λ t, t
  | ctx_map   f C => λ t, plug C (fmap f t)
  | ctx_app1  C s => λ t, plug C (t_app t s)
  | ctx_app2  s C => λ t, plug C (t_app s t)
  | ctx_abort C   => λ t, plug C (t_abort t)
  | ctx_lam   C   => λ t, plug C (v_lam t)
  end.

Definition ctx_approx {V : Set} (t₁ t₂ : term V) : Prop :=
  ∀ (C : ctx V) (v₁ : value ∅),
    red_rtc (plug C t₁) v₁ →
  ∃ v₂ : value ∅, red_rtc (plug C t₂) v₂.