(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains a formalization                                   *)
(* of the reduction semantics of Section 5.1.                           *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib.
Require Import CallCC.Lang.Syntax.

Inductive red_in {V : Set} (E : ectx V) : term V → term V → Prop :=
| red_beta   : ∀ (t : term (inc V)) (v : value V),
    red_in E (t_app (v_lam t) v) (eplug E (subst t v))
| red_callcc : ∀ (v : value V),
    red_in E (t_app v_callcc v) (eplug E (t_app v
      (v_lam (t_abort (eplug (shift E) (v_var VZ))))))
| red_abort  : ∀ t,
    red_in E (t_abort t) t
| red_app1   : ∀ t s r,
    red_in (ectx_app1 E s) t r →
    red_in E (t_app t s) r
| red_app2   : ∀ v t r,
    red_in (ectx_app2 v E) t r →
    red_in E (t_app v t) r.

Definition red {V : Set} : term V → term V → Prop :=
  red_in ectx_hole.

Require Import Relations.
Require Import RelationClasses.

Definition red_rtc {V : Set} := (clos_refl_trans_1n _ (@red V)).

Instance Transitive_red_rtc {V : Set} : Transitive (@red_rtc V).
Proof.
  unfold Transitive, red_rtc.
  induction 1; [ auto | ].
  intro; econstructor 2; [ eassumption | ]; auto.
Qed.