(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file shows that any simulation is a precongruence               *)
(* (a corollary of Theorem 5.8).                                        *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import CallCC.Simulation.Core.
Require Import CallCC.Simulation.UpToTechniques.Lib.
Require Import CallCC.ContLang.Lib.
Require Import CallCC.Lang.CtxApprox.

Require CallCC.Lang.Reduction.

(** The combo up-to technique is a precongruence. *)

Lemma combo_precongruence {V : Set}
  (C : ctx V) (t₁ t₂ : Syntax.term V) (R : DiRel elem) :
  term0_rel (combo R) t₁ t₂ →
  program_rel (combo R) (embed (plug C t₁)) (embed (plug C t₂)).
Proof.
  induction C; intro Ht; simpl.
  + apply combo_program; assumption.
  + apply IHC; take rename; apply rename_term0; assumption.
  + apply IHC; term_simpl.
    apply combo_app; [ assumption | ].
    take refl; constructor.
  + apply IHC; term_simpl.
    apply combo_app; [ | assumption ].
    take refl; constructor.
  + apply IHC; term_simpl.
    take abort; term_simpl; constructor.
    apply combo_program; take rename; apply rename_term; assumption.
  + apply IHC; term_simpl.
    take lam; term_simpl; constructor.
    take rename; apply rename_term; assumption.
Qed.

Lemma precongruence {V : Set}
  (C : ctx V) (t₁ t₂ : Syntax.term V) (R : DiRel elem) :
  DiSimulation R →
  term0_rel R t₁ t₂ →
  program_rel (combo R) (embed (plug C t₁)) (embed (plug C t₂)).
Proof.
  intros Hsim Ht; apply combo_precongruence.
  take_done; assumption.
Qed.

(** Any simulation is sound wrt contextual approximation. *)

Lemma adequacy {V : Set}
  (t₁ t₂ : Syntax.term V) (v₁ : Syntax.value V) (R : DiRel elem) :
  DiSimulation R → program_rel R (embed t₁) (embed t₂) →
  Reduction.red_rtc t₁ (Syntax.t_value v₁) →
  ∃ v₂ : Syntax.value V, Reduction.red_rtc t₂ (Syntax.t_value v₂).
Proof.
  intros Hsim Ht Hred; revert Ht.
  remember (Syntax.t_value v₁) as s₁ eqn: Heq; revert Heq t₂.
  induction Hred as [ s₁ | t t' t''  Hred Hred' IH ]; intros Heq t₂ Ht; subst.
  + simpl in Ht; apply Hsim in Ht.
    apply (progress_rel Hsim) in Ht; simpl in Ht.
    destruct Ht as [ v₂ Hv₂ ].
    apply red_rtc_embed in Hv₂; destruct Hv₂ as [ t' [ Ht' Hred' ] ].
    destruct t' as [ v' | | ]; try discriminate.
    exists v'; assumption.
  + apply embed_red in Hred.
    apply (progress_rel Hsim) in Ht.
    eapply progress_step in Ht; [ | eassumption ].
    destruct Ht as [ p₂ [ Hp₂ Ht' ] ].
    apply red_rtc_embed in Hp₂; destruct Hp₂ as [ t₂' [ Hp₂ Hred₂ ] ]; subst.
    specialize (IH eq_refl t₂' Ht'); destruct IH as [ v₂ Hredv ].
    exists v₂; etransitivity; eassumption.
Qed.

Theorem soundness {V : Set} (R : DiRel elem) (t₁ t₂ : Syntax.term V) :
  DiSimulation R → term0_rel R t₁ t₂ → ctx_approx t₁ t₂.
Proof.
  intros Hsim Ht C v₁ Hred.
  eapply adequacy; [ | apply precongruence; eassumption | eassumption ].
  apply (combo_evolution _ _); assumption.
Qed.