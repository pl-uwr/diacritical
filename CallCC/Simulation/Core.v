(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file defines the notion of diacritical progress for             *)
(* the CBV λ-calculus with call/cc and abort (Definition 5.4).          *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib.
Require Import CallCC.ContLang.Lib.
Require Import Diacritical.Core.

Inductive elem : Type :=
| mk_elem : ∀ Σ : VSig, program Σ → program Σ → elem.

Notation program_rel R p₁ p₂ := (R (mk_elem _ p₁ p₂)).

Definition term_rel (R : DiRel elem) {Σ : VSig} (t₁ t₂ : term Σ) : Prop :=
  program_rel R
    (p_cont (VZ : CVar (incC Σ)) (shift t₁))
    (p_cont (VZ : CVar (incC Σ)) (shift t₂)).

Definition value_rel (R : DiRel elem) {Σ : VSig} (v₁ v₂ : value Σ) : Prop :=
  term_rel R
    (t_app (shift v₁ : value (incV Σ)) (v_var (VZ : Var (incV Σ))))
    (t_app (shift v₂ : value (incV Σ)) (v_var (VZ : Var (incV Σ)))).

Definition ectx_rel (R : DiRel elem) {Σ : VSig} (E₁ E₂ : ectx Σ) : Prop :=
  program_rel R
    (eplug (shift E₁ : ectx (incV Σ)) (v_var (VZ : Var (incV Σ))))
    (eplug (shift E₂ : ectx (incV Σ)) (v_var (VZ : Var (incV Σ)))).

Notation term0_rel R t₁ t₂ := (term_rel R (embed t₁) (embed t₂)).

Instance DiElemProgressCore_elem : DiElemProgressCore elem :=
  { elem_progress := λ Q S e,
    match e with
    | mk_elem Σ p₁ p₂ =>
      match classify p₁ with
      | c_step _ p₁' _ =>
        ∃ p₂', red_rtc p₂ p₂' ∧ program_rel S p₁' p₂'
      | c_result v₁ =>
        ∃ (v₂ : value Σ), red_rtc p₂ v₂
      | c_open_v E₁ x v₁ =>
        ∃ (E₂ : ectx Σ) (v₂ : value Σ),
        red_rtc p₂ (eplug E₂ (t_app (v_var x) v₂)) ∧
        ectx_rel S E₁ E₂ ∧ value_rel S v₁ v₂
      | c_open_c α v₁ =>
        ∃ (v₂ : value Σ), red_rtc p₂ (p_cont α v₂) ∧ value_rel Q v₁ v₂
      end
    end
  }.

Instance DiElemProgress_elem : DiElemProgress elem.
Proof.
  split.
  + intros Q₁ Q₂ S₁ S₂ HQ HS [ Σ p₁ p₂ ]; simpl.
    destruct (classify p₁) as [ p₁ p₁' Hred₁ | v₁ | E₁ x v₁ | α v₁ ].
    - intros [ p₂' [ Hred₂ Hp' ] ].
      exists p₂'; split; auto.
    - intros [ v₂ Hred₂ ].
      exists v₂; assumption.
    - intros [ E₂ [ v₂ [ Hred₂ [ HE Hv ] ] ] ].
      exists E₂; exists v₂; split; [ assumption | split ]; apply HS; assumption.
    - intros [ v₂ [ Hred₂ Hv ] ].
      exists v₂; split; [ assumption | ]; apply HQ; assumption.
Qed.

Lemma elem_progress_red_rtc_r {Σ : VSig} (p₁ p₂ p₂' : program Σ)
  (Q S : DiRel elem) :
  red_rtc p₂ p₂' →
  elem_progress Q S (mk_elem _ p₁ p₂') →
  elem_progress Q S (mk_elem _ p₁ p₂).
Proof.
  simpl; intros Hred₂ Hpr.
  destruct (classify p₁) as [ p₁ p₁' Hred₁ | v₁ | E₁ x v₁ | α v₁ ].
  + destruct Hpr as [ p₂'' [ Hred₂' Hpr ] ].
    exists p₂''; split; [ | assumption ].
    etransitivity; eassumption.
  + destruct Hpr as [ v₂ Hred₂' ].
    exists v₂; etransitivity; eassumption.
  + destruct Hpr as [ E₂ [ v₂ [ Hred₂' Hpr ] ] ].
    exists E₂; exists v₂; split; [ | assumption ].
    etransitivity; eassumption.
  + destruct Hpr as [ v₂ [ Hred₂' Hpr ] ].
    exists v₂; split; [ | assumption ].
    etransitivity; eassumption.
Qed.

Lemma progress_step {Σ : VSig} (p₁ p₁' p₂ : program Σ) (Q S : DiRel elem) :
  red p₁ p₁' →
  elem_progress Q S (mk_elem _ p₁ p₂) ↔
  ∃ p₂', red_rtc p₂ p₂' ∧ program_rel S p₁' p₂'.
Proof.
  intro Hred; simpl.
  destruct (classify p₁) as [ p₁ p₁'' Hred₁ | v₁ | E₁ x v₁ | α v₁ ].
  + rewrite (red_determ Hred Hred₁); reflexivity.
  + inversion Hred.
  + exfalso; apply open_stuck_is_stuck in Hred; assumption.
  + inversion Hred.
Qed.

Lemma progress_open_v {Σ : VSig} E₁ x (v₁ : value Σ) p₂ (Q S : DiRel elem) :
  elem_progress Q S (mk_elem _ (eplug E₁ (t_app (v_var x) v₁)) p₂) ↔
  ∃ (E₂ : ectx Σ) (v₂ : value Σ),
    red_rtc p₂ (eplug E₂ (t_app (v_var x) v₂)) ∧
    ectx_rel S E₁ E₂ ∧ value_rel S v₁ v₂.
Proof.
  simpl.
  remember (eplug E₁ (t_app (v_var x) v₁)) as p₁ eqn: Heq.
  destruct (classify p₁) as [ p₁ p₁' Hred | u₁ | E₁' y u₁ | α u₁ ].
  + exfalso; rewrite Heq in Hred; apply open_stuck_is_stuck in Hred; assumption.
  + exfalso; symmetry in Heq.
    apply eplug_not_result in Heq; [ assumption | ]; intro; discriminate.
  + apply open_stuck_unique in Heq; destruct Heq as [ ? [] ]; subst.
    reflexivity.
  + exfalso; symmetry in Heq.
    apply eplug_not_value in Heq; [ assumption | ]; intro; discriminate.
Qed.