(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for lam.                    *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import CallCC.ContLang.Lib.
Require Import CallCC.Simulation.Core.
Require Import CallCC.Simulation.UpToTechniques.Core.
Require Import CallCC.Simulation.UpToTechniques.Continuity.
Require Import CallCC.Simulation.UpToTechniques.Monotonicity.

Instance Evolution_lam :
  lam !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take lam; take_done; apply Hpr
    | take lam; take_done; apply Hpr | ].
  intros e [ Σ t₁ t₂ α Ht ]; simpl.
  exists (v_lam t₂); split; [ reflexivity | ].
  unfold value_rel; take redr.
  apply Mk_redr with
      (p₁' := p_cont (VZ : CVar (incC _)) (shift t₁))
      (p₂' := p_cont (VZ : CVar (incC _)) (shift t₂)).
  + apply red_rtc1; term_simpl; apply red_beta'; term_simpl.
    unfold subst, shift; rewrite map_map_comp'.
    erewrite bind_map_comp with (f' := subst_pure);
      [ rewrite bind_pure'; constructor 1 | ].
    split.
    - intros [ | x ]; simpl; reflexivity.
    - reflexivity.
  + apply red_rtc1; term_simpl; apply red_beta'; term_simpl.
    unfold subst, shift; rewrite map_map_comp'.
    erewrite bind_map_comp with (f' := subst_pure);
      [ rewrite bind_pure'; constructor 1 | ].
    split.
    - intros [ | x ]; simpl; reflexivity.
    - reflexivity.
  + take_done; pr_assumption.
Qed.