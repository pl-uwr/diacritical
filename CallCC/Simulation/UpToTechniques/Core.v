(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file defines the up-to techniques of Figure 3.                  *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import CallCC.ContLang.Lib.
Require Import CallCC.Simulation.Core.

Require List.
Import List.ListNotations.

Inductive refl (R : DiRel elem) : DiRel elem :=
| Mk_refl : ∀ (Σ : VSig) (p : program Σ),
    program_rel (refl R) p p.

Inductive result (R : DiRel elem) : DiRel elem :=
| Mk_result : ∀ (Σ : VSig) (v₁ v₂ : value Σ),
    program_rel (result R) v₁ v₂.

Inductive abort (R : DiRel elem) : DiRel elem :=
| Mk_abort : ∀ (Σ : VSig) α (p₁ p₂ : program Σ),
    program_rel R p₁ p₂ →
    program_rel (abort R) (p_cont α (t_abort p₁)) (p_cont α (t_abort p₂)).

Inductive lam (R : DiRel elem) : DiRel elem :=
| Mk_lam : ∀ (Σ : VSig) (t₁ t₂ : term (incV Σ)) α,
    term_rel R t₁ t₂ →
    program_rel (lam R) (p_cont α (v_lam t₁)) (p_cont α (v_lam t₂)).

Inductive redr (R : DiRel elem) : DiRel elem :=
| Mk_redr : ∀ (Σ : VSig) (p₁ p₁' p₂ p₂' : program Σ),
    red_rtc p₁ p₁' → red_rtc p₂ p₂' →
    program_rel R p₁' p₂' →
    program_rel (redr R) p₁  p₂.

Inductive rename (R : DiRel elem) : DiRel elem :=
| Mk_rename : ∀ (A B : VSig) (Φ : VMap A B) (p₁ p₂ : program A),
    program_rel R p₁ p₂ →
    program_rel (rename R) (fmap Φ p₁) (fmap Φ p₂).

Arguments Mk_rename {R A B}.

Inductive subst_v (R : DiRel elem) : DiRel elem :=
| Mk_subst_v : ∀ (Σ : VSig) (v₁ v₂ : value Σ) (p₁ p₂ : program (incV Σ)),
    value_rel R v₁ v₂ →
    program_rel R p₁ p₂ →
    program_rel (subst_v R) (subst p₁ v₁) (subst p₂ v₂).

Arguments Mk_subst_v {R Σ}.

Inductive subst_c (R : DiRel elem) : DiRel elem :=
| Mk_subst_c : ∀ (Σ : VSig) (E₁ E₂ : ectx Σ) (p₁ p₂ : program (incC Σ)),
    ectx_rel R E₁ E₂ →
    program_rel R p₁ p₂ →
    program_rel (subst_c R) (subst p₁ E₁) (subst p₂ E₂).

Arguments Mk_subst_c {R Σ}.

Notation strong_ut := ([refl;result;abort;lam;redr;rename;subst_v]%list).
Notation weak_ut := ([subst_c]%list).

(** combo (defined in terms of di_combo from Diacritical/Operations.v)
    represents the closure of the set
    F = {refl, result, abort, lam, red, subst, substc}
    wrt iterated union and composition of functions from F
    (Theorem 5.8).
*)

Notation combo      := (di_combo      strong_ut weak_ut).
Notation combo_str  := (di_combo_str  strong_ut).
Notation combo_weak := (di_combo_weak strong_ut weak_ut).