(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for abort.                  *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import CallCC.ContLang.Lib.
Require Import CallCC.Simulation.Core.
Require Import CallCC.Simulation.UpToTechniques.Core.
Require Import CallCC.Simulation.UpToTechniques.Continuity.
Require Import CallCC.Simulation.UpToTechniques.Monotonicity.

Instance Evolution_abort :
  abort !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take abort; take_done; apply Hpr
    | take abort; take_done; apply Hpr | ].
  intros e [ Σ α p₁ p₂ Hp ]; simpl.
  exists p₂; split; [ apply red_rtc1; constructor | ].
  take_done; pr_assumption.
Qed.