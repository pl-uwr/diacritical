(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains a proof of the compatibility of combo             *)
(* (iterated union and composition of functions                         *)
(* from set F of Section 5.3) wrt application.                          *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import CallCC.ContLang.Lib.
Require Import CallCC.Simulation.Core.
Require Import CallCC.Simulation.UpToTechniques.Core.
Require Import CallCC.Simulation.UpToTechniques.Continuity.
Require Import CallCC.Simulation.UpToTechniques.Monotonicity.
Require Import CallCC.Simulation.UpToTechniques.Rename.

Lemma combo_app_v {Σ : VSig} (R : DiRel elem)
    (v₁ v₂ : value Σ) (t₁ t₂ : term Σ) :
  value_rel (combo R) v₁ v₂ →
  term_rel  (combo R) t₁ t₂ →
  term_rel  (combo R) (t_app v₁ t₁) (t_app v₂ t₂).
Proof.
  intros Hv Ht.
  apply (rename_term (mk_shift : VMap _ (incC _))) in Ht; take in Ht.
  apply (Mk_subst_c
           (ectx_app2 (shift v₁) (ectx_cont (VZ : CVar (incC _))))
           (ectx_app2 (shift v₂) (ectx_cont (VZ : CVar (incC _))))) in Ht.
  + take in Ht; term_simpl in Ht; exact Ht.
  + unfold ectx_rel; term_simpl; exact Hv.
Qed.

Lemma combo_app {Σ : VSig} (R : DiRel elem) (t₁ t₂ s₁ s₂ : term Σ) :
  term_rel (combo R) t₁ t₂ →
  term_rel (combo R) s₁ s₂ →
  term_rel (combo R) (t_app t₁ s₁) (t_app t₂ s₂).
Proof.
  intros Ht Hs.
  unfold term_rel in Ht.
  apply (rename_term (mk_shift : VMap _ (incC _))) in Ht; take in Ht.
  apply (rename_term (mk_shift : VMap _ (incV _))) in Hs; take in Hs.
  apply (Mk_subst_c
           (ectx_app1 (ectx_cont (VZ : CVar (incC _))) (shift s₁))
           (ectx_app1 (ectx_cont (VZ : CVar (incC _))) (shift s₂))) in Ht.
  + take in Ht; term_simpl in Ht; exact Ht.
  + unfold ectx_rel; term_simpl.
    change (term_rel (combo R)
                     (t_app (v_var (VZ : Var (incV Σ))) (shift s₁))
                     (t_app (v_var (VZ : Var (incV Σ))) (shift s₂))).
    apply combo_app_v.
    - take refl; constructor.
    - exact Hs.
Qed.

Lemma combo_program {Σ : VSig} (R : DiRel elem) (t₁ t₂ : term Σ) :
  term_rel (combo R) t₁ t₂ →
  program_rel (combo R) t₁ t₂.
Proof.
  intro Ht; unfold term_rel in Ht.
  apply (Mk_subst_c ectx_hole ectx_hole) in Ht;
    [ term_simpl in Ht | take result; constructor ].
  take in Ht; assumption.
Qed.