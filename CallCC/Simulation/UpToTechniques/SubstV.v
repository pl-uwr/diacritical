(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for subst_v                 *)
(* (subst in the paper).                                                *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import CallCC.ContLang.Lib.
Require Import CallCC.Simulation.Core.
Require Import CallCC.Simulation.UpToTechniques.Core.
Require Import CallCC.Simulation.UpToTechniques.Continuity.
Require Import CallCC.Simulation.UpToTechniques.Monotonicity.
Require Import CallCC.Simulation.UpToTechniques.Rename.

Definition swap_v {Σ : VSig} : VMap (incV (incV Σ)) (incV (incV Σ)) :=
  {| map_v := λ x : Var (incV (incV Σ)),
       match x with
       | VZ        => VS VZ
       | VS VZ     => VZ
       | VS (VS y) => VS (VS y)
       end : Var (incV (incV Σ))
  ;  map_c := λ α, α
  |}.

Definition swap_c {Σ : VSig} : VMap (incC (incC Σ)) (incC (incC Σ)) :=
  {| map_v := (λ x, x) : Var (incC (incC Σ)) → Var (incC (incC Σ))
  ;  map_c := λ α,
       match α with
       | VZ        => VS VZ
       | VS VZ     => VZ
       | VS (VS β) => VS (VS β)
       end
  |}.

Lemma subst_v_term {Σ : VSig} (v₁ v₂ : value Σ) (R : DiRel elem) t₁ t₂ :
  value_rel R (shift (Inc:=incC) v₁) (shift v₂) →
  term_rel R t₁ t₂ →
  term_rel (subst_v R) (subst t₁ v₁) (subst t₂ v₂).
Proof.
  intros Hv Ht; unfold term_rel; unfold term_rel in Ht.
  apply (Mk_subst_v (shift v₁ : value (incC Σ)) (shift v₂ : value (incC Σ))
                    (p_cont (VZ : CVar (incV (incC Σ))) (shift t₁ : term (incC (incV Σ))))
                    (p_cont (VZ : CVar (incV (incC Σ))) (shift t₂ : term (incC (incV Σ)))))
    in Ht.
  + term_simpl in Ht; repeat rewrite shift_subst; term_simpl; assumption.
  + assumption.
Qed.

Lemma subst_c_term {Σ : VSig} (E₁ E₂ : ectx Σ) (R : DiRel elem) t₁ t₂ :
  ectx_rel R E₁ E₂ →
  term_rel R t₁ t₂ →
  term_rel (subst_c (rename R)) (subst t₁ E₁) (subst t₂ E₂).
Proof.
  unfold term_rel; intros HE Ht.
  apply (Mk_rename swap_c) in Ht.
  apply (Mk_subst_c (shift E₁) (shift E₂)) in Ht.
  + term_simpl in Ht; repeat rewrite shift_subst; term_simpl.
    unfold shift in Ht; repeat rewrite map_map_comp' in Ht; assumption.
  + apply rename_ectx; assumption.
Qed.

Lemma subst_v_value {Σ : VSig} (v₁ v₂ : value Σ) (R : DiRel elem) u₁ u₂ :
  value_rel R v₁ v₂ →
  value_rel R u₁ u₂ →
  value_rel (subst_v (rename R)) (subst u₁ v₁) (subst u₂ v₂).
Proof.
  intro Hv; unfold value_rel; intro Hu.
  apply (rename_term swap_v) in Hu.
  apply (subst_v_term (shift v₁) (shift v₂)) in Hu.
  + term_simpl in Hu; repeat rewrite shift_subst; term_simpl.
    unfold shift in Hu; repeat rewrite map_map_comp' in Hu; assumption.
  + unfold shift; repeat rewrite map_map_comp'; apply rename_value; assumption.
Qed.

Lemma subst_c_value {Σ : VSig} (E₁ E₂ : ectx Σ) (R : DiRel elem) v₁ v₂ :
  ectx_rel R E₁ E₂ →
  value_rel R v₁ v₂ →
  value_rel (subst_c (rename R)) (subst v₁ E₁) (subst v₂ E₂).
Proof.
  intros HE Hv.
  unfold value_rel, term_rel; unfold value_rel, term_rel in Hv.
  apply (Mk_rename (swap_c (Σ := incV Σ))) in Hv.
  eapply (Mk_subst_c (shift (shift E₁)) (shift (shift E₂))) in Hv.
  + term_simpl in Hv; repeat rewrite shift_subst; term_simpl.
    unfold shift in Hv; repeat rewrite map_map_comp' in Hv.
    repeat rewrite shift_subst; unfold shift; repeat rewrite map_map_comp'.
    assumption.
  + unfold shift; repeat rewrite map_map_comp'; apply rename_ectx; assumption.
Qed.

Lemma subst_v_ectx {Σ : VSig} (v₁ v₂ : value Σ) (R : DiRel elem) E₁ E₂ :
  value_rel R v₁ v₂ →
  ectx_rel R E₁ E₂ →
  ectx_rel (subst_v (rename R)) (subst E₁ v₁) (subst E₂ v₂).
Proof.
  intro Hv; unfold ectx_rel; intro HE.
  apply (Mk_rename swap_v) in HE.
  apply (Mk_subst_v (shift v₁) (shift v₂)) in HE.
  + term_simpl in HE; repeat rewrite shift_subst; term_simpl.
    unfold shift in HE; repeat rewrite map_map_comp' in HE; assumption.
  + apply rename_value; assumption.
Qed.

Lemma subst_c_ectx {Σ : VSig} (E₁ E₂ : ectx Σ) (R : DiRel elem) F₁ F₂ :
  ectx_rel R (shift (Inc:=incV) E₁) (shift E₂) →
  ectx_rel R F₁ F₂ →
  ectx_rel (subst_c R) (subst F₁ E₁) (subst F₂ E₂).
Proof.
  intros HE HF; unfold ectx_rel; unfold ectx_rel in HF.
  apply (Mk_subst_c (shift E₁ : ectx (incV Σ)) (shift E₂ : ectx (incV Σ)))
    in HF.
  + term_simpl in HF; repeat rewrite shift_subst; term_simpl; assumption.
  + assumption.
Qed.

Lemma subst_v_value_sub (R S : DiRel elem) {Σ : VSig} (v₁ v₂ : value Σ) u₁ u₂ :
  value_rel R v₁ v₂ →
  value_rel R u₁ u₂ →
  subst_v (rename R) ⊆ S → value_rel S (subst u₁ v₁) (subst u₂ v₂).
Proof.
  intros Hv Hu Hsub; apply Hsub, subst_v_value; assumption.
Qed.

Lemma subst_v_ectx_sub (R S : DiRel elem) {Σ : VSig} (v₁ v₂ : value Σ) E₁ E₂ :
  value_rel R v₁ v₂ →
  ectx_rel R E₁ E₂ →
  subst_v (rename R) ⊆ S → ectx_rel S (subst E₁ v₁) (subst E₂ v₂).
Proof.
  intros Hv HE Hsub; apply Hsub, subst_v_ectx; assumption.
Qed.

Lemma subst_c_value_sub (R S : DiRel elem) {Σ : VSig} (E₁ E₂ : ectx Σ) v₁ v₂ :
  ectx_rel R E₁ E₂ →
  value_rel R v₁ v₂ →
  subst_c (rename R) ⊆ S → value_rel S (subst v₁ E₁) (subst v₂ E₂).
Proof.
  intros Hv Hu Hsub; apply Hsub, subst_c_value; assumption.
Qed.

Lemma subst_c_ectx_sub (R S : DiRel elem) {Σ : VSig} (E₁ E₂ : ectx Σ) F₁ F₂ :
  ectx_rel R (shift (Inc:=incV) E₁) (shift E₂) →
  ectx_rel R F₁ F₂ →
  subst_c R ⊆ S → ectx_rel S (subst F₁ E₁) (subst F₂ E₂).
Proof.
  intros Hv HE Hsub; apply Hsub, subst_c_ectx; assumption.
Qed.

Lemma subst_v_value_combo {Σ : VSig} (v₁ v₂ : value Σ) u₁ u₂ R :
  value_rel (combo R) v₁ v₂ →
  value_rel (combo R) u₁ u₂ →
  value_rel (combo R) (subst u₁ v₁) (subst u₂ v₂).
Proof.
  intros Hv Hu; eapply subst_v_value_sub; [ eassumption | eassumption | ].
  take subst_v; take rename; reflexivity.
Qed.

Lemma subst_v_ectx_combo {Σ : VSig} (v₁ v₂ : value Σ) E₁ E₂ R :
  value_rel (combo R) v₁ v₂ →
  ectx_rel (combo R) E₁ E₂ →
  ectx_rel (combo R) (subst E₁ v₁) (subst E₂ v₂).
Proof.
  intros Hv HE; eapply subst_v_ectx_sub; [ eassumption | eassumption | ].
  take subst_v; take rename; reflexivity.
Qed.

Lemma subst_c_value_combo {Σ : VSig} (E₁ E₂ : ectx Σ) v₁ v₂ R :
  ectx_rel (combo R) E₁ E₂ →
  value_rel (combo R) v₁ v₂ →
  value_rel (combo R) (subst v₁ E₁) (subst v₂ E₂).
Proof.
  intros HE Hv; eapply subst_c_value_sub; [ eassumption | eassumption | ].
  take subst_c; take rename; reflexivity.
Qed.

Lemma subst_c_ectx_combo {Σ : VSig} (E₁ E₂ : ectx Σ) F₁ F₂ R :
  ectx_rel (combo R) E₁ E₂ →
  ectx_rel (combo R) F₁ F₂ →
  ectx_rel (combo R) (subst F₁ E₁) (subst F₂ E₂).
Proof.
  intros HE HF; eapply subst_c_ectx_sub; [ | eassumption | ].
  + take rename; apply rename_ectx; assumption.
  + take subst_c; reflexivity.
Qed.

Lemma elem_progress_subst_v_open_v_step {Σ : VSig} {v₁ v₂ : value Σ}
  {E₁ E₂ : ectx (incV Σ)} {u₁ u₂ : value (incV Σ)} {p₁ p₂}
  {Q S : DiRel elem} :
  value_rel (combo S) v₁ v₂ →
  ectx_rel  (combo S) E₁ E₂ →
  value_rel (combo S) u₁ u₂ →
  red (p_cont (VZ : CVar (incC (incV Σ)))
          (shift (t_app (shift (map:=@vmap) (Inc:=incV) v₁)
                        (v_var (VZ : Var (incV Σ))))))
      p₁ →
  red_rtc (p_cont (VZ : CVar (incC (incV Σ)))
          (shift (t_app (shift (map:=@vmap) (Inc:=incV) v₂)
                        (v_var (VZ : Var (incV Σ))))))
      p₂ →
  program_rel (combo S) p₁ p₂ →
  elem_progress Q (combo S) (mk_elem Σ
    (subst (eplug E₁ (t_app (v_var (VZ : Var (incV Σ))) u₁)) v₁)
    (subst (eplug E₂ (t_app (v_var (VZ : Var (incV Σ))) u₂)) v₂)).
Proof.
  intros Hv HE Hu Hred₁ Hred₂ Hs.
  apply (red_substc (shift (Inc:=incV) (subst E₁ v₁))) in Hred₁;
    term_simpl in Hred₁.
  apply (red_rtc_substc (shift (Inc:=incV) (subst E₂ v₂))) in Hred₂;
    term_simpl in Hred₂.
  apply (red_substv (subst u₁ v₁)) in Hred₁; term_simpl in Hred₁.
  apply (red_rtc_substv (subst u₂ v₂)) in Hred₂; term_simpl in Hred₂.
  eapply progress_step.
  + term_simpl; exact Hred₁.
  + eexists; split; [ term_simpl; exact Hred₂ | ].
    take subst_v; constructor.
    - apply subst_v_value_combo; assumption.
    - take subst_c; constructor; [ | assumption ].
      take rename; apply rename_ectx.
      apply subst_v_ectx_combo; assumption.
Qed.

Lemma elem_progress_subst_v_open_v {Σ : VSig} {v₁ v₂ : value Σ}
  {E₁ E₂ : ectx (incV Σ)} {u₁ u₂ : value (incV Σ)}
  (Q Q' S : DiRel elem) :
  value_rel (combo S) v₁ v₂ →
  ectx_rel  (combo S) E₁ E₂ →
  value_rel (combo S) u₁ u₂ →
  elem_progress Q' (combo S) (mk_elem (incC (incV Σ))
    (p_cont (VZ : CVar (incC (incV Σ)))
      (shift (t_app (shift (map:=@vmap) (Inc:=incV) v₁)
                    (v_var (VZ : Var (incV Σ))))))
    (p_cont (VZ : CVar (incC (incV Σ)))
      (shift (t_app (shift (map:=@vmap) (Inc:=incV) v₂)
                    (v_var (VZ : Var (incV Σ))))))) →
  elem_progress Q (combo S) (mk_elem Σ
    (subst (eplug E₁ (t_app (v_var (VZ : Var (incV Σ))) u₁)) v₁)
    (subst (eplug E₂ (t_app (v_var (VZ : Var (incV Σ))) u₂)) v₂)).
Proof.
  intros Hv HE Hu Hv'; destruct v₁ as [ x | t₁ | ]; term_simpl in Hv'.
  + destruct Hv' as [ F₂ [ w₂ [ Hred₂ [ HF Hw ] ] ] ].
    term_simpl; apply progress_open_v.
    apply (red_rtc_substc (shift (Inc:=incV) (subst E₂ v₂))) in Hred₂;
      term_simpl in Hred₂.
    apply (red_rtc_substv (subst u₂ v₂)) in Hred₂; term_simpl in Hred₂.
    eapply subst_v_value_combo in Hu; [ | exact Hv ].
    eapply subst_v_ectx_combo in HE; [ | exact Hv ].
    eapply (subst_c_ectx_combo (shift (subst E₁ _)) (shift (subst E₂ v₂)))
      in HF; [ | take rename; apply rename_ectx; eassumption ].
    term_simpl in HF.
    eapply subst_v_ectx_combo in HF; [ | exact Hu ]; term_simpl in HF.
    eapply (subst_c_value_combo (shift (subst E₁ _)) (shift (subst E₂ v₂)))
      in Hw; [ | take rename; apply rename_ectx; eassumption ].
    term_simpl in Hw.
    eapply subst_v_value_combo in Hw; [ | exact Hu ]; term_simpl in Hw.
    exists (subst (subst F₂ (shift (subst E₂ v₂))) (subst u₂ v₂)).
    exists (subst (subst w₂ (shift (subst E₂ v₂))) (subst u₂ v₂)).
    split; [ | split ].
    - assumption.
    - assumption.
    - assumption.
  + destruct Hv' as [ p₂' [ Hred₂ Hp' ] ].
    eapply (elem_progress_subst_v_open_v_step Hv HE Hu);
      [ | eassumption | eassumption ].
    term_simpl; apply red_beta'; term_simpl; reflexivity.
  + destruct Hv' as [ p₂' [ Hred₂ Hp' ] ].
    eapply (elem_progress_subst_v_open_v_step Hv HE Hu);
      [ | eassumption | eassumption ].
    term_simpl; apply red_callcc'; term_simpl; reflexivity.
Qed.

Instance Evolution_subst_v :
  subst_v !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take subst_v; take_done; apply Hpr
    | take subst_v; take_done; apply Hpr | ].
  intros e [ Σ v₁ v₂ p₁ p₂ Hv Hp ].
  apply (progress_rel Hpr) in Hp; simpl in Hp.
  destruct (classify p₁) as [ p₁ p₁' Hred₁ | u₁ | E₁ x u₁ | α u₁ ].
  + destruct Hp as [ p₂' [ Hred₂ Hp' ] ].
    eapply progress_step; [ eapply red_substv; eassumption | ].
    exists (subst p₂' v₂); split; [ apply red_rtc_substv; assumption | ].
    take subst_v; constructor; take_done; pr_assumption.
  + destruct Hp as [ u₂ Hred₂ ]; simpl.
    exists (subst u₂ v₂); apply (red_rtc_substv v₂ Hred₂).
  + destruct Hp as [ E₂ [ u₂ [ Hred₂ [ HE Hu ] ] ] ].
    apply (red_rtc_substv v₂) in Hred₂.
    destruct x as [ | x ].
    - eapply elem_progress_red_rtc_r; [ exact Hred₂ | ].
      eapply elem_progress_subst_v_open_v.
      * take_done; pr_assumption.
      * take_done; pr_assumption.
      * take_done; pr_assumption.
      * apply (progress_rel Hpr) in Hv.
        eapply elem_progress_monotone; [ reflexivity | | exact Hv ].
        take_done; reflexivity.
    - term_simpl; apply progress_open_v.
      exists (subst E₂ v₂); exists (subst u₂ v₂); split; [ | split ].
      * term_simpl in Hred₂; assumption.
      * apply (subst_v_ectx_sub S); [ pr_assumption | pr_assumption | ].
        take subst_v; take rename; take_done; reflexivity.
      * apply (subst_v_value_sub S); [ pr_assumption | pr_assumption | ].
        take subst_v; take rename; take_done; reflexivity.
  + destruct Hp as [ u₂ [ Hred₂ Hu ] ]; simpl.
    exists (subst u₂ v₂); split; [ apply (red_rtc_substv v₂ Hred₂) | ].
    apply (subst_v_value_sub Q); [ pr_assumption | pr_assumption | ].
    take subst_v; take rename; take_done; reflexivity.
Qed.