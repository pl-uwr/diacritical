(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for redr                    *)
(* (red in the paper).                                                  *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import CallCC.ContLang.Lib.
Require Import CallCC.Simulation.Core.
Require Import CallCC.Simulation.UpToTechniques.Core.
Require Import CallCC.Simulation.UpToTechniques.Continuity.
Require Import CallCC.Simulation.UpToTechniques.Monotonicity.

Instance Evolution_redr :
  redr !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take redr; take_done; apply Hpr
    | take redr; take_done; apply Hpr | ].
  intros e [ Σ p₁ p₁' p₂ p₂' Hred₁ Hred₂ Hp ].
  inversion Hred₁ as [ | p₀ ? Hred₀ Hrtc ]; subst; clear Hred₁.
  + eapply elem_progress_red_rtc_r; [ eassumption | ].
    eapply elem_progress_monotone; [ | | eapply (progress_rel Hpr); assumption ].
    - take_done; reflexivity.
    - take_done; reflexivity.
  + eapply progress_step; [ eassumption | ].
    exists p₂; split; [ reflexivity | ].
    take redr; econstructor; [ eassumption | eassumption | ].
    take_done; pr_assumption.
Qed.