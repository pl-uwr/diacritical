(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains a correctness proof                               *)
(* for the rename up-to technique that is related                       *)
(* to the binding representation, and therefore                         *)
(* not mentioned in the article.                                        *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Set.
Require Import Diacritical.Lib.
Require Import CallCC.ContLang.Lib.
Require Import CallCC.Simulation.Core.
Require Import CallCC.Simulation.UpToTechniques.Core.
Require Import CallCC.Simulation.UpToTechniques.Continuity.
Require Import CallCC.Simulation.UpToTechniques.Monotonicity.

Lemma rename_term {A B : VSig} (Φ : VMap A B) (R : DiRel elem) t₁ t₂ :
  term_rel R t₁ t₂ → term_rel (rename R) (fmap Φ t₁) (fmap Φ t₂).
Proof.
  unfold term_rel; intro Ht.
  apply (Mk_rename (liftA Φ : VMap (incC A) _)) in Ht; term_simpl in Ht.
assumption.
Qed.

Lemma rename_term0 {A B : Set} (f : A [→] B) (R : DiRel elem) t₁ t₂ :
  term0_rel R t₁ t₂ → term0_rel (rename R) (fmap f t₁) (fmap f t₂).
Proof.
  unfold term0_rel; intro Ht.
  rewrite embed_fmap, embed_fmap.
  unfold shift; rewrite map_map_comp', map_map_comp'; simpl.
  apply Mk_rename with (Φ :=
                          {| map_v := f : Var (incC {| Var := A |}) → Var (incC {| Var := B |})
                             ;  map_c := λ α, α
                          |}) in Ht; term_simpl in Ht.
  unfold shift in Ht.
  rewrite map_map_comp', map_map_comp' in Ht; exact Ht.
Qed.

Lemma rename_ectx {A B : VSig} (Φ : VMap A B) (R : DiRel elem) E₁ E₂ :
  ectx_rel R E₁ E₂ → ectx_rel (rename R) (fmap Φ E₁) (fmap Φ E₂).
Proof.
  unfold ectx_rel; intro HE.
  apply (Mk_rename (liftA Φ : VMap (incV A) _)) in HE; term_simpl in HE.
assumption.
Qed.

Lemma rename_value {A B : VSig} (Φ : VMap A B) (R : DiRel elem) v₁ v₂ :
  value_rel R v₁ v₂ → value_rel (rename R) (fmap Φ v₁) (fmap Φ v₂).
Proof.
  unfold value_rel; intro Hv.
  apply (rename_term (liftA Φ : VMap (incV A) _)) in Hv; term_simpl in Hv.
  assumption.
Qed.

Lemma unshiftC_value {Σ : VSig} (v₁ v₂ : value Σ) (R : DiRel elem) :
  value_rel R (shift v₁ : value (incC Σ)) (shift v₂) →
  value_rel (rename R) v₁ v₂.
Proof.
  unfold value_rel, term_rel; term_simpl; intro Hv.
  unfold shift; repeat rewrite map_map_comp'; simpl.
  apply Mk_rename with (Φ :=
                          {| map_v := λ x : Var (incC (incC (incV Σ))), (x : Var (incC (incV Σ)))
                             ; map_c := λ α,
                                        match α with
                                        | VZ        => VZ
                                        | VS VZ     => VZ
                                        | VS (VS β) => VS β
                                        end
                          |}) in Hv.
  term_simpl in Hv.
  unfold shift in Hv; repeat rewrite map_map_comp' in Hv; simpl in Hv.
  exact Hv.
Qed.

Instance Evolution_rename :
  rename !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take rename; take_done; apply Hpr
    | take rename; take_done; apply Hpr | ].
  intros e [ Σ Δ Φ p₁ p₂ Hp ].
  apply (progress_rel Hpr) in Hp; simpl in Hp.
  destruct (classify p₁) as [ p₁ p₁' Hred₁ | v₁ | E₁ x v₁ | α v₁ ].
  + destruct Hp as [ p₂' [ Hred₂ Hp' ] ].
    eapply progress_step; [ eapply red_fmap; eassumption | ].
    exists (fmap Φ p₂'); split; [ apply red_rtc_fmap; assumption | ].
    take rename; constructor; take_done; pr_assumption.
  + destruct Hp as [ v₂ Hred₂ ]; simpl.
    exists (fmap Φ v₂); apply (red_rtc_fmap Φ Hred₂).
  + destruct Hp as [ E₂ [ v₂ [ Hred₂ [ HE Hv ] ] ] ].
    term_simpl; apply progress_open_v.
    exists (fmap Φ E₂); exists (fmap Φ v₂).
    split; [ | split ].
    - apply (red_rtc_fmap Φ) in Hred₂; term_simpl in Hred₂; assumption.
    - take rename; apply rename_ectx;  take_done; pr_assumption.
    - take rename; apply rename_value; take_done; pr_assumption.
  + destruct Hp as [ v₂ [ Hred₂ Hv ] ]; simpl.
    exists (fmap Φ v₂); split.
    - apply (red_rtc_fmap Φ) in Hred₂; assumption.
    - take rename; apply rename_value; take_done; pr_assumption.
Qed.