(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for subst_c                 *)
(* (substc in the paper).                                               *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import CallCC.ContLang.Lib.
Require Import CallCC.Simulation.Core.
Require Import CallCC.Simulation.UpToTechniques.Core.
Require Import CallCC.Simulation.UpToTechniques.Continuity.
Require Import CallCC.Simulation.UpToTechniques.Monotonicity.

Require Import CallCC.Simulation.UpToTechniques.Rename.
Require Import CallCC.Simulation.UpToTechniques.SubstV.

Local Lemma Mk_subst_c' {Σ : VSig} {E₁ E₂ : ectx Σ}
  {R : DiRel elem} {p₁ p₂ : program (incC Σ)} {p₁'} :
  p₁' = subst p₁ E₁ →
  ectx_rel (combo R) E₁ E₂ →
  program_rel (combo R) p₁ p₂ →
  program_rel (combo R) p₁' (subst p₂ E₂).
Proof.
  intros Heq HE Hp; subst.
  take subst_c; constructor; assumption.
Qed.

Lemma elem_progress_subst_c_value_aux {Σ : VSig} {E₁ E₂ : ectx Σ}
    {v₁ v₂ : value (incC Σ)} 
    {R Q S : DiRel elem} :
  R ↣ Q & S →
  ectx_rel R E₁ E₂ →
  value_rel R v₁ v₂ →
  elem_progress Q (combo S) (mk_elem (incC (incV Σ))
     (p_cont (VZ : CVar (incC (incV Σ)))
       (shift (t_app (shift (map:=@vmap) (subst (bnd:=@vbind) v₁ E₁))
                     (v_var (VZ : Var (incV Σ))))))
     (p_cont (VZ : CVar (incC (incV Σ)))
       (shift (t_app (shift (map:=@vmap) (subst (bnd:=@vbind) v₂ E₂))
                     (v_var (VZ : Var (incV Σ))))))).
Proof.
  intros Hpr HE Hv.
  assert (Hpr' := Hpr).
  apply Evolution_rename in Hpr'.
  apply (Mk_rename (swap_c (Σ:=incV Σ))) in Hv.
  apply (progress_rel Hpr') in Hv.
  destruct v₁ as [ x | t | ]; term_simpl; term_simpl in Hv.
  + destruct Hv as [ F₂ [ u₂ [ Hred₂ [ HF Hu ] ] ] ].
    exists (subst F₂ (shift (shift E₂))).
    exists (subst u₂ (shift (shift E₂))); split; [ | split ].
    - apply (red_rtc_substc (shift (shift E₂))) in Hred₂; term_simpl in Hred₂.
      etransitivity; [ | eassumption ].
      unfold shift, subst; repeat rewrite map_map_comp'.
      erewrite bind_map_comp; [ reflexivity | ].
      split; [ | intros [ | ] ]; reflexivity.
    - eapply subst_c_ectx_combo in HF; [ eassumption | ].
      take rename; apply rename_ectx.
      take rename; apply rename_ectx.
      take_done; apply (progress_sub_act Hpr); eassumption.
    - eapply subst_c_value_combo in Hu; [ eassumption | ].
      take rename; apply rename_ectx.
      take rename; apply rename_ectx.
      take_done; apply (progress_sub_act Hpr); eassumption.
  + destruct Hv as [ p₂ [ Hred₂ Hp ] ].
    exists (subst p₂ (shift (shift E₂))); split.
    - apply (red_rtc_substc (shift (shift E₂))) in Hred₂; term_simpl in Hred₂.
      etransitivity; [ | eassumption ].
      unfold shift, subst; repeat rewrite map_map_comp'.
      erewrite bind_map_comp; [ reflexivity | ].
      split; [ | intros [ | ] ]; reflexivity.
    - eapply Mk_subst_c'; [ | | eassumption ];
        [ | take rename; apply rename_ectx; take rename; apply rename_ectx
            ; take_done; apply (progress_sub_act Hpr); eassumption ].
      term_simpl.
      unfold subst, shift; repeat rewrite map_map_comp'.
      repeat rewrite map_to_bind.
      repeat rewrite bind_bind_comp'.
      erewrite bind_equiv; [ reflexivity | ].
      split; [ intros [ | x ] | intros [ | α ] ]; try reflexivity.
      term_simpl.
      unfold shift; rewrite map_to_bind, bind_bind_comp'; apply bind_equiv.
      split; reflexivity.
  + destruct Hv as [ p₂ [ Hred₂ Hp ] ].
    exists (subst p₂ (shift (shift E₂))); split.
    - apply (red_rtc_substc (shift (shift E₂))) in Hred₂; term_simpl in Hred₂.
      etransitivity; [ | eassumption ].
      unfold shift, subst; repeat rewrite map_map_comp'.
      erewrite bind_map_comp; [ reflexivity | ].
      split; [ | intros [ | ] ]; reflexivity.
    - eapply Mk_subst_c'; [ | | eassumption ];
        [ | take rename; apply rename_ectx; take rename; apply rename_ectx
            ; take_done; apply (progress_sub_act Hpr); eassumption ].
      term_simpl; reflexivity.
Qed.

Lemma elem_progress_subst_c_value {R S : DiRel elem} {Σ : VSig}
  {v₁ v₂ : value _} {p₂}
  {E₁ E₂ : ectx Σ} :
  R ↣ R & S →
  ectx_rel R E₁ E₂ →
  red_rtc p₂ (p_cont (VZ : CVar (incC _)) v₂) →
  value_rel R v₁ v₂ →
  elem_progress (combo_weak R) (combo S)
    (mk_elem _ (subst (p_cont (VZ : CVar (incC _)) v₁) E₁) (subst p₂ E₂)).
Proof.
  intros Hpr HE Hred₂ Hv.
  apply (red_rtc_substc E₂) in Hred₂.
  eapply elem_progress_red_rtc_r; [ exact Hred₂ | clear p₂ Hred₂ ].
  assert (HE' := HE); unfold ectx_rel in HE'.
  apply (progress_rel Hpr) in HE'; simpl in HE'.
  match type of HE' with match classify ?p with _ => _ end =>
                         remember p as p₁ eqn: Hp₁
  end.
  match goal with
  | |- elem_progress _ _ (mk_elem _ ?p _) =>
    assert (Heq : p = subst p₁ (subst v₁ E₁)) by
        (rewrite Hp₁; term_simpl; reflexivity);
      rewrite Heq; clear Heq
  end.
  destruct (classify p₁) as [ p₁ p₁' Hred₁ | u₁ | F₁ x u₁ | α u₁ ].
  + destruct HE' as [ p₂' [ Hred₂ Hp' ] ].
    apply (red_substv (subst v₁ E₁)) in Hred₁.
    apply (red_rtc_substv (subst v₂ E₂)) in Hred₂; term_simpl in Hred₂.
    eapply progress_step; [ eassumption | ].
    exists (subst p₂' (subst v₂ E₂)); split; [ term_simpl; assumption | ].
    take subst_v; constructor; [ | take_done; pr_assumption ].
    apply subst_c_value_combo; take_done; pr_assumption.
  + destruct HE' as [ u₂ Hred₂ ]; term_simpl.
    apply (red_rtc_substv (subst v₂ E₂)) in Hred₂; term_simpl in Hred₂.
    exists (subst u₂ (subst v₂ E₂)); assumption.
  + destruct HE' as [ F₂ [ u₂ [ Hred₂ [ HF Hu ] ] ] ].
    apply (red_rtc_substv (subst v₂ E₂)) in Hred₂.
    destruct x as [ | x ].
    - eapply elem_progress_red_rtc_r;
        [ etransitivity; [ | exact Hred₂ ]; term_simpl; reflexivity | ].
      eapply elem_progress_subst_v_open_v with (Q' := R).
      * apply subst_c_value_combo; take_done; pr_assumption.
      * take_done; assumption.
      * take_done; assumption.
      * eapply elem_progress_subst_c_value_aux; eassumption.
    - term_simpl; apply progress_open_v.
      exists (subst F₂ (subst v₂ E₂)); exists (subst u₂ (subst v₂ E₂));
        split; [ | split ].
      * term_simpl in Hred₂; assumption.
      * apply subst_v_ectx_combo; [ | take_done; assumption ].
        apply subst_c_value_combo; take_done; pr_assumption.
      * apply subst_v_value_combo; [ | take_done; assumption ].
        apply subst_c_value_combo; take_done; pr_assumption.
  + destruct HE' as [ u₂ [ Hred₂ Hu ] ]; term_simpl.
    apply (red_rtc_substv (subst v₂ E₂)) in Hred₂; term_simpl in Hred₂.
    exists (subst u₂ (subst v₂ E₂)); split; [ assumption | ].
    eapply (subst_v_value_sub (combo_weak R)).
    - eapply subst_c_value_sub; [ eassumption | eassumption | ].
      take subst_c; take rename; take_done; reflexivity.
    - take_done; assumption.
    - take subst_v; take rename; reflexivity.
Qed.

Instance Evolution_subst_c :
  subst_c ↝ combo_weak & combo.
Proof.
  intros R S Hpr; split;
    [ take subst_c; take_done; reflexivity
    | take subst_c; take_done; apply Hpr | ].
  intros e [ Σ E₁ E₂ p₁ p₂ HE Hp ].
  apply (progress_rel Hpr) in Hp; simpl in Hp.
  destruct (classify p₁) as [ p₁ p₁' Hred₁ | v₁ | F₁ x v₁ | α v₁ ].
  + destruct Hp as [ p₂' [ Hred₂ Hp' ] ].
    eapply progress_step; [ eapply red_substc; eassumption | ].
    exists (subst p₂' E₂); split; [ apply red_rtc_substc; assumption | ].
    take subst_c; constructor; take_done; pr_assumption.
  + destruct Hp as [ v₂ Hred₂ ]; simpl.
    exists (subst v₂ E₂); apply (red_rtc_substc E₂ Hred₂).
  + destruct Hp as [ F₂ [ v₂ [ Hred₂ [ HF Hv ] ] ] ].
    term_simpl; apply progress_open_v.
    exists (subst F₂ E₂); exists (subst v₂ E₂).
    split; [ | split ].
    - apply (red_rtc_substc E₂) in Hred₂; term_simpl in Hred₂; assumption.
    - apply subst_c_ectx_combo; take_done; pr_assumption.
    - apply subst_c_value_combo; take_done; pr_assumption.
  + destruct Hp as [ v₂ [ Hred₂ Hv ] ].
    destruct α as [ | α ].
    - apply (elem_progress_subst_c_value Hpr HE Hred₂ Hv).
    - term_simpl.
      exists (subst v₂ E₂); split; [ apply (red_rtc_substc E₂ Hred₂) | ].
      eapply subst_c_value_sub; [ eassumption | eassumption | ].
      take subst_c; take rename; take_done; reflexivity.
Qed.