(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for result.                 *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import CallCC.ContLang.Lib.
Require Import CallCC.Simulation.Core.
Require Import CallCC.Simulation.UpToTechniques.Core.
Require Import CallCC.Simulation.UpToTechniques.Continuity.
Require Import CallCC.Simulation.UpToTechniques.Monotonicity.

Instance Evolution_result :
  result !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take result; take_done; apply Hpr
    | take result; take_done; apply Hpr | ].
  intros e [ Σ v₁ v₂ ]; simpl.
  exists v₂; reflexivity.
Qed.