(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the continuity proofs for the up-to techniques    *)
(* of Figure 3 (continuity is stipulated in Definition 3.12).           *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import CallCC.Simulation.UpToTechniques.Core.

Import List.ListNotations.

Instance Continuous_refl : Continuous refl.
Proof.
  intros R _ [ Σ p ]; exists nil; split; constructor.
Qed.

Instance Continuous_result : Continuous result.
Proof.
  intros R _ [ Σ v₁ v₂ ]; exists nil; split; constructor.
Qed.

Instance Continuous_abort : Continuous abort.
Proof.
  intros R _ [ Σ α p₁ p₂ Hp ]; eexists [ _ ]%list; split.
  + constructor; [ exact Hp | ]; constructor.
  + constructor; left; reflexivity.
Qed.

Instance Continuous_lam : Continuous lam.
Proof.
  intros R _ [ Σ t₁ t₂ α Ht ]; eexists [ _ ]%list; split.
  + constructor; [ exact Ht | ]; constructor.
  + constructor; left; reflexivity.
Qed.

Instance Continuous_redr : Continuous redr.
Proof.
  intros R _ [ Σ p₁ p₁' p₂ p₂' Hred₁ Hred₂ Hp ]; eexists [ _ ]%list; split.
  + constructor; [ exact Hp | ]; constructor.
  + econstructor; [ eassumption | eassumption | left; reflexivity ].
Qed.

Instance Continuous_rename : Continuous rename.
Proof.
  intros R _ [ Σ Δ Φ p₁ p₂ Hp ]; eexists [ _ ]%list; split.
  + econstructor; [ exact Hp | ]; constructor.
  + constructor; left; reflexivity.
Qed.

Instance Continuous_subst_v : Continuous subst_v.
Proof.
  intros R _ [ Σ v₁ v₂ p₁ p₂ Hv Hp ]; eexists [ _ ; _ ]%list; split.
  + constructor; [ exact Hv | ].
    constructor; [ exact Hp | ]; constructor.
  + constructor; [ left | right; left ]; reflexivity.
Qed.

Instance Continuous_subst_c : Continuous subst_c.
Proof.
  intros R _ [ Σ E₁ E₂ p₁ p₂ HE Hp ]; eexists [ _ ; _ ]%list; split.
  + constructor; [ exact HE | ].
    constructor; [ exact Hp | ]; constructor.
  + constructor; [ left | right; left ]; reflexivity.
Qed.