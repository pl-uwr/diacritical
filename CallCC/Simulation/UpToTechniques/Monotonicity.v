(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the monotonicity proofs                           *)
(* for the up-to techniques of Figure 3.                                *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import CallCC.Simulation.UpToTechniques.Core.

Instance Monotone_refl : Monotone refl.
Proof.
  intros R S Hsub e; destruct 1; constructor.
Qed.

Instance Monotone_result : Monotone result.
Proof.
  intros R S Hsub e; destruct 1; constructor.
Qed.

Instance Monotone_abort : Monotone abort.
Proof.
  intros R S Hsub e; destruct 1; constructor; apply Hsub; assumption.
Qed.

Instance Monotone_lam : Monotone lam.
Proof.
  intros R S Hsub e; destruct 1; constructor; apply Hsub; assumption.
Qed.

Instance Monotone_redr : Monotone redr.
Proof.
  intros R S Hsub e; destruct 1; econstructor.
  + eassumption.
  + eassumption.
  + apply Hsub; assumption.
Qed.

Instance Monotone_rename : Monotone rename.
Proof.
  intros R S Hsub e; destruct 1; constructor; apply Hsub; assumption.
Qed.

Instance Monotone_subst_v : Monotone subst_v.
Proof.
  intros R S Hsub e; destruct 1; constructor; apply Hsub; assumption.
Qed.

Instance Monotone_subst_c : Monotone subst_c.
Proof.
  intros R S Hsub e; destruct 1; constructor; apply Hsub; assumption.
Qed.