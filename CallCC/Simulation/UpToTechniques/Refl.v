(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for refl.                   *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import CallCC.ContLang.Lib.
Require Import CallCC.Simulation.Core.
Require Import CallCC.Simulation.UpToTechniques.Core.
Require Import CallCC.Simulation.UpToTechniques.Continuity.
Require Import CallCC.Simulation.UpToTechniques.Monotonicity.

Instance Evolution_refl :
  refl !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take refl; take_done; apply Hpr
    | take refl; take_done; apply Hpr | ].
  intros e [ Σ p ]; simpl.
  destruct (classify p) as [ p p' Hred | v | E x v | α v ].
  + exists p'; split; [ apply red_rtc1; assumption | ].
    take refl; constructor.
  + exists v; reflexivity.
  + exists E; exists v; split; [ reflexivity | ].
    split; take refl; constructor.
  + exists v; split; [ reflexivity | ].
    take refl; constructor.
Qed.