# Proving Soundness of Extensional Normal-Form Bisimilarities

This is the Coq formalization that accompanies the "Proving Soundness
of Extensional Normal-Form Bisimilarities" paper. The code was tested
under Coq version 8.6.1.

## Structure of the development

Here we briefly discuss how the development is divided into subdirectories.

* The [Binding](Binding/) directory contains a library used for
  representing variable binding (based on the "nested datatypes"
  approach). The library heavily uses typeclasses and is expressive
  enough to handle languages with multiple sorts of variables (such
  as, e.g., the [auxiliary calculus](CallCC/ContLang/Syntax.v) for
  abortive control).

* The [Diacritical](Diacritical/) directory contains a library that supports
  diacritical simulations (Section 3.2 of the paper).

* The [Lambda](Lambda/) directory contains formalization of simulations
  for the pure lambda calculus (Section 3).

* Simulations for shift/reset control operators (Section 4) are
  formalized in the [ShiftReset](ShiftReset/) directory.

* The formalization of simulations for abortive control (Section 5)
  can be found in the [CallCC](CallCC/) directory.

### Note

The formalization does not include the examples from the paper of how
the bisimulations are used in practice.

## Other formalization

Previous formalization that accompanies the conference version of the
paper can be found [here](http://www.ii.uni.wroc.pl/~ppolesiuk/apnfbisim/).
