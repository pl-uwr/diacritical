Require Import Utf8.
Require Import Binding.Core.
Require Import Binding.Inc.


Class SetPureCore (F : Set → Type) : Type :=
  set_pure : ∀ A : Set, A → F A.

Arguments set_pure {F _ A}.

Record Arr (A B : Set) : Type :=
  { apply_arr : A → B
  }.

Arguments apply_arr {A B}.
Coercion apply_arr : Arr >-> Funclass.

Record Sub {F : Set → Type} {FP : SetPureCore F} (A B : Set) : Type :=
  { apply_sub : A → F B
  }.

Arguments apply_sub {F FP A B}.
Coercion apply_sub : Sub >-> Funclass.

Notation "A '[→]' B" := (Arr A B) (at level 99) : type_scope.
Notation "A '[⇒]' B" := (Sub A B) (at level 99) : type_scope.

Class SetPure (F : Set → Type) {FP : SetPureCore F}
    {map  : ∀ A B : Set, (A [→] B) → F A → F B}
    {MapF : IsMapCore map}
    {bnd  : ∀ A B : Set, (A [⇒] B) → F A → F B}
    {BndF : IsBindCore bnd} : Prop :=
  { fmap_set_pure : ∀ (A B : Set) (f : A [→] B) (x : A),
      fmap f (set_pure x) = set_pure (f x)
  ; bind_set_pure : ∀ (A B : Set) (f : A [⇒] B) (x : A),
      bind f (set_pure x) = f x
  }.

Section SetInstances.
Context {F : Set → Type} {FP : SetPureCore F}.
Context {map : ∀ A B : Set, (A [→] B) → F A → F B} {MapF : IsMapCore map}.
Context {bnd : ∀ A B : Set, (A [⇒] B) → F A → F B} {BndF : IsBindCore bnd}.
Context {SPF : SetPure F}.

Global Instance ArrowCore_Set : ArrowCore Arr :=
  { arrow_id   := λ _,         {| apply_arr := λ x, x       |}
  ; arrow_comp := λ _ _ _ f g, {| apply_arr := λ x, f (g x) |}
  ; arrow_eq   := λ _ _   f g, ∀ x, f x = g x
  }.

Global Instance Arrow_Set : Arrow Arr.
Proof.
split; unfold arrow_eq; simpl; congruence.
Qed.

Global Instance PreSubstCore_Set : PreSubstCore Sub :=
  { subst_pure := λ _, {| apply_sub := set_pure |}
  ; subst_eq   := λ _ _ f g, ∀ x, f x = g x
  }.

Global Instance PreSubst_Set : PreSubst Sub.
Proof.
split; unfold subst_eq; simpl; congruence.
Qed.

Global Instance SubstCore_Set : SubstCore Arr Sub :=
  { arrow_subst_comp := λ (A B C : Set) (f : B [→] C) (g : A [⇒] B),
      {| apply_sub := λ x, fmap f (g x) |}
  ; subst_comp := λ (A B C : Set) (f : B [⇒] C) (g : A [⇒] B),
      {| apply_sub := λ x, bind f (g x) |}
  }.

Context {MF : FMap map} {BF : Bind bnd}.
Context {MBF : BindMap map bnd}.

Global Instance Subst_Set : Subst Arr Sub.
Proof.
split; unfold subst_eq; simpl.
+ intros A B f x; apply map_id; reflexivity.
+ intros A B f x; apply bind_pure; reflexivity.
+ intros A B f x; apply bind_set_pure.
Qed.

Global Instance ALiftableCore_inc : ALiftableCore Arr inc :=
  { liftA := λ _ _ f, {| apply_arr := inc_map f |} }.

Global Instance ALiftable_inc : ALiftable Arr inc.
Proof.
split; simpl.
+ intros A f Heq [ | x ]; simpl; [ reflexivity | ].
  apply f_equal, Heq.
+ intros A B C f g h Heq [ | x ]; simpl; [ reflexivity | ].
  apply f_equal, Heq.
Qed.

Global Instance SLiftableCore_inc : SLiftableCore Sub inc :=
  { liftS := λ A B (f : A [⇒] B),
      {| apply_sub := λ x,
           match x with
           | VZ   => set_pure VZ
           | VS y => fmap {| apply_arr := VS |} (f y)
           end
      |}
  }.

Global Instance SLiftable_inc : SLiftable Arr Sub inc.
Proof.
split; simpl.
+ intros A f Heq [ | x ]; simpl; [ reflexivity | ].
  rewrite Heq; apply fmap_set_pure.
+ intros A B C f g h Heq [ | x ]; simpl.
  - rewrite bind_set_pure; reflexivity.
  - rewrite <- Heq; simpl.
    apply bind_map_comp; intro y; simpl.
    rewrite fmap_set_pure, bind_set_pure; reflexivity.
Qed.

Global Instance ASLiftable_inc : ASLiftable Arr Sub inc.
Proof.
intros A B B' C f g g' f' Heq; intros [ | x ]; simpl.
+ rewrite fmap_set_pure, fmap_set_pure, bind_set_pure; reflexivity.
+ rewrite fmap_set_pure, bind_set_pure; simpl.
  specialize (Heq x); simpl in Heq.
  rewrite fmap_set_pure, bind_set_pure in Heq.
  rewrite Heq.
  erewrite (map_map_comp _ {| apply_arr := VS |}); [ | reflexivity ].
  apply map_map_comp; simpl; reflexivity.
Qed.

Global Instance ShiftableCore_inc : ShiftableCore Arr inc :=
  λ A, {| apply_arr := VS |}.

Global Instance LiftAShift_inc : LiftAShift Arr inc.
Proof.
unfold LiftAShift; reflexivity.
Qed.

Global Instance LiftSShift_inc : LiftSShift Arr Sub inc.
Proof.
intros A B f x; simpl.
rewrite fmap_set_pure; apply bind_set_pure.
Qed.

Global Instance SubstitutableCore_inc : SubstitutableCore Sub F inc :=
  λ A v,
  {| apply_sub := λ x,
       match x with
       | VZ => v
       | VS y => set_pure y
       end
  |}.

Global Instance SubstShift_inc : SubstShift Arr Sub F inc.
Proof.
intros A v x; simpl.
rewrite fmap_set_pure, bind_set_pure; simpl; reflexivity.
Qed.

Global Instance SubstFMap_inc : SubstFMap Arr Sub F inc.
Proof.
intros A B f v [ | x ]; simpl.
+ rewrite fmap_set_pure, bind_set_pure; reflexivity.
+ rewrite fmap_set_pure, fmap_set_pure, bind_set_pure.
  reflexivity.
Qed.

Global Instance SubstBind_inc : SubstBind Arr Sub F inc.
Proof.
intros A B f v [ | x ]; simpl.
+ rewrite bind_set_pure; reflexivity.
+ rewrite bind_set_pure.
  erewrite bind_map_comp.
  - rewrite bind_pure; [ | reflexivity ].
    symmetry; apply map_id; reflexivity.
  - intro y; simpl.
    rewrite fmap_set_pure, fmap_set_pure, bind_set_pure; reflexivity.
Qed.

End SetInstances.

Arguments set_pure {F _ A} /.