Require Import Utf8.
Require Import Binding.Core.

Section Properties.
Context {Obj : Type}.
Context {Arr : Obj → Obj → Type} {AC : ArrowCore Arr} {AA : Arrow Arr}.
Context {Sub : Obj → Obj → Type} {PSC : PreSubstCore Sub} {PS : PreSubst Sub}.
Context {SC  : SubstCore Arr Sub} {SS : Subst Arr Sub}.
Context {F G : Obj → Type}.
Context {Inc : Obj → Obj}.
Context {Sh  : ShiftableCore Arr Inc}.
Context {ALC : ALiftableCore Arr Inc}.
Context {SLC : SLiftableCore Sub Inc}.
Context {Sb : SubstitutableCore Sub F Inc}.
Context {LAS : LiftAShift Arr Inc}.
Context {LSS : LiftSShift Arr Sub Inc}.
Context {mapF : ∀ A B, Arr A B → F A → F B} {MapF : IsMapCore mapF}.
Context {mapG : ∀ A B, Arr A B → G A → G B} {MapG : IsMapCore mapG}
  {MG : FMap mapG}.
Context {bndF : ∀ A B, Sub A B → F A → F B} {BindF : IsBindCore bndF}.
Context {bndG : ∀ A B, Sub A B → G A → G B} {BindG : IsBindCore bndG}
  {BMG : BindMap mapG bndG} {BG : Bind bndG}.
Context {SbShF  : SubstShift Arr Sub F Inc}.
Context {SbMapF : SubstFMap  Arr Sub F Inc}.
Context {SbBndF : SubstBind  Arr Sub F Inc}.

Lemma map_id' {A : Obj} (t : G A) :
  fmap arrow_id t = t.
Proof.
apply map_id; reflexivity.
Qed.

Lemma map_map_comp' {A B C : Obj} (f : Arr B C) (g : Arr A B) (t : G A) :
  fmap f (fmap g t) = fmap (arrow_comp f g) t.
Proof.
apply map_map_comp; reflexivity.
Qed.

Lemma map_equiv {A B : Obj} (f g : Arr A B) (t : G A) :
  arrow_eq f g → fmap f t = fmap g t.
Proof.
intro Heq; rewrite <- (map_id' (fmap f t)).
apply map_map_comp.
etransitivity; [ | eassumption ].
apply arrow_comp_id_l.
Qed.

Lemma bind_pure' {A : Obj} (t : G A) :
  bind subst_pure t = t.
Proof.
apply bind_pure; reflexivity.
Qed.

Lemma bind_bind_comp' {A B C : Obj} (f : Sub B C) (g : Sub A B) (t : G A) :
  bind f (bind g t) = bind (subst_comp f g) t.
Proof.
apply bind_bind_comp; reflexivity.
Qed.

Lemma bind_equiv {A B : Obj} {f g : Sub A B} (t : G A) :
  subst_eq f g → bind f t = bind g t.
Proof.
intro Heq; rewrite <- (bind_pure' (bind f t)).
apply bind_bind_comp.
etransitivity; [ | eassumption ].
apply subst_comp_pure_l.
Qed.

Lemma map_to_bind {A B : Obj} (f : Arr A B) (t : G A) :
  fmap f t = bind (of_arrow f) t.
Proof.
rewrite <- (bind_pure' (fmap f t)).
rewrite <- (map_id' (bind (of_arrow f) t)).
apply bind_map_comp.
etransitivity; [ apply subst_comp_pure_l | ].
symmetry; apply arrow_subst_comp_id.
Qed.

Lemma fmap_liftA_shift_comm {A B : Obj} (f : Arr A B) (t : G A) :
  fmap (liftA f) (shift t) = shift (fmap f t).
Proof.
unfold shift.
rewrite (map_map_comp' mk_shift f).
apply map_map_comp, liftA_mk_shift_comm.
Qed.

Lemma bind_liftS_shift_comm {A B : Obj} (f : Sub A B) (a : G A) :
  bind (liftS f) (shift a) = shift (bind f a).
Proof.
unfold shift; apply bind_map_comp.
apply liftS_mk_shift_comm.
Qed.

Lemma subst_shift_id {A : Obj} (t : G A) (v : F A):
  subst (shift t) v = t.
Proof.
unfold shift; rewrite map_to_bind.
unfold subst; rewrite bind_bind_comp'.
apply bind_pure, subst_shift_pure.
Qed.

Lemma fmap_subst {A B : Obj} (f : Arr A B) (t : G (Inc A)) (v : F A) :
  fmap f (subst t v) = subst (fmap (liftA f) t) (fmap f v).
Proof.
symmetry; apply bind_map_comp; symmetry.
apply map_mk_subst_comm.
Qed.

Lemma shift_subst {Inc' : Obj → Obj} {Sh' : ShiftableCore Arr Inc'}
  {A : Obj} (t : G (Inc A)) (v : F A) :
  shift (Inc:=Inc') (subst t v) = subst (fmap (liftA mk_shift) t) (shift v).
Proof.
apply fmap_subst.
Qed.

Lemma bind_subst {A B : Obj} (f : Sub A B) (t : G (Inc A)) (v : F A) :
  bind f (subst t v) = subst (bind (liftS f) t) (bind f v).
Proof.
unfold subst at 2.
rewrite bind_bind_comp'; apply bind_bind_comp.
apply bind_mk_subst_comm.
Qed.

End Properties.