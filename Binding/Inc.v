Require Import Utf8.

Notation "∅" := Empty_set.

Inductive inc (V : Set) : Set :=
| VZ : inc V
| VS : V → inc V
.

Arguments VZ {V}.
Arguments VS {V}.

Definition inc_map {A B : Set} (f : A → B) (m : inc A) : inc B :=
  match m with
  | VZ   => VZ
  | VS x => VS (f x)
  end.