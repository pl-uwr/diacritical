## Binding variables with de Bruijn indices as a nested data type.

This directory contains a library for formalizing variable binding
using de Bruijn notation represented as a nested data type [10].
