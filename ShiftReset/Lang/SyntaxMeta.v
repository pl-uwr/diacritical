(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This files contains administrative lemmas related to the binding     *)
(* representation chosen for this formalization.                        *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib Binding.Set Binding.Auto.
Require Import ShiftReset.Lang.Syntax.

(** Administrative lemmas showing that the meta operations
    on term syntax (tmap and tbind) are well-behaved with context
    plugging and context composition.
*)

Lemma msnoc_injective {V : Set} {F₁ F₂ : mctx V} {E₁ E₂ : ectx V} :
  msnoc F₁ E₁ = msnoc F₂ E₂ → F₁ = F₂ ∧ E₁ = E₂.
Proof.
  revert F₂; induction F₁ as [ E₁' | E₁' F₁ IHF ]; intro F₂; simpl.
  + destruct F₂ as [ E₂' | E₂' F₂ ]; simpl.
    - injection 1 as []; auto.
    - destruct F₂; discriminate.
  + destruct F₂ as [ E₂' | E₂' F₂ ]; simpl.
    - destruct F₁; discriminate.
    - intro H; injection H; clear H.
      intros Heq H; destruct (IHF _ Heq); subst; auto.
Qed.

Lemma tmap_eplug {A B : Set} (f : A [→] B) (E : ectx A) (t : term A) :
  tmap f (eplug E t) = eplug (emap f E) (tmap f t).
Proof.
  induction E; simpl; try (rewrite IHE); reflexivity.
Qed.

Lemma emap_ecomp {A B : Set} (f : A [→] B) (E E' : ectx A) :
  emap f (ecomp E E') = ecomp (emap f E) (emap f E').
Proof.
  induction E; simpl; try (rewrite IHE); reflexivity.
Qed.

Lemma tmap_mplug {A B : Set} (f : A [→] B) (F : mctx A) (t : term A) :
  tmap f (mplug F t) = mplug (mmap f F) (tmap f t).
Proof.
  induction F; simpl; rewrite tmap_eplug; simpl; try rewrite IHF; reflexivity.
Qed.

Lemma mmap_mcomp {A B : Set} (f : A [→] B) (F F' : mctx A) :
  mmap f (mcomp F F') = mcomp (mmap f F) (mmap f F').
Proof.
  induction F as [ E | E F IHF ]; simpl.
  + destruct F'; simpl; rewrite emap_ecomp; reflexivity.
  + rewrite IHF; reflexivity.
Qed.

Lemma mmap_msnoc {A B : Set} (f : A [→] B) (F : mctx A) (E : ectx A) :
  mmap f (msnoc F E) = msnoc (mmap f F) (emap f E).
Proof.
  induction F; simpl; try rewrite IHF; reflexivity.
Qed.

Lemma tbind_eplug {A B : Set} (f : A [⇒] B) (E : ectx A) (t : term A) :
  tbind f (eplug E t) = eplug (ebind f E) (tbind f t).
Proof.
  induction E; simpl; try (rewrite IHE); reflexivity.
Qed.

Lemma tbind_mplug {A B : Set} (f : A [⇒] B) (F : mctx A) (t : term A) :
  tbind f (mplug F t) = mplug (mbind f F) (tbind f t).
Proof.
  induction F; simpl; rewrite tbind_eplug; simpl; try rewrite IHF; reflexivity.
Qed.

Lemma mbind_msnoc {A B : Set} (f : A [⇒] B) (F : mctx A) (E : ectx A) :
  mbind f (msnoc F E) = msnoc (mbind f F) (ebind f E).
Proof.
  induction F; simpl; try rewrite IHF; reflexivity.
Qed.

Hint Rewrite -> @tmap_eplug  : term_simpl_raw.
Hint Rewrite -> @tmap_mplug  : term_simpl_raw.
Hint Rewrite -> @emap_ecomp  : term_simpl_raw.
Hint Rewrite -> @mmap_mcomp  : term_simpl_raw.
Hint Rewrite -> @mmap_msnoc  : term_simpl_raw.

Hint Rewrite -> @tbind_eplug : term_simpl_raw.
Hint Rewrite -> @tbind_mplug : term_simpl_raw.
Hint Rewrite -> @mbind_msnoc : term_simpl_raw.

Lemma ecomp_hole_r {V : Set} (E : ectx V) :
  ecomp E ectx_hole = E.
Proof.
  induction E; simpl; try rewrite IHE; reflexivity.
Qed.

Lemma mcomp_hole_r {V : Set} (F : mctx V) :
  mcomp F ectx_hole = F.
Proof.
  induction F as [ E | E F IHF ]; simpl.
  + rewrite ecomp_hole_r; reflexivity.
  + rewrite IHF; reflexivity.
Qed.

Hint Rewrite -> @ecomp_hole_r : term_simpl_raw.
Hint Rewrite -> @mcomp_hole_r : term_simpl_raw.

Lemma eplug_ecomp {V : Set} (E E' : ectx V) (t : term V) :
  eplug (ecomp E E') t = eplug E (eplug E' t).
Proof.
  induction E; simpl; try (rewrite IHE); reflexivity.
Qed.

Lemma mplug_mcomp {V : Set} (F F' : mctx V) (t : term V) :
  mplug (mcomp F F') t = mplug F (mplug F' t).
Proof.
  induction F as [ E | E F IHF ]; simpl.
  + destruct F'; apply eplug_ecomp.
  + rewrite IHF; reflexivity.
Qed.

Lemma mplug_msnoc {V : Set} (F : mctx V) (E : ectx V) (t : term V) :
  mplug (msnoc F E) t = mplug F (t_rst (eplug E t)).
Proof.
  induction F; simpl; try rewrite IHF; reflexivity.
Qed.

Lemma mcomp_msnoc {V : Set} (F F' : mctx V) (E : ectx V) :
  mcomp F (msnoc F' E) = msnoc (mcomp F F') E.
Proof.
  induction F as [ E' | E' F IHF ]; simpl.
  + destruct F'; reflexivity.
  + rewrite IHF; reflexivity.
Qed.

Lemma mcomp_msnoc_pure {V : Set} (F : mctx V) (E E' : ectx V) :
  mcomp (msnoc F E) E' = msnoc F (ecomp E E').
Proof.
  induction F as [ E'' | E'' F ]; simpl; try rewrite IHF; reflexivity.
Qed.

Instance SetPure_value : SetPure value.
Proof. split; intros; reflexivity. Qed.

Fixpoint tmap_id {A : Set} (f : A [→] A) (t : term A) :
  arrow_eq f arrow_id → tmap f t = t
with pmap_id {A : Set} (f : A [→] A) (t : pure_term A) :
       arrow_eq f arrow_id → pmap f t = t
with vmap_id {A : Set} (f : A [→] A) (v : value A) :
       arrow_eq f arrow_id → vmap f v = v.
Proof.
  + auto_map_id.
  + auto_map_id.
  + auto_map_id.
Qed.

Fixpoint tmap_map_comp {A B C : Set}
         (f : B [→] C) (g : A [→] B) h (t : term A) :
  arrow_eq (arrow_comp f g) h → tmap f (tmap g t) = tmap h t
with pmap_map_comp {A B C : Set}
                   (f : B [→] C) (g : A [→] B) h (t : pure_term A) :
       arrow_eq (arrow_comp f g) h → pmap f (pmap g t) = pmap h t
with vmap_map_comp {A B C : Set}
                   (f : B [→] C) (g : A [→] B) h (v : value A) :
       arrow_eq (arrow_comp f g) h → vmap f (vmap g v) = vmap h v.
Proof.
  + auto_map_map_comp.
  + auto_map_map_comp.
  + auto_map_map_comp.
Qed.

Instance FMap_tmap : FMap (@tmap).
Proof.
  split.
  + exact @tmap_id.
  + exact @tmap_map_comp.
Qed.

Instance FMap_vmap : FMap (@vmap).
Proof.
  split.
  + exact @vmap_id.
  + exact @vmap_map_comp.
Qed.

Fixpoint emap_id {A : Set} (f : A [→] A) (E : ectx A) :
  arrow_eq f arrow_id → emap f E = E.
Proof. auto_map_id. Qed.

Fixpoint emap_map_comp {A B C : Set}
         (f : B [→] C) (g : A [→] B) h (E : ectx A) :
  arrow_eq (arrow_comp f g) h → emap f (emap g E) = emap h E.
Proof. auto_map_map_comp. Qed.

Instance FMap_emap : FMap (@emap).
Proof.
  split.
  + exact @emap_id.
  + exact @emap_map_comp.
Qed.

Fixpoint mmap_id {A : Set} (f : A [→] A) (F : mctx A) :
  arrow_eq f arrow_id → mmap f F = F.
Proof. auto_map_id. Qed.

Fixpoint mmap_map_comp {A B C : Set}
         (f : B [→] C) (g : A [→] B) h (F : mctx A) :
  arrow_eq (arrow_comp f g) h → mmap f (mmap g F) = mmap h F.
Proof. auto_map_map_comp. Qed.

Instance FMap_mmap : FMap (@mmap).
Proof.
  split.
  + exact @mmap_id.
  + exact @mmap_map_comp.
Qed.

Fixpoint tbind_map_comp {A B B' C : Set}
         (f : B [⇒] C) (g : A [→] B) (g' : B' [→] C) (f' : A [⇒] B')
         (t : term A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  tbind f (tmap g t) = tmap g' (tbind f' t)
with pbind_map_comp {A B B' C : Set}
                    (f : B [⇒] C) (g : A [→] B) (g' : B' [→] C) (f' : A [⇒] B')
                    (t : pure_term A) :
       subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
       pbind f (pmap g t) = pmap g' (pbind f' t)
with vbind_map_comp {A B B' C : Set}
                    (f : B [⇒] C) (g : A [→] B) (g' : B' [→] C) (f' : A [⇒] B')
                    (v : value A) :
       subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
       vbind f (vmap g v) = vmap g' (vbind f' v).
Proof.
  + auto_bind_map_comp.
  + auto_bind_map_comp.
  + auto_bind_map_comp.
Qed.

Instance BindMap_tmap_tbind : BindMap (@tmap) (@tbind).
Proof.
  split.
  + exact @tbind_map_comp.
Qed.

Instance BindMap_vmap_vbind : BindMap (@vmap) (@vbind).
Proof.
  split.
  + exact @vbind_map_comp.
Qed.

Fixpoint ebind_map_comp {A B B' C : Set}
         (f : B [⇒] C) (g : A [→] B) (g' : B' [→] C) (f' : A [⇒] B')
         (E : ectx A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  ebind f (emap g E) = emap g' (ebind f' E).
Proof. auto_bind_map_comp. Qed.

Instance BindMap_emap_ebind : BindMap (@emap) (@ebind).
Proof.
  split.
  + exact @ebind_map_comp.
Qed.

Fixpoint mbind_map_comp {A B B' C : Set}
         (f : B [⇒] C) (g : A [→] B) (g' : B' [→] C) (f' : A [⇒] B')
         (F : mctx A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  mbind f (mmap g F) = mmap g' (mbind f' F).
Proof. auto_bind_map_comp. Qed.

Instance BindMap_mmap_mbind : BindMap (@mmap) (@mbind).
Proof.
  split.
  + exact @mbind_map_comp.
Qed.

Fixpoint tbind_pure {A : Set}
         (f : A [⇒] A) (t : term A) : subst_eq f subst_pure → tbind f t = t
with pbind_pure {A : Set}
                (f : A [⇒] A) (t : pure_term A) : subst_eq f subst_pure → pbind f t = t
with vbind_pure {A : Set}
                (f : A [⇒] A) (v : value A) : subst_eq f subst_pure → vbind f v = v.
Proof.
  + auto_bind_pure.
  + auto_bind_pure.
  + auto_bind_pure.
Qed.

Fixpoint tbind_bind_comp {A B C : Set}
         (f : B [⇒] C) (g : A [⇒] B) h (t : term A) :
  subst_eq (subst_comp f g) h → tbind f (tbind g t) = tbind h t
with pbind_bind_comp {A B C : Set}
                     (f : B [⇒] C) (g : A [⇒] B) h (t : pure_term A) :
       subst_eq (subst_comp f g) h → pbind f (pbind g t) = pbind h t
with vbind_bind_comp {A B C : Set}
                     (f : B [⇒] C) (g : A [⇒] B) h (v : value A) :
       subst_eq (subst_comp f g) h → vbind f (vbind g v) = vbind h v.
Proof.
  + auto_bind_bind_comp.
  + auto_bind_bind_comp.
  + auto_bind_bind_comp.
Qed.

Instance Bind_tbind : Bind (@tbind).
Proof.
  split.
  + exact @tbind_pure.
  + exact @tbind_bind_comp.
Qed.

Instance Bind_vbind : Bind (@vbind).
Proof.
  split.
  + exact @vbind_pure.
  + exact @vbind_bind_comp.
Qed.

Fixpoint ebind_pure {A : Set}
         (f : A [⇒] A) (E : ectx A) : subst_eq f subst_pure → ebind f E = E.
Proof. auto_bind_pure. Qed.

Fixpoint ebind_bind_comp {A B C : Set}
         (f : B [⇒] C) (g : A [⇒] B) h (E : ectx A) :
  subst_eq (subst_comp f g) h → ebind f (ebind g E) = ebind h E.
Proof. auto_bind_bind_comp. Qed.

Instance Bind_ebind : Bind (@ebind).
Proof.
  split.
  + exact @ebind_pure.
  + exact @ebind_bind_comp.
Qed.

Fixpoint mbind_pure {A : Set}
         (f : A [⇒] A) (F : mctx A) : subst_eq f subst_pure → mbind f F = F.
Proof. auto_bind_pure. Qed.

Fixpoint mbind_bind_comp {A B C : Set}
         (f : B [⇒] C) (g : A [⇒] B) h (F : mctx A) :
  subst_eq (subst_comp f g) h → mbind f (mbind g F) = mbind h F.
Proof. auto_bind_bind_comp. Qed.

Instance Bind_mbind : Bind (@mbind).
Proof.
  split.
  + exact @mbind_pure.
  + exact @mbind_bind_comp.
Qed.