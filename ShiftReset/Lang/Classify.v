(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file provides a classification of terms and contexts            *)
(* in the CBV λ-calculus with shift and reset.                          *)
(************************************************************************)

Require Import Utf8.
Require Import ShiftReset.Lang.Syntax.
Require Import ShiftReset.Lang.Reduction.

(** A term either makes a reduction step,
    or it is a value (canonical form),
    or it is an open stuck term,
    or it is a control-stuck term.
*)

Inductive class {V : Set} : term V → Type :=
| c_step  : ∀ t t', red t t' → class t
| c_value : ∀ v : value V, class v
| c_open  : ∀ F x (v : value V), class (mplug F (t_app (v_var x) v))
| c_ctrl  : ∀ E (v : value V), class (eplug E (t_app v_sft v)).

Inductive pure_class {V : Set} : pure_term V → Type :=
| pc_step  : ∀ (t t' : pure_term V), red t t' → pure_class t
| pc_value : ∀ v : value V, pure_class v
| pc_open  : ∀ F x (v : value V),
    pure_class (t_rst (mplug F (t_app (v_var x) v))).

Fixpoint classify {V : Set} (t : term V) : class t
with classify_pure {V : Set} (t : pure_term V) : pure_class t.
Proof.
  + destruct t as [ t | t s ].
    - destruct (classify_pure _ t) as [ t t' Hred | v | F x v ].
      * eapply c_step, Hred.
      * apply c_value.
      * apply (c_open (mctx_rst ectx_hole F)).
    - destruct (classify _ t) as [ t t' Hred | v | [ E | E F ] x v | E v ].
      * eapply c_step, red_app1, Hred.
      * { destruct (classify _ s) as [ s s' Hred | u | [ E | E F ] x u | E u ].
          + eapply c_step, red_app2, Hred.
          + destruct v as [ x | t | ].
            - apply (c_open ectx_hole).
            - eapply c_step, red_beta.
            - apply (c_ctrl ectx_hole).
          + apply (c_open (ectx_app2 v E)).
          + apply (c_open (mctx_rst (ectx_app2 v E) F)).
          + apply (c_ctrl (ectx_app2 v E)).
        }
      * apply (c_open (ectx_app1 E s)).
      * apply (c_open (mctx_rst (ectx_app1 E s) F)).
      * apply (c_ctrl (ectx_app1 E s)).
  + destruct t as [ v | t ].
    - apply pc_value.
    - destruct (classify _ t) as [ t t' Hred | v | F x v | E v ].
      * eapply pc_step, red_rst, Hred.
      * eapply pc_step, red_reset.
      * apply pc_open.
      * eapply pc_step, red_shift.
Defined.

(** A general evaluation context (meta-context)
    is either a pure context
    or it decomposes into a pure context
    and a general evaluation context.
*)

Inductive mctx_class {V : Set} : mctx V → Type :=
| mc_pure : ∀ E : ectx V, mctx_class E
| mc_rst  : ∀ F E, mctx_class (msnoc F E).

Fixpoint classify_mctx {V : Set} (F : mctx V) : mctx_class F.
Proof.
  destruct F as [ E | E F ].
  + apply mc_pure.
  + destruct (classify_mctx _ F) as [ E' | F E' ].
    - apply (mc_rst E E').
    - apply (mc_rst (mctx_rst E F) E').
Defined.