(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file formalizes some basic properties of the reduction          *)
(* relation, tacitly assumed in Section 4.                              *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib Binding.Set.
Require Import ShiftReset.Lang.Syntax ShiftReset.Lang.SyntaxMeta.
Require Import ShiftReset.Lang.Reduction.

(** Potential redices. *)

Inductive ppredex {V : Set} : term V → Prop :=
| ppr_app : ∀ v u : value V, ppredex (t_app v u)
| ppr_rst : ∀ t : term V, ppredex (t_rst t).

Ltac discr_red :=
  repeat match goal with
  | H: red (t_pure (t_value _)) _ |- _ => inversion H
  | H: t_pure (t_value _) = eplug ?E (t_app _ _) |- _ =>
    destruct E; discriminate
  | H: t_pure (t_value _) = eplug ?E ?t |- _ =>
    destruct E; try discriminate; simpl in H; subst
  | H: eplug ?E ?t = t_pure (t_value _) |- _ =>
    destruct E; try discriminate; simpl in H; subst
  | H: t_pure (t_value _) = mplug ?F ?t |- _ =>
    destruct F; try discriminate; simpl in H; subst
  | H: ppredex (t_app _ _) |- _ =>
    inversion H; clear H; subst
  | H: ppredex (t_pure _) |- _ =>
    inversion H; clear H; subst
  end.

(** Unique decomposition property. *)

Lemma pure_unique_decomposition {V : Set} {E₁ E₂ : ectx V} {t₁ t₂ : term V} :
  ppredex t₁ → ppredex t₂ → eplug E₁ t₁ = eplug E₂ t₂ → E₁ = E₂ ∧ t₁ = t₂.
Proof.
  intros Ht₁ Ht₂; revert E₂; induction E₁ as [ | E₁ IHE s₁ | v₁ E₁ IHE ];
    intro E₂; simpl.
  + inversion 1; subst.
    destruct E₂; simpl in Ht₁; auto; discr_red.
  + destruct E₂; simpl.
    - intro; subst; discr_red.
    - intro H; injection H; clear H.
      intros H Heq; destruct (IHE _ Heq); subst; auto.
    - injection 1; intros; discr_red.
  + destruct E₂; simpl.
    - intro; subst; discr_red.
    - injection 1; intros; discr_red.
    - intro H; injection H; clear H.
      intros Heq H; destruct (IHE _ Heq); subst; auto.
Qed.

Lemma unique_decomposition_appv {V : Set}
    {F₁ F₂ : mctx V} {v₁ v₂ u₁ u₂ : value V} :
  mplug F₁ (t_app v₁ u₁) = mplug F₂ (t_app v₂ u₂) →
  F₁ = F₂ ∧ v₁ = v₂ ∧ u₁ = u₂.
Proof.
  revert F₂; induction F₁ as [ E₁ | E₁ F₁ IHF ];
    destruct F₂ as [ E₂ | E₂ F₂ ]; simpl.
  + intro Heq; apply pure_unique_decomposition in Heq; try apply ppr_app.
    destruct Heq as [ H Heq ]; injection Heq as []; subst; auto.
  + intro Heq; exfalso; apply pure_unique_decomposition in Heq; try constructor.
    destruct Heq as [ _ ? ]; discriminate.
  + intro Heq; exfalso; apply pure_unique_decomposition in Heq; try constructor.
    destruct Heq as [ _ ? ]; discriminate.
  + intro Heq; apply pure_unique_decomposition in Heq; try apply ppr_rst.
    destruct Heq as [ H Heq ]; subst.
    injection Heq; clear Heq; intro Heq.
    apply IHF in Heq; destruct Heq; subst; auto.
Qed.

(** Control-stuck terms are different from open stuck terms. *)

Lemma ctrl_stuck_is_not_open_stuck {V : Set} (E : ectx V) (F : mctx V) x
    (v₁ v₂ : value V) :
  eplug E (t_app v_sft v₁) = mplug F (t_app (v_var x) v₂) → False.
Proof.
  intro Heq; apply (unique_decomposition_appv (F₁ := E)) in Heq.
  destruct Heq as [ _ [ ? _ ] ]; discriminate.
Qed.

(** An open stuck term is irreducible. *)

Lemma open_stuck_is_stuck {V : Set} (F : mctx V) (x : V) (v : value V) t :
  red (mplug F (t_app (v_var x) v)) t → False.
Proof.
  revert t; induction F as [ E | E F IHF ]; simpl.
  + induction E as [ | E IHE s | u E IHE ]; simpl; intro t.
    - inversion 1; subst; discr_red.
    - inversion 1; subst; discr_red.
      eapply IHE; eassumption.
    - inversion 1; subst; discr_red.
      eapply IHE; eassumption.
  + induction E as [ | E IHE s | u E IHE ]; simpl; intro t.
    - inversion 1; subst.
      * eapply ctrl_stuck_is_not_open_stuck; eassumption.
      * discr_red.
      * eapply IHF; eassumption.
    - inversion 1; subst; discr_red.
      eapply IHE; eassumption.
    - inversion 1; subst; discr_red.
      eapply IHE; eassumption.
Qed.

(** A control-stuck term is irreducible. *)

Lemma ctrl_stuck_is_stuck {V : Set} (E : ectx V) (v : value V) t :
  red (eplug E (t_app v_sft v)) t → False.
Proof.
  revert t; induction E as [ | E IHE s | u E IHE ]; simpl; intro t.
  + inversion 1; subst; discr_red.
  + inversion 1; subst; discr_red.
    eapply IHE; eassumption.
  + inversion 1; subst; discr_red.
    eapply IHE; eassumption.
Qed.

(** An open stuck term decomposes uniquely. *)

Lemma open_stuck_unique {V : Set}
      (F₁ F₂ : mctx V) (x₁ x₂ : V) (v₁ v₂ : value V) :
  mplug F₁ (t_app (v_var x₁) v₁) = mplug F₂ (t_app (v_var x₂) v₂) →
  F₁ = F₂ ∧ x₁ = x₂ ∧ v₁ = v₂.
Proof.
  intro Heq; apply unique_decomposition_appv in Heq.
  destruct Heq as [ ? [ Heq ? ] ]; subst.
  injection Heq; auto.
Qed.

(** A control-stuck term decomposes uniquely. *)

Lemma ctrl_stuck_unique {V : Set} {E₁ E₂ : ectx V} {v₁ v₂ : value V} :
  eplug E₁ (t_app v_sft v₁) = eplug E₂ (t_app v_sft v₂) → E₁ = E₂ ∧ v₁ = v₂.
Proof.
  intro Heq; apply pure_unique_decomposition in Heq; try apply ppr_app.
  destruct Heq as [ HE Heq ]; injection Heq as []; auto.
Qed.

(** One-step reduction relation is deterministic. *)

Lemma red_determ {V : Set} {t t₁ t₂ : term V} :
  red t t₁ → red t t₂ → t₁ = t₂.
Proof.
  intro Hred₁; revert t₂.
  induction Hred₁ as
      [ t v | E v | v | t t' s Hred₁ IH | v t t' Hred₁ IH | t t' Hred₁ IH ];
    intro t₂.
  + inversion 1; subst; discr_red.
    reflexivity.
  + inversion 1; subst; discr_red.
    - match goal with
      | H: eplug _ _ = eplug _ _ |- _ =>
        destruct (ctrl_stuck_unique H); subst; reflexivity
      end.
    - exfalso; eapply ctrl_stuck_is_stuck; eassumption.
  + inversion 1; subst; discr_red.
    reflexivity.
  + inversion 1; subst; discr_red.
    erewrite IH; [ reflexivity | ]; assumption.
  + inversion 1; subst; discr_red.
    erewrite IH; [ reflexivity | ]; assumption.
  + inversion 1; subst; discr_red.
    - exfalso; eapply ctrl_stuck_is_stuck; eassumption.
    - erewrite IH; [ reflexivity | ]; assumption.
Qed.

(** Reduction preserves purity of terms. *)

Lemma red_pure {V : Set} {t : pure_term V} {t' : term V} :
  red t t' → ∃ t'' : pure_term V, t' = t''.
Proof.
  inversion 1; eexists; reflexivity.
Qed.

Lemma red_rtc_pure {V : Set} {t : pure_term V} {t' : term V} :
  red_rtc t t' → ∃ t'' : pure_term V, t' = t''.
Proof.
  intro Hred; remember (t_pure t) as t₀ eqn: Heq; generalize t Heq; clear t Heq.
  induction Hred as [ t₀ | t₀ t₁ t₂ Hred Hrtc IH ].
  + intros; eexists; eassumption.
  + intros p Heq; subst.
    apply red_pure in Hred; destruct Hred; subst.
    eapply IH; reflexivity.
Qed.

(** One-step reduction relation is compatible wrt pure evaluation contexts. *)

Lemma red_in_ectx {V : Set} (E : ectx V) {t t' : term V} :
  red t t' → red (eplug E t) (eplug E t').
Proof.
  intro Hred; induction E as [ | E IHE s | v E IHE ]; simpl.
  + assumption.
  + apply red_app1; assumption.
  + apply red_app2; assumption.
Qed.

Lemma red_rtc_in_ectx {V : Set} (E : ectx V) {t t' : term V} :
  red_rtc t t' → red_rtc (eplug E t) (eplug E t').
Proof.
  induction 1; [ reflexivity | ].
  econstructor 2; [ | eassumption ].
  apply red_in_ectx; assumption.
Qed.

(** One-step reduction relation is compatible wrt evaluation contexts. *)

Lemma red_in_mctx {V : Set} (F : mctx V) {t t' : term V} :
  red t t' → red (mplug F t) (mplug F t').
Proof.
  intro Hred; induction F as [ E | E F IHF ]; simpl.
  + apply red_in_ectx; assumption.
  + apply red_in_ectx, red_rst, IHF.
Qed.

Lemma red_rtc_in_mctx {V : Set} (F : mctx V) {t t' : term V} :
  red_rtc t t' → red_rtc (mplug F t) (mplug F t').
Proof.
  induction 1; [ reflexivity | ].
  econstructor 2; [ | eassumption ].
  apply red_in_mctx; assumption.
Qed.

(** One-step reduction relation is compatible wrt substitution. *)

Lemma red_fmap {A B : Set} (f : A [→] B) {t t' : term A} :
  red t t' → red (fmap f t) (fmap f t').
Proof.
  induction 1 as
      [ t v | E v | v | t t' s Hred IH | v t t' Hred IH | t t' Hred IH ];
    term_simpl.
  + apply red_beta.
  + apply red_shift.
  + apply red_reset.
  + apply red_app1, IH.
  + apply red_app2, IH.
  + apply red_rst, IH.
Qed.

Lemma red_bind {A B : Set} (f : A [⇒] B) {t t' : term A} :
  red t t' → red (bind f t) (bind f t').
Proof.
  induction 1 as
    [ t v | E v | v | t t' s Hred IH | v t t' Hred IH | t t' Hred IH ];
    term_simpl.
  + apply red_beta.
  + apply red_shift.
  + apply red_reset.
  + apply red_app1, IH.
  + apply red_app2, IH.
  + apply red_rst, IH.
Qed.

Lemma red_subst {V : Set} (v : value V) {t t' : term (inc V)} :
  red t t' → red (subst t v) (subst t' v).
Proof.
  apply red_bind.
Qed.

Lemma red_rtc_fmap {A B : Set} (f : A [→] B) {t t' : term A} :
  red_rtc t t' → red_rtc (fmap f t) (fmap f t').
Proof.
  induction 1; [ reflexivity | ].
  econstructor 2; [ | eassumption ].
  apply red_fmap; assumption.
Qed.

Lemma red_rtc_bind {A B : Set} (f : A [⇒] B) {t t' : term A} :
  red_rtc t t' → red_rtc (bind f t) (bind f t').
Proof.
  induction 1; [ reflexivity | ].
  econstructor 2; [ | eassumption ].
  apply red_bind; assumption.
Qed.

Lemma red_rtc_subst {V : Set} (v : value V) {t t' : term (inc V)} :
  red_rtc t t' → red_rtc (subst t v) (subst t' v).
Proof.
  apply red_rtc_bind.
Qed.
