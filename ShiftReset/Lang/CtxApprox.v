(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file formalizes a notion of contextual approximation.           *)
(* Its symmetric closure yields the contextual equivalence relation     *)
(* of Section 4.                                                        *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib Binding.Set.
Require Import ShiftReset.Lang.Syntax.
Require Import ShiftReset.Lang.Reduction.

Inductive ctx : Set → Type :=
| ctx_hole : ctx ∅
| ctx_map  : ∀ A B : Set, (A [→] B) → ctx B → ctx A
| ctx_app1 : ∀ V : Set, ctx V → term V → ctx V
| ctx_app2 : ∀ V : Set, term V → ctx V → ctx V
| ctx_rst  : ∀ V : Set, ctx V → ctx V
| ctx_lam  : ∀ V : Set, ctx V → ctx (inc V).

Arguments ctx_map  {A B} f C.
Arguments ctx_app1 {V}   C t.
Arguments ctx_app2 {V}   t C.
Arguments ctx_rst  {V}   C.
Arguments ctx_lam  {V}   C.

Fixpoint plug {V : Set} (C : ctx V) : term V → term ∅ :=
  match C in ctx V return term V → term ∅ with
  | ctx_hole      => λ t, t
  | ctx_map   f C => λ t, plug C (fmap f t)
  | ctx_app1  C s => λ t, plug C (t_app t s)
  | ctx_app2  s C => λ t, plug C (t_app s t)
  | ctx_rst   C   => λ t, plug C (t_rst t)
  | ctx_lam   C   => λ t, plug C (v_lam t)
  end.

Record obs_approx (t₁ t₂ : term ∅) : Prop :=
  { obs_value : ∀ (v₁ : value ∅), red_rtc t₁ v₁ →
                ∃ (v₂ : value ∅), red_rtc t₂ v₂
  ; obs_ctrl :
    ∀ (E₁ : ectx ∅) (v₁ : value ∅), red_rtc t₁ (eplug E₁ (t_app v_sft v₁)) →
    ∃ (E₂ : ectx ∅) (v₂ : value ∅), red_rtc t₂ (eplug E₂ (t_app v_sft v₂))
  }.

Definition ctx_approx {V : Set} (t₁ t₂ : term V) : Prop :=
  ∀ (C : ctx V), obs_approx (plug C t₁) (plug C t₂).
