(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file formalizes the syntax of the CBV λ-calculus                *)
(* with shift and reset, presented in Section 4.1 of the paper.         *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib Binding.Set.

(** Terms, including pure terms. *)

Inductive term (V : Set) : Type :=
| t_pure  : pure_term V → term V
| t_app   : term V → term V → term V
with pure_term (V : Set) : Type :=
| t_value : value V → pure_term V
| t_rst   : term V → pure_term V
with value (V : Set) : Type :=
| v_var    : V → value V
| v_lam    : term (inc V) → value V
| v_sft    : value V.

Arguments t_pure  {V}.
Arguments t_app   {V}.
Arguments t_value {V}.
Arguments t_rst   {V}.

Arguments v_var    {V}.
Arguments v_lam    {V}.
Arguments v_sft    {V}.

Coercion t_value : value >-> pure_term.
Coercion t_pure  : pure_term >-> term.

Instance SetPureCore_value : SetPureCore value :=
  { set_pure := @v_var }.

Arguments SetPureCore_value {A} /.

(** Pure evaluation contexts. *)

Inductive ectx (V : Set) : Type :=
| ectx_hole : ectx V
| ectx_app1 : ectx V  → term V → ectx V
| ectx_app2 : value V → ectx V → ectx V.

Arguments ectx_hole {V}.
Arguments ectx_app1 {V}.
Arguments ectx_app2 {V}.

(** General evaluation contexts (meta-contexts). *)

Inductive mctx (V : Set) : Type :=
| mctx_pure : ectx V → mctx V
| mctx_rst  : ectx V → mctx V → mctx V.

Arguments mctx_pure {V}.
Arguments mctx_rst  {V}.

Coercion mctx_pure : ectx >-> mctx.

Fixpoint eplug {V : Set} (E : ectx V) (t : term V) : term V :=
  match E with
  | ectx_hole     => t
  | ectx_app1 E s => t_app (eplug E t) s
  | ectx_app2 v E => t_app v (eplug E t)
  end.

Fixpoint ecomp {V : Set} (E E' : ectx V) : ectx V :=
  match E with
  | ectx_hole     => E'
  | ectx_app1 E t => ectx_app1 (ecomp E E') t
  | ectx_app2 v E => ectx_app2 v (ecomp E E')
  end.

Fixpoint mplug {V : Set} (F : mctx V) (t : term V) : term V :=
  match F with
  | mctx_pure E   => eplug E t
  | mctx_rst  E F => eplug E (t_rst (mplug F t))
  end.

Fixpoint mcomp {V : Set} (F F' : mctx V) : mctx V :=
  match F with
  | mctx_pure E =>
    match F' with
    | mctx_pure E'    => ecomp E E'
    | mctx_rst  E' F' => mctx_rst (ecomp E E') F'
    end
  | mctx_rst E F => mctx_rst E (mcomp F F')
  end.

Fixpoint msnoc {V : Set} (F : mctx V) (E : ectx V) : mctx V :=
  match F with
  | mctx_pure E'   => mctx_rst E' E
  | mctx_rst  E' F => mctx_rst E' (msnoc F E)
  end.

Fixpoint tmap {A B : Set} (f : A [→] B) (t : term A) : term B :=
  match t with
  | t_pure p   => pmap f p
  | t_app  t s => t_app (tmap f t) (tmap f s)
  end
with pmap {A B : Set} (f : A [→] B) (p : pure_term A) : pure_term B :=
  match p with
  | t_value v => vmap f v
  | t_rst   t => t_rst (tmap f t)
  end
with vmap {A B : Set} (f : A [→] B) (v : value A) : value B :=
  match v with
  | v_var x  => v_var (f x)
  | v_lam t  => v_lam (tmap (liftA f) t)
  | v_sft    => v_sft
  end.

Instance IsMapCore_tmap : IsMapCore (@tmap).
Instance IsMapCore_pmap : IsMapCore (@pmap).
Instance IsMapCore_vmap : IsMapCore (@vmap).

Fixpoint emap {A B : Set} (f : A [→] B) (E : ectx A) : ectx B :=
  match E with
  | ectx_hole     => ectx_hole
  | ectx_app1 E t => ectx_app1 (emap f E) (fmap f t)
  | ectx_app2 v E => ectx_app2 (fmap f v) (emap f E)
  end.

Instance IsMapCore_fmap : IsMapCore (@emap).

Fixpoint mmap {A B : Set} (f : A [→] B) (F : mctx A) : mctx B :=
  match F with
  | mctx_pure E   => mctx_pure (fmap f E)
  | mctx_rst  E F => mctx_rst  (fmap f E) (mmap f F)
  end.

Instance IsMapCore_mmap : IsMapCore (@mmap).

Fixpoint tbind {A B : Set} (f : A [⇒] B) (t : term A) : term B :=
  match t with
  | t_pure p   => pbind f p
  | t_app  t s => t_app (tbind f t) (tbind f s)
  end
with pbind {A B : Set} (f : A [⇒] B) (p : pure_term A) : pure_term B :=
  match p with
  | t_value v => vbind f v
  | t_rst   t => t_rst (tbind f t)
  end
with vbind {A B : Set} (f : A [⇒] B) (v : value A) : value B :=
  match v with
  | v_var x  => f x
  | v_lam t  => v_lam (tbind (liftS f) t)
  | v_sft    => v_sft
  end.

Instance IsBindCore_tbind : IsBindCore (@tbind).
Instance IsBindCore_pbind : IsBindCore (@pbind).
Instance IsBindCore_vbind : IsBindCore (@vbind).

Fixpoint ebind {A B : Set} (f : A [⇒] B) (E : ectx A) : ectx B :=
  match E with
  | ectx_hole     => ectx_hole
  | ectx_app1 E t => ectx_app1 (ebind f E) (bind f t)
  | ectx_app2 v E => ectx_app2 (bind f v) (ebind f E)
  end.

Instance IsBindCore_ebind : IsBindCore (@ebind).

Fixpoint mbind {A B : Set} (f : A [⇒] B) (F : mctx A) : mctx B :=
  match F with
  | mctx_pure E   => mctx_pure (bind f E)
  | mctx_rst  E F => mctx_rst  (bind f E) (mbind f F)
  end.

Instance IsBindCore_mbind : IsBindCore (@mbind).