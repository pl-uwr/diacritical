(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file gathers and exports the notions related to the syntax      *)
(* and reduction semantics of the calculus.                             *)
(************************************************************************)

Require Export Binding.Lib Binding.Set.

Require Export ShiftReset.Lang.Classify.
Require Export ShiftReset.Lang.Reduction.
Require Export ShiftReset.Lang.ReductionProperties.
Require Export ShiftReset.Lang.Syntax.
Require Export ShiftReset.Lang.SyntaxMeta.