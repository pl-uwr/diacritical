(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file presents an SOS-like formulation                           *)
(* of the reduction semantics of Section 4.1.                           *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib.
Require Import ShiftReset.Lang.Syntax.

(** One-step reduction relation. *)

Inductive red {V : Set} : term V → term V → Prop :=
| red_beta : ∀ (t : term (inc V)) (v : value V),
    red (t_app (v_lam t) v) (subst t v)
| red_shift : ∀ (E : ectx V) (v : value V),
    red (t_rst (eplug E (t_app v_sft v)))
        (t_rst (t_app v (v_lam (t_rst (eplug (shift E) (v_var VZ))))))
| red_reset : ∀ v : value V,
    red (t_rst v) v
| red_app1 : ∀ (t t' s : term V),
    red t t' →
    red (t_app t s) (t_app t' s)
| red_app2 : ∀ (v : value V) (t t' : term V),
    red t t' →
    red (t_app v t) (t_app v t')
| red_rst : ∀ t t' : term V,
    red t t' →
    red (t_rst t) (t_rst t').

Lemma red_shift' {V : Set} (E : ectx V) (v : value V) t :
  t = t_rst (t_app v (v_lam (t_rst (eplug (shift E) (v_var VZ))))) →
  red (t_rst (eplug E (t_app v_sft v))) t.
Proof.
  intro; subst; apply red_shift.
Qed.

(** Reflexive transitive closure of red. *)

Require Import Relation_Operators.
Require Import RelationClasses.

Notation red_rtc := (@clos_refl_trans_1n _ (@red _)).

Instance Reflexive_red_rtc {V : Set} :
  Reflexive (clos_refl_trans_1n _ (@red V)).
Proof.
  constructor 1.
Qed.

Instance Transitive_red_rtc {V : Set} :
  Transitive (clos_refl_trans_1n _ (@red V)).
Proof.
  intros t₁ t₂ t₃; induction 1; [ auto | ]; intro H3.
  econstructor 2; [ eassumption | ]; auto.
Qed.