## An extensional normal-form bisimilarity for the CBV λ-calculus with shift and reset

This directory contains a formalization of Sections 4.1–4.2, and it is
structured as follows:

* The [Lang](Lang/) directory contains a formalization of the
  calculus (Section 4.1), using the library [Binding](../Binding/).

* The [Simulation](Simulation/) directory contains a formalization of
  simulations for the lambda calculus with shift and reset (Section 4.2),
  using the library [Diacritical](../Diacritical/).
