(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for pctxrst.                *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib.
Require Import ShiftReset.Simulation.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Continuity.
Require Import ShiftReset.Simulation.UpToTechniques.Monotonicity.

Require Import ShiftReset.Simulation.UpToTechniques.Combo.
Require Import ShiftReset.Simulation.UpToTechniques.SubstV.
Require Import ShiftReset.Simulation.UpToTechniques.SubstRename.

Instance Evolution_pctxrst :
  pctxrst ↝ combo_weak & combo.
Proof.
  intros R S Hpr; split;
    [ take pctxrst; take_done; reflexivity
    | take pctxrst; take_done; apply Hpr | ].
  intros e [ V E₁ E₂ t₁ t₂ HE Ht ].
  apply (progress_rel Hpr) in Ht; simpl in Ht.
  destruct (classify t₁) as [ t₁ t₁' Hred₁ | v₁ | F₁ x v₁ | E₁' v₁ ].
  + destruct Ht as [ t₂' [ Hred₂ Ht' ] ].
    eapply progress_step; [ apply red_rst, red_in_ectx; eassumption | ].
    apply (red_rtc_in_mctx (mctx_rst ectx_hole E₂)) in Hred₂; simpl in Hred₂.
    exists (t_rst (eplug E₂ t₂')); split; [ assumption | ].
    take pctxrst; constructor; take_done; pr_assumption.
  + destruct Ht as [ v₂ [ Hred₂ Hv ] ].
    eapply elem_progress_red_rtc_r;
      [ eapply (red_rtc_in_mctx (mctx_rst ectx_hole E₂)); eassumption | ].
    unfold rst_ectx_rel in HE.
    apply (Mk_subst_v v₁ v₂) in HE; [ | assumption ]; term_simpl in HE.
    apply Evolution_subst_v in Hpr; apply (progress_rel Hpr) in HE.
    eapply elem_progress_monotone; [ | reflexivity | eassumption ].
    apply (combo_str_in_combo_weak _ _).
  + destruct Ht as [ F₂ [ v₂ [ Hred₂ [ HF Hv ] ] ] ].
    change (elem_progress (combo_weak R)
                          (combo S)
                          (mk_elem V
                                   (mplug (mctx_rst ectx_hole E₁)
                                          (mplug F₁ (t_app (v_var x) v₁)))
                                   (t_rst (eplug E₂ t₂)))).
    rewrite <- mplug_mcomp; apply progress_open.
    exists (mcomp (mctx_rst ectx_hole E₂) F₂); exists v₂; split; [ | split ].
    - apply (red_rtc_in_mctx (mctx_rst ectx_hole E₂)) in Hred₂.
      rewrite mplug_mcomp; exact Hred₂.
    - apply mctxr_mctx_combo.
      * { unfold mctx_rel; simpl; split.
          + take_done; pr_assumption.
          + take refl; constructor.
        }
      * eapply mctx_rel_monotone; [ | eassumption ].
        take_done; reflexivity.
    - take_done; assumption.
  + destruct Ht as [ E₂' [ v₂ [ Hred₂ [ HE' Hv ] ] ] ].
    rewrite <- eplug_ecomp; eapply progress_step; [ apply red_shift | ].
    eexists; split.
    - etransitivity.
      * apply (red_rtc_in_mctx (mctx_rst ectx_hole E₂)) in Hred₂; exact Hred₂.
      * simpl; rewrite <- eplug_ecomp.
        econstructor 2; [ apply red_shift | reflexivity ].
    - take subst_v; apply rst_value_rel_apply; [ take_done; pr_assumption | ].
      apply combo_app; [ | take refl; constructor ].
      take rename; apply (Mk_rename mk_shift (v_lam _) (v_lam _)).
      take lam; constructor; term_simpl.
      take pctxrst; constructor; [ | take refl; constructor ].
      take pctxrst; apply pctxrst_ectx_rel.
      * take rename; apply rename_rst_ectx.
        take rename; apply rename_rst_ectx.
        take_done; pr_assumption.
      * take rename; apply rename_pure_ectx; take_done; pr_assumption.
Qed.