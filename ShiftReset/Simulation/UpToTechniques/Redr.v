(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for redr                    *)
(* (red in the paper).                                                  *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib.
Require Import ShiftReset.Simulation.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Continuity.
Require Import ShiftReset.Simulation.UpToTechniques.Monotonicity.

Instance Evolution_redr :
  redr !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take redr; take_done; apply Hpr
    | take redr; take_done; apply Hpr | ].
  intros e [ V t₁ t₁' t₂ t₂' Hred₁ Hred₂ Ht ].
  inversion Hred₁ as [ | t₀ ? Hred₀ Hrtc ]; subst; clear Hred₁.
  + eapply elem_progress_red_rtc_r; [ eassumption | ].
    eapply elem_progress_monotone; [ | | eapply (progress_rel Hpr); assumption ].
    - take_done; reflexivity.
    - take_done; reflexivity.
  + eapply progress_step; [ eassumption | ].
    exists t₂; split; [ reflexivity | ].
    take redr; econstructor; [ eassumption | eassumption | ].
    take_done; pr_assumption.
Qed.