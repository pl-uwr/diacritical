(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the monotonicity proofs                           *)
(* for the up-to techniques of Figure 1 and 2.                          *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Simulation.UpToTechniques.Core.

(** Proving the up-to techniques monotone. *)

Instance Monotone_refl : Monotone refl.
Proof.
  intros R S Hsub e; destruct 1; constructor.
Qed.

Instance Monotone_lam : Monotone lam.
Proof.
  intros R S Hsub e; destruct 1; constructor; apply Hsub; assumption.
Qed.

Instance Monotone_redr : Monotone redr.
Proof.
  intros R S Hsub e; destruct 1; econstructor.
  + eassumption.
  + eassumption.
  + apply Hsub; assumption.
Qed.

Instance Monotone_pctx : Monotone pctx.
Proof.
  intros R S Hsub e; destruct 1; constructor; apply Hsub; assumption.
Qed.

Instance Monotone_pctxrst : Monotone pctxrst.
Proof.
  intros R S Hsub e; destruct 1; constructor; apply Hsub; assumption.
Qed.

Instance Monotone_mctxpure : Monotone mctxpure.
Proof.
  intros R S Hsub e; destruct 1; constructor; apply Hsub; assumption.
Qed.

Instance Monotone_rename : Monotone rename.
Proof.
  intros R S Hsub e; destruct 1; constructor; apply Hsub; assumption.
Qed.

Instance Monotone_subst_v : Monotone subst_v.
Proof.
  intros R S Hsub e; destruct 1; constructor; apply Hsub; assumption.
Qed.