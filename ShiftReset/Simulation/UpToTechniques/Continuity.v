(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the continuity proofs for the up-to techniques    *)
(* of Figure 1 and 2 (continuity is stipulated in Definition 3.12).     *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Simulation.UpToTechniques.Core.

Import List.ListNotations.

(** Proving the up-to techniques continuous. *)

Instance Continuous_refl : Continuous refl.
Proof.
  intros R _ [ V t ]; exists nil; split; [ auto | ]; constructor.
Qed.

Instance Continuous_lam : Continuous lam.
Proof.
  intros R _ [ V t₁ t₂ Ht ]; eexists [ _ ]%list; split.
  + constructor; [ exact Ht | ]; constructor.
  + constructor; left; reflexivity.
Qed.

Instance Continuous_redr : Continuous redr.
Proof.
  intros R _ [ V t₁ t₁' t₂ t₂' Hred₁ Hred₂ Ht ]; eexists [ _ ]%list; split.
  + constructor; [ exact Ht | ]; constructor.
  + econstructor; [ eassumption | eassumption | left; reflexivity ].
Qed.

Instance Continuous_pctx : Continuous pctx.
Proof.
  intros R _ [ V E₁ E₂ t₁ t₂ HE Ht ]; eexists [ _ ; _ ]%list; split.
  + constructor; [ exact HE | ].
    constructor; [ exact Ht | ]; constructor.
  + constructor; [ left | right; left ]; reflexivity.
Qed.

Instance Continuous_pctxrst : Continuous pctxrst.
Proof.
  intros R _ [ V E₁ E₂ t₁ t₂ HE Ht ]; eexists [ _ ; _ ]%list; split.
  + constructor; [ exact HE | ].
    constructor; [ exact Ht | ]; constructor.
  + constructor; [ left | right; left ]; reflexivity.
Qed.

Instance Continuous_mctxpure : Continuous mctxpure.
Proof.
  intros R _ [ V F₁ F₂ t₁ t₂ HF Ht ]; eexists [ _ ; _ ]%list; split.
  + constructor; [ exact HF | ].
    constructor; [ exact Ht | ]; constructor.
  + constructor; [ left | right; left ]; reflexivity.
Qed.

Instance Continuous_rename : Continuous rename.
Proof.
  intros R _ [ A B f t₁ t₂ Ht ]; eexists [ _ ]%list; split.
  + constructor; [ exact Ht | ]; constructor.
  + constructor; left; reflexivity.
Qed.

Instance Continuous_subst_v : Continuous subst_v.
Proof.
  intros R _ [ V v₁ v₂ t₁ t₂ Hv Ht ]; eexists [ _ ; _ ]%list; split.
  + constructor; [ exact Hv | ].
    constructor; [ exact Ht | ]; constructor.
  + constructor; [ left | right; left ]; reflexivity.
Qed.