(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains auxiliary lemmas related to variable              *)
(* renaming and substitution, that are used in the correctness proof    *)
(* for subst_v.                                                         *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib.
Require Import ShiftReset.Simulation.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Continuity.
Require Import ShiftReset.Simulation.UpToTechniques.Monotonicity.

Require Import ShiftReset.Simulation.UpToTechniques.Combo.

Lemma rename_value {A B : Set} (f : A [→] B) (R : DiRel elem) v₁ v₂ :
  value_rel R v₁ v₂ → value_rel (rename R) (fmap f v₁) (fmap f v₂).
Proof.
  unfold value_rel; intro Hv.
  apply (Mk_rename (liftA f)) in Hv; term_simpl in Hv.
  assumption.
Qed.

Lemma rename_rst_value {A B : Set} (f : A [→] B) (R : DiRel elem) v₁ v₂ :
  rst_value_rel R v₁ v₂ → rst_value_rel (rename R) (fmap f v₁) (fmap f v₂).
Proof.
  unfold rst_value_rel; intro Hv.
  apply (Mk_rename (liftA f)) in Hv; term_simpl in Hv.
  assumption.
Qed.

Lemma rename_pure_ectx {A B : Set} (f : A [→] B) (R : DiRel elem) E₁ E₂ :
  pure_ectx_rel R E₁ E₂ → pure_ectx_rel (rename R) (fmap f E₁) (fmap f E₂).
Proof.
  unfold pure_ectx_rel; intro HE.
  apply (Mk_rename (liftA f)) in HE; term_simpl in HE.
  assumption.
Qed.

Lemma rename_rst_ectx {A B : Set} (f : A [→] B) (R : DiRel elem) E₁ E₂ :
  rst_ectx_rel R E₁ E₂ → rst_ectx_rel (rename R) (fmap f E₁) (fmap f E₂).
Proof.
  unfold rst_ectx_rel; intro HE.
  apply (Mk_rename (liftA f)) in HE; term_simpl in HE.
  assumption.
Qed.

Lemma rename_pure_mctx {A B : Set} (f : A [→] B) (R : DiRel elem) F₁ F₂ :
  pure_mctx_rel R F₁ F₂ → pure_mctx_rel (rename R) (fmap f F₁) (fmap f F₂).
Proof.
  unfold pure_mctx_rel; intro HF.
  apply (Mk_rename (liftA f)) in HF; term_simpl in HF.
  assumption.
Qed.

Lemma rename_mctx {A B : Set} (f : A [→] B) (R : DiRel elem) F₁ F₂ :
  mctx_rel R F₁ F₂ → mctx_rel (rename R) (fmap f F₁) (fmap f F₂).
Proof.
  intro HF; unfold mctx_rel in HF.
  destruct (classify_mctx F₁) as [ E₁ | F₁ E₁ ];
    destruct (classify_mctx F₂) as [ E₂ | F₂ E₂ ].
  + apply rename_pure_ectx; assumption.
  + destruct HF.
  + destruct HF.
  + destruct HF as [ HE HF ]; term_simpl.
    apply mctx_rel_snoc.
    - apply rename_rst_ectx; assumption.
    - apply rename_pure_mctx; assumption.
Qed.

Lemma rename_mctx_combo {A B : Set} (f : A [→] B) (R : DiRel elem) F₁ F₂ :
  mctx_rel (combo R) F₁ F₂ → mctx_rel (combo R) (fmap f F₁) (fmap f F₂).
Proof.
  intro HF; eapply mctx_rel_monotone; [ | apply rename_mctx; eassumption ].
  take rename; reflexivity.
Qed.

Lemma pctx_pure_ectx_rel {V : Set} (R : DiRel elem) (E₁ E₂ E₁' E₂' : ectx V) :
  pure_ectx_rel R (shift E₁) (shift E₂) →
  pure_ectx_rel R E₁' E₂' →
  pure_ectx_rel (pctx R) (ecomp E₁ E₁') (ecomp E₂ E₂').
Proof.
  intros HE HE'; unfold pure_ectx_rel; term_simpl.
  rewrite eplug_ecomp, eplug_ecomp; constructor; assumption.
Qed.

Lemma pctxrst_ectx_rel {V : Set} (R : DiRel elem) (E₁ E₂ E₁' E₂' : ectx V) :
  rst_ectx_rel R (shift E₁) (shift E₂) →
  pure_ectx_rel R E₁' E₂' →
  rst_ectx_rel (pctxrst R) (ecomp E₁ E₁') (ecomp E₂ E₂').
Proof.
  intros HE HE'; unfold rst_ectx_rel; term_simpl.
  rewrite eplug_ecomp, eplug_ecomp; constructor; assumption.
Qed.

Lemma pctx_pure_ectx_combo {V : Set}
    (R : DiRel elem) (E₁ E₂ E₁' E₂' : ectx V) :
  pure_ectx_rel (combo R) (shift E₁) (shift E₂) →
  pure_ectx_rel (combo R) E₁' E₂' →
  pure_ectx_rel (combo R) (ecomp E₁ E₁') (ecomp E₂ E₂').
Proof.
  intros HE HE'; take pctx; apply pctx_pure_ectx_rel; assumption.
Qed.

Lemma mctxpure_rst_mctx_combo {V : Set}
    (R : DiRel elem) (F₁ F₂ F₁' F₂' : mctx V) :
  pure_mctx_rel (combo R) F₁ F₂ →
  mctx_rel (combo R) (mctx_rst ectx_hole F₁') (mctx_rst ectx_hole F₂') →
  mctx_rel (combo R) (mcomp F₁ (mctx_rst ectx_hole F₁'))
                     (mcomp F₂ (mctx_rst ectx_hole F₂')).
Proof.
  intros HF HF'; unfold mctx_rel in HF'; simpl in HF'.
  destruct (classify_mctx F₁') as [ E₁ | F₁' E₁ ];
    destruct (classify_mctx F₂') as [ E₂ | F₂' E₂ ].
  + destruct HF' as [ HE HF' ].
    change (mctx_rst ectx_hole E₁) with (msnoc ectx_hole E₁).
    change (mctx_rst ectx_hole E₂) with (msnoc ectx_hole E₂).
    rewrite mcomp_msnoc, mcomp_msnoc; apply mctx_rel_snoc; [ assumption | ].
    term_simpl; assumption.
  + destruct HF' as [ HE HF' ].
    change (mctx_rst ectx_hole E₁) with (msnoc ectx_hole E₁).
    change (mctx_rst ectx_hole (msnoc F₂' E₂))
      with (msnoc (mctx_rst ectx_hole F₂') E₂).
    rewrite mcomp_msnoc, mcomp_msnoc; apply mctx_rel_snoc; [ assumption | ].
    unfold pure_mctx_rel; term_simpl; rewrite mplug_mcomp; simpl.
    take mctxpure; constructor.
    - take rename; apply rename_pure_mctx; assumption.
    - assumption.
  + destruct HF' as [ HE HF' ].
    change (mctx_rst ectx_hole (msnoc F₁' E₁))
    with (msnoc (mctx_rst ectx_hole F₁') E₁).
    change (mctx_rst ectx_hole E₂) with (msnoc ectx_hole E₂).
    rewrite mcomp_msnoc, mcomp_msnoc; apply mctx_rel_snoc; [ assumption | ].
    unfold pure_mctx_rel; term_simpl; rewrite mplug_mcomp; simpl.
    take mctxpure; constructor.
    - take rename; apply rename_pure_mctx; assumption.
    - assumption.
  + destruct HF' as [ HE HF' ].
    change (mctx_rst ectx_hole (msnoc F₁' E₁))
      with (msnoc (mctx_rst ectx_hole F₁') E₁).
    change (mctx_rst ectx_hole (msnoc F₂' E₂))
      with (msnoc (mctx_rst ectx_hole F₂') E₂).
    rewrite mcomp_msnoc, mcomp_msnoc; apply mctx_rel_snoc; [ assumption | ].
    unfold pure_mctx_rel; term_simpl; rewrite mplug_mcomp, mplug_mcomp; simpl.
    take mctxpure; constructor.
    - take rename; apply rename_pure_mctx; assumption.
    - assumption.
Qed.

Lemma mctxr_pure_mctx_combo {V : Set}
    (R : DiRel elem) (F₁ F₂ F₁' F₂' : mctx V) :
  mctx_rel (combo R) (shift F₁) (shift F₂) →
  pure_mctx_rel (combo R) F₁' F₂' →
  pure_mctx_rel (combo R) (mcomp F₁ F₁') (mcomp F₂ F₂').
Proof.
  unfold pure_mctx_rel; intros HF HF'; term_simpl.
  rewrite mplug_mcomp, mplug_mcomp.
  apply combo_mctx; assumption.
Qed.

Lemma mctxr_mctx_combo {V : Set}
    (R : DiRel elem) (F₁ F₂ F₁' F₂' : mctx V) :
  mctx_rel (combo R) F₁ F₂ →
  mctx_rel (combo R) F₁' F₂' →
  mctx_rel (combo R) (mcomp F₁ F₁') (mcomp F₂ F₂').
Proof.
  intros HF HF'; unfold mctx_rel in HF'.
  destruct (classify_mctx F₁') as [ E₁' | F₁' E₁' ];
    destruct (classify_mctx F₂') as [ E₂' | F₂' E₂' ].
  + unfold mctx_rel in HF.
    destruct (classify_mctx F₁) as [ E₁ | F₁ E₁ ];
      destruct (classify_mctx F₂) as [ E₂ | F₂ E₂ ].
    - apply pctx_pure_ectx_combo; [ | assumption ].
      take rename; apply rename_pure_ectx; assumption.
    - destruct HF.
    - destruct HF.
    - destruct HF as [ HE HF ].
      repeat rewrite mcomp_msnoc_pure.
      apply mctx_rel_snoc; [ | assumption ].
      take pctxrst; apply pctxrst_ectx_rel; [ | eassumption ].
      take rename; apply rename_rst_ectx; assumption.
  + destruct HF'.
  + destruct HF'.
  + destruct HF' as [ HE' HF' ].
    repeat rewrite mcomp_msnoc.
    apply mctx_rel_snoc; [ assumption | ].
    apply mctxr_pure_mctx_combo; [ | assumption ].
    apply rename_mctx_combo; assumption.
Qed.

Definition swap {V : Set} : inc (inc V) [→] inc (inc V) :=
  {| apply_arr := λ x,
       match x with
       | VZ        => VS VZ
       | VS VZ     => VZ
       | VS (VS y) => VS (VS y)
       end
  |}.

Lemma subst_v_value {V : Set} (v₁ v₂ : value V) (R : DiRel elem) u₁ u₂ :
  value_rel R v₁ v₂ →
  value_rel R u₁ u₂ →
  value_rel (subst_v (rename R)) (subst u₁ v₁) (subst u₂ v₂).
Proof.
  intro Hv; unfold value_rel; intro Hu.
  apply (Mk_rename swap) in Hu.
  apply (Mk_subst_v (shift v₁) (shift v₂)) in Hu.
  + term_simpl in Hu; repeat rewrite shift_subst; term_simpl.
    unfold shift in Hu; repeat rewrite map_map_comp' in Hu; assumption.
  + apply rename_value; assumption.
Qed.

Lemma subst_v_rst_value {V : Set} (v₁ v₂ : value V) (R : DiRel elem) u₁ u₂ :
  value_rel R v₁ v₂ →
  rst_value_rel R u₁ u₂ →
  rst_value_rel (subst_v (rename R)) (subst u₁ v₁) (subst u₂ v₂).
Proof.
  intro Hv; unfold rst_value_rel; intro Hu.
  apply (Mk_rename swap) in Hu.
  apply (Mk_subst_v (shift v₁) (shift v₂)) in Hu.
  + term_simpl in Hu; repeat rewrite shift_subst; term_simpl.
    unfold shift in Hu; repeat rewrite map_map_comp' in Hu; assumption.
  + apply rename_value; assumption.
Qed.

Lemma subst_v_pure_ectx {V : Set} (v₁ v₂ : value V) (R : DiRel elem) E₁ E₂ :
  value_rel R v₁ v₂ →
  pure_ectx_rel R E₁ E₂ →
  pure_ectx_rel (subst_v (rename R)) (subst E₁ v₁) (subst E₂ v₂).
Proof.
  intro Hv; unfold pure_ectx_rel; intro HE.
  apply (Mk_rename swap) in HE.
  apply (Mk_subst_v (shift v₁) (shift v₂)) in HE.
  + term_simpl in HE; repeat rewrite shift_subst; term_simpl.
    unfold shift in HE; repeat rewrite map_map_comp' in HE; assumption.
  + apply rename_value; assumption.
Qed.

Lemma subst_v_rst_ectx {V : Set} (v₁ v₂ : value V) (R : DiRel elem) E₁ E₂ :
  value_rel R v₁ v₂ →
  rst_ectx_rel R E₁ E₂ →
  rst_ectx_rel (subst_v (rename R)) (subst E₁ v₁) (subst E₂ v₂).
Proof.
  intro Hv; unfold rst_ectx_rel; intro HE.
  apply (Mk_rename swap) in HE.
  apply (Mk_subst_v (shift v₁) (shift v₂)) in HE.
  + term_simpl in HE; repeat rewrite shift_subst; term_simpl.
    unfold shift in HE; repeat rewrite map_map_comp' in HE; assumption.
  + apply rename_value; assumption.
Qed.

Lemma subst_v_pure_mctx {V : Set} (v₁ v₂ : value V) (R : DiRel elem) F₁ F₂ :
  value_rel R v₁ v₂ →
  pure_mctx_rel R F₁ F₂ →
  pure_mctx_rel (subst_v (rename R)) (subst F₁ v₁) (subst F₂ v₂).
Proof.
  intro Hv; unfold pure_mctx_rel; intro HF.
  apply (Mk_rename swap) in HF.
  apply (Mk_subst_v (shift v₁) (shift v₂)) in HF.
  + term_simpl in HF; repeat rewrite shift_subst; term_simpl.
    unfold shift in HF; repeat rewrite map_map_comp' in HF; assumption.
  + apply rename_value; assumption.
Qed.

Lemma subst_v_mctx {V : Set} (v₁ v₂ : value V) (R : DiRel elem) F₁ F₂ :
  value_rel R v₁ v₂ →
  mctx_rel R F₁ F₂ →
  mctx_rel (subst_v (rename R)) (subst F₁ v₁) (subst F₂ v₂).
Proof.
  intros Hv HF; unfold mctx_rel in HF.
  destruct (classify_mctx F₁) as [ E₁ | F₁ E₁ ];
    destruct (classify_mctx F₂) as [ E₂ | F₂ E₂ ].
  + apply subst_v_pure_ectx; assumption.
  + destruct HF.
  + destruct HF.
  + destruct HF as [ HE HF ]; term_simpl; apply mctx_rel_snoc.
    - apply subst_v_rst_ectx; assumption.
    - apply subst_v_pure_mctx; assumption.
Qed.

Lemma subst_v_value_sub (R S : DiRel elem) {V : Set} (v₁ v₂ : value V) u₁ u₂ :
  value_rel R v₁ v₂ →
  value_rel R u₁ u₂ →
  subst_v (rename R) ⊆ S → value_rel S (subst u₁ v₁) (subst u₂ v₂).
Proof.
  intros Hv Hu Hsub; apply Hsub, subst_v_value; assumption.
Qed.

Lemma subst_v_rst_value_sub (R S : DiRel elem) {V : Set}
    (v₁ v₂ : value V) u₁ u₂ :
  value_rel R v₁ v₂ →
  rst_value_rel R u₁ u₂ →
  subst_v (rename R) ⊆ S → rst_value_rel S (subst u₁ v₁) (subst u₂ v₂).
Proof.
  intros Hv Hu Hsub; apply Hsub, subst_v_rst_value; assumption.
Qed.

Lemma subst_v_mctx_sub (R S : DiRel elem) {V : Set} (v₁ v₂ : value V) F₁ F₂ :
  value_rel R v₁ v₂ →
  mctx_rel R F₁ F₂ →
  subst_v (rename R) ⊆ S → mctx_rel S (subst F₁ v₁) (subst F₂ v₂).
Proof.
  intros Hv HE Hsub; apply (mctx_rel_monotone Hsub), subst_v_mctx; assumption.
Qed.

Lemma subst_v_value_combo {V : Set} (v₁ v₂ : value V) u₁ u₂ R :
  value_rel (combo R) v₁ v₂ →
  value_rel (combo R) u₁ u₂ →
  value_rel (combo R) (subst u₁ v₁) (subst u₂ v₂).
Proof.
  intros Hv Hu; eapply subst_v_value_sub; [ eassumption | eassumption | ].
  take subst_v; take rename; reflexivity.
Qed.

Lemma subst_v_rst_value_combo {V : Set} (v₁ v₂ : value V) u₁ u₂ R :
  value_rel (combo R) v₁ v₂ →
  rst_value_rel (combo R) u₁ u₂ →
  rst_value_rel (combo R) (subst u₁ v₁) (subst u₂ v₂).
Proof.
  intros Hv Hu; eapply subst_v_rst_value_sub; [ eassumption | eassumption | ].
  take subst_v; take rename; reflexivity.
Qed.

Lemma subst_v_pure_ectx_combo {V : Set}
    (v₁ v₂ : value V) (R : DiRel elem) E₁ E₂ :
  value_rel (combo R) v₁ v₂ →
  pure_ectx_rel (combo R) E₁ E₂ →
  pure_ectx_rel (combo R) (subst E₁ v₁) (subst E₂ v₂).
Proof.
  intros Hv HE.
  take subst_v; eapply (monotone subst_v);
    [ | apply subst_v_pure_ectx; eassumption ].
  take rename; reflexivity.
Qed.

Lemma subst_v_rst_ectx_combo {V : Set}
    (v₁ v₂ : value V) (R : DiRel elem) E₁ E₂ :
  value_rel (combo R) v₁ v₂ →
  rst_ectx_rel (combo R) E₁ E₂ →
  rst_ectx_rel (combo R) (subst E₁ v₁) (subst E₂ v₂).
Proof.
  intros Hv HE.
  take subst_v; eapply (monotone subst_v);
    [ | apply subst_v_rst_ectx; eassumption ].
  take rename; reflexivity.
Qed.

Lemma subst_v_pure_mctx_combo {V : Set}
    (v₁ v₂ : value V) (R : DiRel elem) F₁ F₂ :
  value_rel (combo R) v₁ v₂ →
  pure_mctx_rel (combo R) F₁ F₂ →
  pure_mctx_rel (combo R) (subst F₁ v₁) (subst F₂ v₂).
Proof.
  intros Hv HF.
  take subst_v; eapply (monotone subst_v);
    [ | apply subst_v_pure_mctx; eassumption ].
  take rename; reflexivity.
Qed.

Lemma subst_v_mctx_combo {V : Set} (v₁ v₂ : value V) F₁ F₂ R :
  value_rel (combo R) v₁ v₂ →
  mctx_rel (combo R) F₁ F₂ →
  mctx_rel (combo R) (subst F₁ v₁) (subst F₂ v₂).
Proof.
  intros Hv HE; eapply subst_v_mctx_sub; [ eassumption | eassumption | ].
  take subst_v; take rename; reflexivity.
Qed.