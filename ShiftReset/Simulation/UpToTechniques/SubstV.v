(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for subst_v                 *)
(* (subst in the paper; commented on after Theorem 4.6).                *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib.
Require Import ShiftReset.Simulation.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Continuity.
Require Import ShiftReset.Simulation.UpToTechniques.Monotonicity.

Require Import ShiftReset.Simulation.UpToTechniques.Combo.
Require Import ShiftReset.Simulation.UpToTechniques.SubstRename.

Lemma rst_value_rel_apply (R : DiRel elem) {V : Set} (v₁ v₂ u₁ u₂ : value V) :
  rst_value_rel R v₁ v₂ →
  value_rel R u₁ u₂ →
  term_rel (subst_v R) (t_rst (t_app v₁ u₁)) (t_rst (t_app v₂ u₂)).
Proof.
  intros Hv Hu; unfold rst_value_rel in Hv.
  apply (Mk_subst_v u₁ u₂) in Hv; [ | assumption ].
  term_simpl in Hv; assumption.
Qed.

Lemma elem_progress_subst_v_open_v_step {V : Set} {v₁ v₂ : value V}
  {F₁ F₂ : mctx (inc V)} {u₁ u₂ : value (inc V)} {t₁ t₂}
  {Q S : DiRel elem} :
  value_rel (combo S) v₁ v₂ →
  mctx_rel  (combo S) F₁ F₂ →
  value_rel (combo S) u₁ u₂ →
  red     (t_app (shift v₁ : value (inc V)) (v_var VZ)) t₁ →
  red_rtc (t_app (shift v₂ : value (inc V)) (v_var VZ)) t₂ →
  term_rel (combo S) t₁ t₂ →
  elem_progress Q (combo S) (mk_elem V
    (subst (mplug F₁ (t_app (v_var VZ) u₁)) v₁)
    (subst (mplug F₂ (t_app (v_var VZ) u₂)) v₂)).
Proof.
  intros Hv HF Hu Hred₁ Hred₂ Hs.
  apply (red_subst (subst u₁ v₁)) in Hred₁; term_simpl in Hred₁.
  apply (red_rtc_subst (subst u₂ v₂)) in Hred₂; term_simpl in Hred₂.
  apply (red_in_mctx (subst F₁ v₁)) in Hred₁.
  apply (red_rtc_in_mctx (subst F₂ v₂)) in Hred₂.
  eapply progress_step.
  + term_simpl; exact Hred₁.
  + eexists; split; [ term_simpl; exact Hred₂ | ].
    apply combo_mctx.
    - apply subst_v_mctx_combo; assumption.
    - take subst_v; constructor.
      * apply subst_v_value_combo; assumption.
      * assumption.
Qed.

Lemma elem_progress_subst_v_open_v {V : Set} {v₁ v₂ : value V}
  {F₁ F₂ : mctx (inc V)} {u₁ u₂ : value (inc V)}
  (Q Q' S : DiRel elem) :
  value_rel (combo S) v₁ v₂ →
  mctx_rel  (combo S) F₁ F₂ →
  value_rel (combo S) u₁ u₂ →
  elem_progress Q' (combo S) (mk_elem (inc V)
    (t_app (shift v₁ : value (inc V)) (v_var VZ))
    (t_app (shift v₂ : value (inc V)) (v_var VZ))) →
  elem_progress Q (combo S) (mk_elem V
    (subst (mplug F₁ (t_app (v_var VZ) u₁)) v₁)
    (subst (mplug F₂ (t_app (v_var VZ) u₂)) v₂)).
Proof.
  intros Hv HF Hu Hv'; destruct v₁ as [ x | t₁ | ]; term_simpl in Hv'.
  + destruct Hv' as [ F₂' [ w₂ [ Hred₂ [ HF' Hw ] ] ] ].
    term_simpl; apply progress_open.
    apply (red_rtc_subst (subst u₂ v₂)) in Hred₂; term_simpl in Hred₂.
    apply (red_rtc_in_mctx (subst F₂ v₂)) in Hred₂.
    rewrite <- mplug_mcomp in Hred₂.
    eapply subst_v_value_combo in Hu; [ | exact Hv ].
    eapply subst_v_mctx_combo in HF; [ | exact Hv ].
    eapply subst_v_mctx_combo in HF'; [ | exact Hu ]; term_simpl in HF'.
    eapply mctxr_mctx_combo in HF'; [ | exact HF ]; term_simpl in HF'.
    eapply subst_v_value_combo in Hw; [ | exact Hu ]; term_simpl in Hw.
    eexists; eexists; split; [ exact Hred₂ | ]; split.
    - assumption.
    - assumption.
  + destruct Hv' as [ t₂' [ Hred₂ Ht' ] ].
    eapply (elem_progress_subst_v_open_v_step Hv HF Hu);
      [ | eassumption | eassumption ].
    constructor.
  + destruct Hv' as [ E₂ [ w₂ [ Hred₂ [ HE Hw ] ] ] ].
    apply (red_rtc_subst (subst u₂ v₂)) in Hred₂; term_simpl in Hred₂.
    apply (red_rtc_in_mctx (subst F₂ v₂)) in Hred₂.
    eapply elem_progress_red_rtc_r; [ term_simpl; exact Hred₂ | ].
    eapply subst_v_value_combo in Hu; [ | exact Hv ].
    eapply subst_v_rst_value_combo in Hw; [ | exact Hu ]; term_simpl in Hw.
    unfold mctx_rel in HF.
    destruct (classify_mctx F₁) as [ E₁' | F₁ E₁' ];
      destruct (classify_mctx F₂) as [ E₂' | F₂ E₂' ].
    - term_simpl; apply progress_ctrl.
      rewrite <- eplug_ecomp.
      eexists; eexists; split; [ reflexivity | ]; split; [ | assumption ].
      eapply subst_v_pure_ectx_combo in HF; [ | exact Hv ].
      eapply subst_v_pure_ectx_combo in HE; [ | exact Hu ].
      eapply pctx_pure_ectx_combo in HE;
        [ | take rename; apply rename_pure_ectx; exact HF ].
      term_simpl in HE; assumption.
    - destruct HF.
    - destruct HF.
    - destruct HF as [ HE' HF ].
      term_simpl; repeat rewrite mplug_msnoc; eapply progress_step;
        [ apply red_in_mctx, red_shift | ].
      rewrite <- eplug_ecomp; eexists; split;
        [ apply red_rtc_in_mctx; econstructor 2;
          [ apply red_shift | reflexivity ] | ].
      eapply subst_v_pure_mctx_combo in HF; [ | exact Hv ].
      take mctxpure; constructor; [ assumption | ].
      take subst_v; apply rst_value_rel_apply; [ assumption | ]; term_simpl.
      apply combo_app; [ | take refl; constructor ].
      take rename; apply (Mk_rename mk_shift (v_lam _) (v_lam _)).
      take lam; constructor; rewrite eplug_ecomp.
      eapply subst_v_rst_ectx_combo in HE'; [ | exact Hv ].
      take pctxrst; constructor;
        [ take rename; apply rename_rst_ectx, HE' | ].
      eapply subst_v_pure_ectx_combo in HE; [ | exact Hu ].
      exact HE.
Qed.

Instance Evolution_subst_v :
  subst_v !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take subst_v; take_done; apply Hpr
    | take subst_v; take_done; apply Hpr | ].
  intros e [ V v₁ v₂ t₁ t₂ Hv Ht ].
  apply (progress_rel Hpr) in Ht; simpl in Ht.
  destruct (classify t₁) as [ t₁ t₁' Hred₁ | u₁ | F₁ x u₁ | E₁ u₁ ].
  + destruct Ht as [ t₂' [ Hred₂ Ht' ] ].
    eapply progress_step; [ eapply red_subst; eassumption | ].
    exists (subst (Inc:=inc) t₂' v₂).
    split; [ apply red_rtc_subst; assumption | ].
    take subst_v; constructor; take_done; pr_assumption.
  + destruct Ht as [ u₂ [ Hred₂ Hu ] ]; simpl.
    exists (subst (Inc:=inc) u₂ v₂); split; [ apply (red_rtc_subst v₂ Hred₂) | ].
    apply (subst_v_value_sub Q); [ pr_assumption | pr_assumption | ].
    take subst_v; take rename; take_done; reflexivity.
  + destruct Ht as [ F₂ [ u₂ [ Hred₂ [ HF Hu ] ] ] ].
    apply (red_rtc_subst v₂) in Hred₂.
    destruct x as [ | x ].
    - eapply elem_progress_red_rtc_r; [ exact Hred₂ | ].
      eapply elem_progress_subst_v_open_v.
      * take_done; pr_assumption.
      * eapply mctx_rel_monotone; [ | eassumption ].
        take_done; reflexivity.
      * take_done; pr_assumption.
      * apply (progress_rel Hpr) in Hv.
        eapply elem_progress_monotone; [ reflexivity | | exact Hv ].
        take_done; reflexivity.
    - term_simpl; apply progress_open.
      exists (subst (Inc:=inc) F₂ v₂); exists (subst (Inc:=inc) u₂ v₂).
      split; [ | split ].
      * term_simpl in Hred₂; assumption.
      * apply subst_v_mctx_combo; [ take_done; pr_assumption | ].
        eapply mctx_rel_monotone; [ | eassumption ].
        take_done; reflexivity.
      * apply (subst_v_value_sub S); [ pr_assumption | pr_assumption | ].
        take subst_v; take rename; take_done; reflexivity.
  + destruct Ht as [ E₂ [ u₂ [ Hred₂ [ HE Hu ] ] ] ].
    apply (red_rtc_subst v₂) in Hred₂; term_simpl in Hred₂.
    term_simpl; apply progress_ctrl.
    eexists; eexists; split; [ exact Hred₂ | split ].
    - apply subst_v_pure_ectx_combo; take_done; pr_assumption.
    - apply subst_v_rst_value_combo; take_done; pr_assumption.
Qed.