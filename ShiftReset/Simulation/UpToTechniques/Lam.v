(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for lam.                    *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib.
Require Import ShiftReset.Simulation.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Continuity.
Require Import ShiftReset.Simulation.UpToTechniques.Monotonicity.

Instance Evolution_lam :
  lam !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take lam; take_done; apply Hpr
    | take lam; take_done; apply Hpr | ].
  intros e [ V t₁ t₂ Ht ]; simpl.
  exists (v_lam t₂); split; [ reflexivity | ].
  unfold value_rel.
  apply (combo_str_take redr); apply Mk_redr with (t₁' := t₁) (t₂' := t₂).
  + term_simpl; econstructor; [ constructor | ]; term_simpl.
    unfold subst; rewrite map_to_bind, bind_bind_comp'.
    rewrite bind_pure; [ reflexivity | ].
    intros [ | x ]; reflexivity.
  + term_simpl; econstructor; [ constructor | ]; term_simpl.
    unfold subst; rewrite map_to_bind, bind_bind_comp'.
    rewrite bind_pure; [ reflexivity | ].
    intros [ | x ]; reflexivity.
  + take_done; pr_assumption.
Qed.