(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for refl.                   *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib.
Require Import ShiftReset.Simulation.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Continuity.
Require Import ShiftReset.Simulation.UpToTechniques.Monotonicity.

Instance Evolution_refl :
  refl !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take refl; take_done; apply Hpr
    | take refl; take_done; apply Hpr | ].
  intros e [ V t ]; simpl.
  destruct (classify t) as [ t t' Hred | v | F x v | E v ].
  + exists t'; split; [ econstructor; [ eassumption | reflexivity ] | ].
    take refl; constructor.
  + exists v; split; [ reflexivity | ].
    take refl; constructor.
  + exists F; exists v; split; [ reflexivity | ].
    split; [ | take refl; constructor ].
    unfold mctx_rel; destruct (classify_mctx F); [ take refl; constructor | ].
    split; take refl; constructor.
  + exists E; exists v; split; [ reflexivity | ].
    split; take refl; constructor.
Qed.