(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file defines the up-to techniques of Figure 1 and 2.            *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib.
Require Import ShiftReset.Simulation.Core.

Require List.
Import List.ListNotations.

(** Up to reflexivity. *)

Inductive refl (R : DiRel elem) : DiRel elem :=
| Mk_refl : ∀ (V : Set) (t : term V),
    term_rel (refl R) t t.

(** Up to λ-abstraction. *)

Inductive lam (R : DiRel elem) : DiRel elem :=
| Mk_lam : ∀ (V : Set) (t₁ t₂ : term (inc V)),
    term_rel R t₁ t₂ →
    term_rel (lam R) (v_lam t₁) (v_lam t₂).

(** Up to reduction. *)

Inductive redr (R : DiRel elem) : DiRel elem :=
| Mk_redr : ∀ (V : Set) (t₁ t₁' t₂ t₂' : term V),
    red_rtc t₁ t₁' → red_rtc t₂ t₂' →
    term_rel R t₁' t₂' →
    term_rel (redr R) t₁ t₂.

(** Up to pure evaluation context. *)

Inductive pctx (R : DiRel elem) : DiRel elem :=
| Mk_pctx : ∀ (V : Set) (E₁ E₂ : ectx V) (t₁ t₂ : term V),
    pure_ectx_rel R E₁ E₂ →
    term_rel R t₁ t₂ →
    term_rel (pctx R) (eplug E₁ t₁) (eplug E₂ t₂).

Arguments Mk_pctx {R V}.

(** Up to reset pure evaluation context. *)

Inductive pctxrst (R : DiRel elem) : DiRel elem :=
| Mk_pctxrst : ∀ (V : Set) (E₁ E₂ : ectx V) (t₁ t₂ : term V),
    rst_ectx_rel R E₁ E₂ →
    term_rel R t₁ t₂ →
    term_rel (pctxrst R) (t_rst (eplug E₁ t₁)) (t_rst (eplug E₂ t₂)).

Arguments Mk_pctxrst {R V}.

(** Up to meta-context (corresponds to ectxpure in the paper). *)

Inductive mctxpure (R : DiRel elem) : DiRel elem :=
| Mk_mctxpure : ∀ (V : Set) (F₁ F₂ : mctx V) (t₁ t₂ : pure_term V),
    pure_mctx_rel R F₁ F₂ →
    term_rel R t₁ t₂ →
    term_rel (mctxpure R) (mplug F₁ t₁) (mplug F₂ t₂).

Arguments Mk_mctxpure {R V}.

(** Up to variable renaming (administrative) *)

Inductive rename (R : DiRel elem) : DiRel elem :=
| Mk_rename : ∀ (A B : Set) (f : A [→] B) (t₁ t₂ : term A),
    term_rel R t₁ t₂ →
    term_rel (rename R) (fmap f t₁) (fmap f t₂).

Arguments Mk_rename {R A B}.

(** Up to substitution. *)

Inductive subst_v (R : DiRel elem) : DiRel elem :=
| Mk_subst_v : ∀ (V : Set) (v₁ v₂ : value V) (t₁ t₂ : term (inc V)),
    value_rel R v₁ v₂ →
    term_rel R t₁ t₂ →
    term_rel (subst_v R) (subst t₁ v₁) (subst t₂ v₂).

Arguments Mk_subst_v {R V}.

Notation strong_ut := ([refl;lam;redr;rename;subst_v]%list).
Notation weak_ut := ([pctx;pctxrst;mctxpure]%list).

(** combo (defined in terms of di_combo from Diacritical/Operations.v)
    represents the closure of the set
    F = {refl, lam, subst, pctx, pctxrst, ectxpure, id, red}
    wrt iterated union and composition of functions from F
    (F^ω in Section 4.2).
*)

Notation combo      := (di_combo      strong_ut weak_ut).
Notation combo_str  := (di_combo_str  strong_ut).
Notation combo_weak := (di_combo_weak strong_ut weak_ut).
