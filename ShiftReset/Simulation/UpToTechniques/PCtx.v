(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for pctx.                   *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib.
Require Import ShiftReset.Simulation.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Continuity.
Require Import ShiftReset.Simulation.UpToTechniques.Monotonicity.

Require Import ShiftReset.Simulation.UpToTechniques.SubstV.
Require Import ShiftReset.Simulation.UpToTechniques.SubstRename.

Instance Evolution_pctx :
  pctx ↝ combo_weak & combo.
Proof.
  intros R S Hpr; split;
    [ take pctx; take_done; reflexivity
    | take pctx; take_done; apply Hpr | ].
  intros e [ V E₁ E₂ t₁ t₂ HE Ht ].
  apply (progress_rel Hpr) in Ht; simpl in Ht.
  destruct (classify t₁) as [ t₁ t₁' Hred₁ | v₁ | F₁ x v₁ | E₁' v₁ ].
  + destruct Ht as [ t₂' [ Hred₂ Ht' ] ].
    eapply progress_step; [ apply red_in_ectx; eassumption | ].
    exists (eplug E₂ t₂').
    split; [ apply red_rtc_in_ectx; assumption | ].
    take pctx; constructor; take_done; pr_assumption.
  + destruct Ht as [ v₂ [ Hred₂ Hv ] ].
    eapply elem_progress_red_rtc_r; [ apply red_rtc_in_ectx; eassumption | ].
    unfold pure_ectx_rel in HE.
    apply (Mk_subst_v v₁ v₂) in HE; [ | assumption ]; term_simpl in HE.
    apply Evolution_subst_v in Hpr; apply (progress_rel Hpr) in HE.
    eapply elem_progress_monotone; [ | reflexivity | eassumption ].
    apply (combo_str_in_combo_weak _ _).
  + destruct Ht as [ F₂ [ v₂ [ Hred₂ [ HF Hv ] ] ] ].
    change (eplug E₁) with (mplug E₁).
    rewrite <- mplug_mcomp; apply progress_open.
    exists (mcomp E₂ F₂); exists v₂; split; [ | split ].
    - rewrite mplug_mcomp; apply red_rtc_in_ectx; assumption.
    - apply mctxr_mctx_combo.
      * unfold mctx_rel; simpl; take_done; pr_assumption.
      * eapply mctx_rel_monotone; [ | eassumption ].
        take_done; reflexivity.
    - take_done; assumption.
  + destruct Ht as [ E₂' [ v₂ [ Hred₂ [ HE' Hv ] ] ] ].
    apply (red_rtc_in_ectx E₂) in Hred₂; rewrite <- eplug_ecomp in Hred₂.
    rewrite <- eplug_ecomp; apply progress_ctrl.
    eexists; eexists; split; [ exact Hred₂ | ]; split.
    - apply pctx_pure_ectx_combo; [ | take_done; pr_assumption ].
      take rename; apply rename_pure_ectx; take_done; pr_assumption.
    - take_done; pr_assumption.
Qed.