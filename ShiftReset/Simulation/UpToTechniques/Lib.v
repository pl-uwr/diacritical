(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file gathers and exports the notions related                    *)
(* to the up-to techniques for the CBV λ-calculus with shift and reset. *)
(************************************************************************)

Require Export ShiftReset.Simulation.UpToTechniques.Core.
Require Export ShiftReset.Simulation.UpToTechniques.Continuity.
Require Export ShiftReset.Simulation.UpToTechniques.Monotonicity.

Require Export ShiftReset.Simulation.UpToTechniques.Lam.
Require Export ShiftReset.Simulation.UpToTechniques.MCtxPure.
Require Export ShiftReset.Simulation.UpToTechniques.PCtx.
Require Export ShiftReset.Simulation.UpToTechniques.PCtxRst.
Require Export ShiftReset.Simulation.UpToTechniques.Redr.
Require Export ShiftReset.Simulation.UpToTechniques.Refl.
Require Export ShiftReset.Simulation.UpToTechniques.Rename.
Require Export ShiftReset.Simulation.UpToTechniques.SubstV.

Require Export ShiftReset.Simulation.UpToTechniques.Combo.