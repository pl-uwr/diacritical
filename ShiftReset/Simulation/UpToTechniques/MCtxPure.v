(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for mctxpure                *)
(* (ectxpure in the paper).                                             *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib.
Require Import ShiftReset.Simulation.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Continuity.
Require Import ShiftReset.Simulation.UpToTechniques.Monotonicity.

Require Import ShiftReset.Simulation.UpToTechniques.SubstV.
Require Import ShiftReset.Simulation.UpToTechniques.SubstRename.

Instance Evolution_mctxpure :
  mctxpure ↝ combo_weak & combo.
Proof.
  intros R S Hpr; split;
    [ take mctxpure; take_done; reflexivity
    | take mctxpure; take_done; apply Hpr | ].
  intros e [ V F₁ F₂ t₁ t₂ HF Ht ].
  apply (progress_rel Hpr) in Ht; simpl in Ht.
  destruct (classify_pure t₁) as [ t₁ t₁' Hred₁ | v₁ | F₁' x v₁ ].
  + destruct Ht as [ t₂'' [ Hred₂ Ht' ] ].
    destruct (red_rtc_pure Hred₂) as [ t₂' ? ]; subst.
    eapply progress_step; [ apply red_in_mctx; eassumption | ].
    exists (mplug F₂ t₂').
    split; [ apply red_rtc_in_mctx; assumption | ].
    take mctxpure; constructor; take_done; pr_assumption.
  + destruct Ht as [ v₂ [ Hred₂ Hv ] ].
    eapply elem_progress_red_rtc_r; [ apply red_rtc_in_mctx; eassumption | ].
    unfold pure_mctx_rel in HF.
    apply (Mk_subst_v v₁ v₂) in HF; [ | assumption ]; term_simpl in HF.
    apply Evolution_subst_v in Hpr; apply (progress_rel Hpr) in HF.
    eapply elem_progress_monotone; [ | reflexivity | eassumption ].
    apply (combo_str_in_combo_weak _ _).
  + destruct Ht as [ F₂' [ v₂ [ Hred₂ [ HF' Hv ] ] ] ].
    change (elem_progress (combo_weak R)
                          (combo S)
                          (mk_elem V
                                   (mplug F₁
                                          (mplug (mctx_rst ectx_hole F₁')
                                                 (t_app (v_var x) v₁)))
                                   (mplug F₂ t₂))).
    rewrite <- mplug_mcomp; apply progress_open.
    exists (mcomp F₂ F₂'); exists v₂; split; [ | split ].
    - rewrite mplug_mcomp; apply red_rtc_in_mctx; assumption.
    - apply red_rtc_pure in Hred₂; destruct Hred₂ as [ p Heq ].
      destruct F₂' as [ [] | [] F₂' ]; try discriminate; clear p Heq.
      apply mctxpure_rst_mctx_combo.
      * take_done; pr_assumption.
      * eapply mctx_rel_monotone; [ | eassumption ].
        take_done; reflexivity.
    - take_done; assumption.
Qed.