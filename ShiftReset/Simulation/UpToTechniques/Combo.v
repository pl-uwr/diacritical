(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains a proof of the compatibility of combo             *)
(* (iterated union and composition of functions                         *)
(* from set F of Section 4.2) wrt application.                          *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib.
Require Import ShiftReset.Simulation.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Continuity.
Require Import ShiftReset.Simulation.UpToTechniques.Monotonicity.

Lemma combo_app {V : Set} (R : DiRel elem) (t₁ t₂ s₁ s₂ : term V) :
  term_rel (combo R) t₁ t₂ →
  term_rel (combo R) s₁ s₂ →
  term_rel (combo R) (t_app t₁ s₁) (t_app t₂ s₂).
Proof.
  intros Ht Hs.
  take pctx.
  apply (Mk_pctx (ectx_app1 ectx_hole s₁) (ectx_app1 ectx_hole s₂));
    [ | assumption ].
  take pctx; simpl.
  apply (Mk_pctx (ectx_app2 _ ectx_hole) (ectx_app2 _ ectx_hole));
    [ take refl; constructor | ].
  take rename; constructor; assumption.
Qed.

Lemma combo_rst {V : Set} (R : DiRel elem) (t₁ t₂ : term V) :
  term_rel (combo R) t₁ t₂ →
  term_rel (combo R) (t_rst t₁) (t_rst t₂).
Proof.
  intros Ht; take pctxrst.
  apply (Mk_pctxrst ectx_hole ectx_hole); [ | assumption ].
  take refl; constructor.
Qed.

Lemma combo_mctx {V : Set} (R : DiRel elem) (F₁ F₂ : mctx V) (t₁ t₂ : term V) :
  mctx_rel (combo R) F₁ F₂ →
  term_rel (combo R) t₁ t₂ →
  term_rel (combo R) (mplug F₁ t₁) (mplug F₂ t₂).
Proof.
  intros HF Ht; unfold mctx_rel in HF.
  destruct (classify_mctx F₁) as [ E₁ | F₁ E₁ ];
    destruct (classify_mctx F₂) as [ E₂ | F₂ E₂ ].
  + take pctx; constructor; assumption.
  + destruct HF.
  + destruct HF.
  + destruct HF as [ HE HF ].
    rewrite mplug_msnoc, mplug_msnoc.
    take mctxpure; constructor; [ assumption | ].
    take pctxrst; constructor; assumption.
Qed.