(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains a correctness proof                               *)
(* for the rename up-to technique that is related                       *)
(* to the binding representation, and therefore                         *)
(* not mentioned in the article.                                        *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib.
Require Import ShiftReset.Simulation.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Continuity.
Require Import ShiftReset.Simulation.UpToTechniques.Monotonicity.

Require Import ShiftReset.Simulation.UpToTechniques.SubstRename.

Instance Evolution_rename :
  rename !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take rename; take_done; apply Hpr
    | take rename; take_done; apply Hpr | ].
  intros e [ A B f t₁ t₂ Ht ].
  apply (progress_rel Hpr) in Ht; simpl in Ht.
  destruct (classify t₁) as [ t₁ t₁' Hred₁ | v₁ | F₁ x v₁ | E₁ v₁ ].
  + destruct Ht as [ t₂' [ Hred₂ Ht' ] ].
    eapply progress_step; [ eapply red_fmap; eassumption | ].
    exists (fmap f t₂'); split; [ apply red_rtc_fmap; assumption | ].
    take rename; constructor; take_done; pr_assumption.
  + destruct Ht as [ v₂ [ Hred₂ Hv ] ]; simpl.
    exists (fmap f v₂); split.
    - apply (red_rtc_fmap f) in Hred₂; assumption.
    - take rename; apply rename_value; take_done; pr_assumption.
  + destruct Ht as [ F₂ [ v₂ [ Hred₂ [ HF Hv ] ] ] ].
    term_simpl; apply progress_open.
    exists (fmap f F₂); exists (fmap f v₂).
    split; [ | split ].
    - apply (red_rtc_fmap f) in Hred₂; term_simpl in Hred₂; assumption.
    - apply (@mctx_rel_monotone (rename S));
        [ take rename; take_done; reflexivity | ].
      apply rename_mctx; assumption.
    - take rename; apply rename_value; take_done; pr_assumption.
  + destruct Ht as [ E₂ [ v₂ [ Hred₂ [ HE Hv ] ] ] ].
    term_simpl; apply progress_ctrl.
    exists (fmap f E₂); exists (fmap f v₂); split; [ | split ].
    - apply (red_rtc_fmap f) in Hred₂; term_simpl in Hred₂; assumption.
    - take rename; apply rename_pure_ectx; take_done; assumption.
    - take rename; apply rename_rst_value; take_done; assumption.
Qed.