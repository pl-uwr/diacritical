(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file shows that any simulation is a precongruence               *)
(* (end of Section 4.2).                                                *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib ShiftReset.Lang.CtxApprox.
Require Import ShiftReset.Simulation.Core.
Require Import ShiftReset.Simulation.UpToTechniques.Lib.

(** The combo up-to technique is a precongruence. *)

Lemma combo_precongruence {V : Set}
      (C : ctx V) (t₁ t₂ : term V) (R : DiRel elem) :
  term_rel (combo R) t₁ t₂ →
  term_rel (combo R) (plug C t₁) (plug C t₂).
Proof.
  induction C; intro Ht; simpl.
  + assumption.
  + apply IHC; take rename; constructor; assumption.
  + apply IHC, combo_app; [ assumption | ].
    take refl; constructor.
  + apply IHC, combo_app; [ | assumption ].
    take refl; constructor.
  + apply IHC, combo_rst; assumption.
  + apply IHC; take lam; constructor; assumption.
Qed.

(** Any simulation is a precongruence. *)

Lemma precongruence {V : Set}
  (C : ctx V) (t₁ t₂ : term V) (R : DiRel elem) :
  DiSimulation R →
  term_rel R t₁ t₂ →
  term_rel (combo R) (plug C t₁) (plug C t₂).
Proof.
  intros Hsim Ht; apply combo_precongruence.
  take_done; assumption.
Qed.

(** Any simulation is sound wrt contextual approximation. *)

Lemma adequacy (t₁ t₂ : term ∅) (R : DiRel elem) :
  DiSimulation R → term_rel R t₁ t₂ → obs_approx t₁ t₂.
Proof.
  intros Hsim Ht; split.
  + intros v₁ Hred.
    remember (t_pure v₁) as s₁ eqn: Heq; revert Heq t₂ Ht.
    induction Hred as [ t₁ | t₁ t₁' t₁'' Hred Hrtc IH ]; intros Heq t₂ Ht; subst.
    - apply (progress_rel Hsim) in Ht; simpl in Ht.
      destruct Ht as [ v₂ [ Hred₂ _ ] ].
      exists v₂; assumption.
    - apply (progress_rel Hsim) in Ht.
      eapply progress_step in Ht; [ | eassumption ].
      destruct Ht as [ t₂' [ Hred₂ Ht' ] ].
      specialize (IH eq_refl t₂' Ht'); destruct IH as [ v₂ Hred' ].
      exists v₂; transitivity t₂'; assumption.
  + intros E₁ v₁ Hred.
    remember (eplug E₁ (t_app v_sft v₁)) as s₁ eqn: Heq; revert Heq t₂ Ht.
    induction Hred as [ t₁ | t₁ t₁' t₁'' Hred Hrtc IH ]; intros Heq t₂ Ht; subst.
    - apply (progress_rel Hsim) in Ht.
      apply progress_ctrl in Ht.
      destruct Ht as [ E₂ [ v₂ [ Hred₂ _ ] ] ].
      exists E₂; exists v₂; assumption.
    - apply (progress_rel Hsim) in Ht.
      eapply progress_step in Ht; [ | eassumption ].
      destruct Ht as [ t₂' [ Hred₂ Ht' ] ].
      specialize (IH eq_refl t₂' Ht'); destruct IH as [ E₂ [ v₂ Hred' ] ].
      exists E₂; exists v₂; transitivity t₂'; assumption.
Qed.

Theorem soundness {V : Set} (R : DiRel elem) (t₁ t₂ : Syntax.term V) :
  DiSimulation R → term_rel R t₁ t₂ → ctx_approx t₁ t₂.
Proof.
  intros Hsim Ht C.
  eapply adequacy; [ | apply precongruence; eassumption ].
  apply (combo_evolution _ _); assumption.
Qed.