(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file defines the notion of diacritical progress for             *)
(* the CBV λ-calculus with shift and reset (Definition 4.3).            *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import ShiftReset.Lang.Lib.

Inductive elem : Type :=
| mk_elem : ∀ V : Set, term V → term V → elem.

(** Relating terms. *)

Notation term_rel R t₁ t₂ := (R (mk_elem _ t₁ t₂)).

(** Relating values. *)

Definition value_rel (R : DiRel elem) {V : Set} (v₁ v₂ : value V) : Prop :=
  term_rel R
           (t_app (shift v₁ : value (inc V)) (v_var VZ))
           (t_app (shift v₂ : value (inc V)) (v_var VZ)).

Definition rst_value_rel (R : DiRel elem) {V : Set} (v₁ v₂ : value V) : Prop :=
  term_rel R
           (t_rst (t_app (shift v₁ : value (inc V)) (v_var VZ)))
           (t_rst (t_app (shift v₂ : value (inc V)) (v_var VZ))).

(** Relating pure evaluation contexts. *)

Definition pure_ectx_rel (R : DiRel elem) {V : Set} (E₁ E₂ : ectx V) : Prop :=
  term_rel R
           (eplug (shift E₁) (v_var VZ))
           (eplug (shift E₂) (v_var VZ)).

Definition rst_ectx_rel (R : DiRel elem) {V : Set} (E₁ E₂ : ectx V) : Prop :=
  term_rel R
           (t_rst (eplug (shift E₁) (v_var VZ)))
           (t_rst (eplug (shift E₂) (v_var VZ))).

(** Relating meta-contexts. *)

Definition pure_mctx_rel (R : DiRel elem) {V : Set} (F₁ F₂ : mctx V) : Prop :=
  term_rel R
           (mplug (shift F₁) (v_var VZ))
           (mplug (shift F₂) (v_var VZ)).

Definition mctx_rel (R : DiRel elem) {V : Set} (F₁ F₂ : mctx V) : Prop :=
  match classify_mctx F₁, classify_mctx F₂ with
  | mc_pure E₁, mc_pure E₂ => pure_ectx_rel R E₁ E₂
  | mc_rst F₁ E₁, mc_rst F₂ E₂ => rst_ectx_rel R E₁ E₂ ∧ pure_mctx_rel R F₁ F₂
  | mc_pure _, mc_rst  _ _ => False
  | mc_rst _ _, mc_pure _ => False
  end.

Lemma mctx_rel_monotone {R S : DiRel elem} {V : Set} {F₁ F₂ : mctx V} :
  R ⊆ S → mctx_rel R F₁ F₂ → mctx_rel S F₁ F₂.
Proof.
  unfold mctx_rel; intros HRS HF.
  destruct (classify_mctx F₁), (classify_mctx F₂);
    try split; try apply HRS; tauto.
Qed.

Lemma mctx_rel_snoc {R : DiRel elem} {V : Set} (F₁ F₂ : mctx V) E₁ E₂ :
  rst_ectx_rel R E₁ E₂ →
  pure_mctx_rel R F₁ F₂ →
  mctx_rel R (msnoc F₁ E₁) (msnoc F₂ E₂).
Proof.
  intros HE HF; unfold mctx_rel.
  remember (msnoc F₁ E₁) as F₁' eqn: Heq₁.
  remember (msnoc F₂ E₂) as F₂' eqn: Heq₂.
  destruct (classify_mctx F₁') as [ E₁' | F₁' E₁' ];
    [ destruct F₁; discriminate | ].
  destruct (classify_mctx F₂') as [ E₂' | F₂' E₂' ];
    [ destruct F₂; discriminate | ].
  destruct (msnoc_injective Heq₁); destruct (msnoc_injective Heq₂); subst.
  split; assumption.
Qed.

(** Instantiating the generic definition of diacritical progress
    with the concrete one, corresponding to the normal-form simulation
    in the CBV λ-calculus with shift and reset.
*)

Instance DiElemProgressCore_elem : DiElemProgressCore elem :=
  { elem_progress := λ Q S e,
    match e with
    | mk_elem V t₁ t₂ =>
      match classify t₁ with
      | c_step _ t₁' _ =>
        ∃ t₂', red_rtc t₂ t₂' ∧ term_rel S t₁' t₂'
      | c_value v₁ =>
        ∃ (v₂ : value V), red_rtc t₂ v₂ ∧ value_rel Q v₁ v₂
      | c_open F₁ x v₁ =>
        ∃ (F₂ : mctx V) (v₂ : value V),
        red_rtc t₂ (mplug F₂ (t_app (v_var x) v₂)) ∧
        mctx_rel S F₁ F₂ ∧ value_rel S v₁ v₂
      | c_ctrl E₁ v₁ =>
        ∃ (E₂ : ectx V) (v₂ : value V),
        red_rtc t₂ (eplug E₂ (t_app v_sft v₂)) ∧
        pure_ectx_rel S E₁ E₂ ∧ rst_value_rel S v₁ v₂
      end
    end
  }.

Instance DiElemProgress_elem : DiElemProgress elem.
Proof.
  split.
  + intros Q₁ Q₂ S₁ S₂ HQ HS [ V t₁ t₂ ]; simpl.
    destruct (classify t₁) as [ t₁ t₁' Hred₁ | v₁ | F₁ x v₁ | E₁ v₁ ].
    - intros [ t₂' [ Hred₂ Ht' ] ].
      exists t₂'; split; auto.
    - intros [ v₂ [ Hred₂ Hv ] ].
      exists v₂; split; [ assumption | ]; apply HQ; assumption.
    - intros [ F₂ [ v₂ [ Hred₂ [ HE Hv ] ] ] ].
      exists F₂; exists v₂; split; [ assumption | split ];
        [ | apply HS; assumption ].
      apply (mctx_rel_monotone HS); assumption.
    - intros [ E₂ [ v₂ [ Hred₂ [ HE Hv ] ] ] ].
      exists E₂; exists v₂; split; [ assumption | split ]; apply HS; assumption.
Qed.

(** Diacritical progress corresponding to normal-form simulations is
    backward-closed wrt reduction on the second argument.
*)

Lemma elem_progress_red_rtc_r {V : Set} (t₁ t₂ t₂' : term V)
      (Q S : DiRel elem) :
  red_rtc t₂ t₂' →
  elem_progress Q S (mk_elem _ t₁ t₂') →
  elem_progress Q S (mk_elem _ t₁ t₂).
Proof.
  simpl; intros Hred₂ Hpr.
  destruct (classify t₁) as [ t₁ t₁' Hred₁ | v₁ | F₁ x v₁ | E₁ v₁ ].
  + destruct Hpr as [ t₂'' [ Hred₂' Hpr ] ].
    exists t₂''; split; [ | assumption ].
    etransitivity; eassumption.
  + destruct Hpr as [ v₂ [ Hred₂' Hpr ] ].
    exists v₂; split; [ | assumption ].
    etransitivity; eassumption.
  + destruct Hpr as [ F₂ [ v₂ [ Hred₂' Hpr ] ] ].
    exists F₂; exists v₂; split; [ | assumption ].
    etransitivity; eassumption.
  + destruct Hpr as [ E₂ [ v₂ [ Hred₂' Hpr ] ] ].
    exists E₂; exists v₂; split; [ | assumption ].
    etransitivity; eassumption.
Qed.

(** In normal-form simulations, reducible, open stuck, and control-stuck terms
    are handled by the "active" component S in elem_progress Q S.
*)

Lemma progress_step {V : Set} (t₁ t₁' t₂ : term V) (Q S : DiRel elem) :
  red t₁ t₁' →
  elem_progress Q S (mk_elem _ t₁ t₂) ↔
  ∃ t₂', red_rtc t₂ t₂' ∧ term_rel S t₁' t₂'.
Proof.
  intro Hred; simpl.
  destruct (classify t₁) as [ t₁ t₁'' Hred₁ | v₁ | F₁ x v₁ | E₁ v₁ ].
  + rewrite (red_determ Hred Hred₁); reflexivity.
  + inversion Hred.
  + exfalso; apply open_stuck_is_stuck in Hred; assumption.
  + exfalso; apply ctrl_stuck_is_stuck in Hred; assumption.
Qed.

Lemma progress_open {V : Set} F₁ x (v₁ : value V) t₂ (Q S : DiRel elem) :
  elem_progress Q S (mk_elem _ (mplug F₁ (t_app (v_var x) v₁)) t₂) ↔
  ∃ (F₂ : mctx V) (v₂ : value V),
    red_rtc t₂ (mplug F₂ (t_app (v_var x) v₂)) ∧
    mctx_rel S F₁ F₂ ∧ value_rel S v₁ v₂.
Proof.
  simpl.
  remember (mplug F₁ (t_app (v_var x) v₁)) as t₁ eqn: Heq.
  destruct (classify t₁) as [ t₁ t₁' Hred | u₁ | F₁' y u₁ | E₁ u₁ ].
  + exfalso; rewrite Heq in Hred; apply open_stuck_is_stuck in Hred; assumption.
  + exfalso; destruct F₁ as [ E₁ | E₁ ? ]; destruct E₁; discriminate.
  + apply open_stuck_unique in Heq; destruct Heq as [ ? [] ]; subst.
    reflexivity.
  + exfalso; apply ctrl_stuck_is_not_open_stuck in Heq; assumption.
Qed.

Lemma progress_ctrl {V : Set} (E₁ : ectx V) (v₁ : value V) t₂
      (Q S : DiRel elem) :
  elem_progress Q S (mk_elem _ (eplug E₁ (t_app v_sft v₁)) t₂) ↔
  ∃ E₂ (v₂ : value V), red_rtc t₂ (eplug E₂ (t_app v_sft v₂)) ∧
                       pure_ectx_rel S E₁ E₂ ∧ rst_value_rel S v₁ v₂.
Proof.
  simpl.
  remember (eplug E₁ (t_app v_sft v₁)) as t₁ eqn: Heq.
  destruct (classify t₁) as [ t₁ t₁' Hred | u₁ | F₁ x u₁ | E₁' u₁ ].
  + exfalso; rewrite Heq in Hred; apply ctrl_stuck_is_stuck in Hred; assumption.
  + destruct E₁; discriminate.
  + exfalso; eapply @ctrl_stuck_is_not_open_stuck; symmetry; eassumption.
  + apply ctrl_stuck_unique in Heq; destruct Heq; subst; reflexivity.
Qed.