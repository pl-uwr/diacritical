(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file defines the notion of diacritical progress for             *)
(* the CBV λ-calculus (Definition 3.3).                                 *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import Lambda.Lang.Lib.

Inductive elem : Type :=
| mk_elem : ∀ V : Set, term V → term V → elem.

(** Relating terms. *)

Notation term_rel R t₁ t₂ := (R (mk_elem _ t₁ t₂)).

(** Relating values. *)

Definition value_rel (R : DiRel elem) {V : Set} (v₁ v₂ : value V) : Prop :=
  term_rel R
    (t_app (shift v₁ : value (inc V)) (v_var VZ))
    (t_app (shift v₂ : value (inc V)) (v_var VZ)).

(** Relating evaluation contexts. *)

Definition ectx_rel (R : DiRel elem) {V : Set} (E₁ E₂ : ectx V) : Prop :=
  term_rel R
    (eplug (shift E₁ : ectx (inc V)) (v_var VZ))
    (eplug (shift E₂ : ectx (inc V)) (v_var VZ)).

(** Instantiating the generic definition of diacritical progress
    with the concrete one, corresponding to the normal-form simulation
    in the CBV λ-calculus.
*)

Instance DiElemProgressCore_elem : DiElemProgressCore elem :=
  { elem_progress := λ Q S e,
    match e with
    | mk_elem V t₁ t₂ =>
      match classify t₁ with
      | c_step _ t₁' _ =>
        ∃ t₂', red_rtc t₂ t₂' ∧ term_rel S t₁' t₂'
      | c_value v₁ =>
        ∃ (v₂ : value V), red_rtc t₂ v₂ ∧ value_rel Q v₁ v₂
      | c_open E₁ x v₁ =>
        ∃ (E₂ : ectx V) (v₂ : value V),
        red_rtc t₂ (eplug E₂ (t_app (v_var x) v₂)) ∧
        ectx_rel S E₁ E₂ ∧ value_rel S v₁ v₂
      end
    end
  }.

Instance DiElemProgress_elem : DiElemProgress elem.
Proof.
  split.
  + intros Q₁ Q₂ S₁ S₂ HQ HS [ V t₁ t₂ ]; simpl.
    destruct (classify t₁) as [ t₁ t₁' Hred₁ | v₁ | E₁ x v₁ ].
    - intros [ t₂' [ Hred₂ Ht' ] ].
      exists t₂'; split; auto.
    - intros [ v₂ [ Hred₂ Hv ] ].
      exists v₂; split; [ assumption | ]; apply HQ; assumption.
    - intros [ E₂ [ v₂ [ Hred₂ [ HE Hv ] ] ] ].
      exists E₂; exists v₂; split; [ assumption | split ]; apply HS; assumption.
Qed.

(** Diacritical progress corresponding to normal-form simulations is
    backward-closed wrt reduction on the second argument.
*)

Lemma elem_progress_red_rtc_r {V : Set} (t₁ t₂ t₂' : term V)
  (Q S : DiRel elem) :
  red_rtc t₂ t₂' →
  elem_progress Q S (mk_elem _ t₁ t₂') →
  elem_progress Q S (mk_elem _ t₁ t₂).
Proof.
  simpl; intros Hred₂ Hpr.
  destruct (classify t₁) as [ t₁ t₁' Hred₁ | v₁ | E₁ x v₁ ].
  + destruct Hpr as [ t₂'' [ Hred₂' Hpr ] ].
    exists t₂''; split; [ | assumption ].
    etransitivity; eassumption.
  + destruct Hpr as [ v₂ [ Hred₂' Hpr ] ].
    exists v₂; split; [ | assumption ].
    etransitivity; eassumption.
  + destruct Hpr as [ E₂ [ v₂ [ Hred₂' Hpr ] ] ].
    exists E₂; exists v₂; split; [ | assumption ].
    etransitivity; eassumption.
Qed.

(** In normal-form simulations, reducible and open stuck terms
    are handled by the "active" component S in elem_progress Q S.
*)

Lemma progress_step {V : Set} (t₁ t₁' t₂ : term V) (Q S : DiRel elem) :
  red t₁ t₁' →
  elem_progress Q S (mk_elem _ t₁ t₂) ↔
  ∃ t₂', red_rtc t₂ t₂' ∧ term_rel S t₁' t₂'.
Proof.
  intro Hred; simpl.
  destruct (classify t₁) as [ t₁ t₁'' Hred₁ | v₁ | E₁ x v₁ ].
  + rewrite (red_determ Hred Hred₁); reflexivity.
  + inversion Hred.
  + exfalso; apply open_stuck_is_stuck in Hred; assumption.
Qed.

Lemma progress_open {V : Set} E₁ x (v₁ : value V) t₂ (Q S : DiRel elem) :
  elem_progress Q S (mk_elem _ (eplug E₁ (t_app (v_var x) v₁)) t₂) ↔
  ∃ (E₂ : ectx V) (v₂ : value V),
    red_rtc t₂ (eplug E₂ (t_app (v_var x) v₂)) ∧
    ectx_rel S E₁ E₂ ∧ value_rel S v₁ v₂.
Proof.
  simpl.
  remember (eplug E₁ (t_app (v_var x) v₁)) as t₁ eqn: Heq.
  destruct (classify t₁) as [ t₁ t₁' Hred | u₁ | E₁' y u₁ ].
  + exfalso; rewrite Heq in Hred; apply open_stuck_is_stuck in Hred; assumption.
  + exfalso; destruct E₁; discriminate.
  + apply open_stuck_unique in Heq; destruct Heq as [ ? [] ]; subst.
    reflexivity.
Qed.
