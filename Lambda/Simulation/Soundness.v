(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file shows that any simulation is a precongruence.              *)
(* It corresponds to Theorem 3.21 and Corollary 3.22,                   *)
(* where, as a corollary, bisimilarity ≈ is proved a congruence.        *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import Lambda.Lang.Lib Lambda.Lang.CtxApprox.
Require Import Lambda.Simulation.Core.
Require Import Lambda.Simulation.UpToTechniques.Lib.

(** The combo up-to technique is a precongruence. *)

Lemma combo_precongruence {V : Set}
  (C : ctx V) (t₁ t₂ : term V) (R : DiRel elem) :
  term_rel (combo R) t₁ t₂ →
  term_rel (combo R) (plug C t₁) (plug C t₂).
Proof.
  induction C; intro Ht; simpl.
  + assumption.
  + apply IHC; take rename; constructor; assumption.
  + apply IHC, combo_app; [ assumption | ].
    take refl; constructor.
  + apply IHC, combo_app; [ | assumption ].
    take refl; constructor.
  + apply IHC; take lam; constructor; assumption.
Qed.

(** Any simulation is a precongruence. *)

Lemma precongruence {V : Set}
  (C : ctx V) (t₁ t₂ : term V) (R : DiRel elem) :
  DiSimulation R →
  term_rel R t₁ t₂ →
  term_rel (combo R) (plug C t₁) (plug C t₂).
Proof.
  intros Hsim Ht; apply combo_precongruence.
  take_done; assumption.
Qed.

(** Any simulation is sound wrt contextual approximation. *)

Lemma adequacy {V : Set}
  (t₁ t₂ : term V) (v₁ : value V) (R : DiRel elem) :
  DiSimulation R → term_rel R t₁ t₂ →
  red_rtc t₁ v₁ →
  ∃ v₂ : value V, red_rtc t₂ v₂.
Proof.
  intros Hsim Ht Hred; revert Ht.
  remember (t_value v₁) as s₁ eqn: Heq; revert Heq t₂.
  induction Hred as [ t₁ | t₁ t₁' t₁'' Hred Hrtc IH ]; intros Heq t₂ Ht; subst.
  + apply (progress_rel Hsim) in Ht; simpl in Ht.
    destruct Ht as [ v₂ [ Hred₂ _ ] ].
    exists v₂; assumption.
  + apply (progress_rel Hsim) in Ht.
    eapply progress_step in Ht; [ | eassumption ].
    destruct Ht as [ t₂' [ Hred₂ Ht' ] ].
    specialize (IH eq_refl t₂' Ht'); destruct IH as [ v₂ Hred' ].
    exists v₂; transitivity t₂'; assumption.
Qed.

Theorem soundness {V : Set} (R : DiRel elem) (t₁ t₂ : Syntax.term V) :
  DiSimulation R → term_rel R t₁ t₂ → ctx_approx t₁ t₂.
Proof.
  intros Hsim Ht C v₁ Hred.
  eapply adequacy; [ | apply precongruence; eassumption | eassumption ].
  apply (combo_evolution _ _); assumption.
Qed.
