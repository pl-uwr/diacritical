(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file corresponds to Lemma 3.19                                  *)
(* and it contains the correctness proof for subst_v                    *)
(* (subst in the paper).                                                *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import Lambda.Lang.Lib.
Require Import Lambda.Simulation.Core.
Require Import Lambda.Simulation.UpToTechniques.Core.
Require Import Lambda.Simulation.UpToTechniques.Continuity.
Require Import Lambda.Simulation.UpToTechniques.Monotonicity.
Require Import Lambda.Simulation.UpToTechniques.Rename.

Lemma ectxr_ectx_rel {V : Set} (R : DiRel elem) (E₁ E₂ E₁' E₂' : ectx V) :
  ectx_rel R (shift E₁) (shift E₂) →
  ectx_rel R E₁' E₂' →
  ectx_rel (ectxr R) (ecomp E₁ E₁') (ecomp E₂ E₂').
Proof.
  intros HE HE'; unfold ectx_rel; term_simpl.
  rewrite eplug_ecomp, eplug_ecomp; constructor; assumption.
Qed.

Definition swap {V : Set} : inc (inc V) [→] inc (inc V) :=
  {| apply_arr := λ x,
       match x with
       | VZ        => VS VZ
       | VS VZ     => VZ
       | VS (VS y) => VS (VS y)
       end
  |}.

(** Extending subst_v to relations on values and contexts. *)

Lemma subst_v_value {V : Set} (v₁ v₂ : value V) (R : DiRel elem) u₁ u₂ :
  value_rel R v₁ v₂ →
  value_rel R u₁ u₂ →
  value_rel (subst_v (rename R)) (subst u₁ v₁) (subst u₂ v₂).
Proof.
  intro Hv; unfold value_rel; intro Hu.
  apply (Mk_rename swap) in Hu.
  apply (Mk_subst_v (shift v₁) (shift v₂)) in Hu.
  + term_simpl in Hu; repeat rewrite shift_subst; term_simpl.
    unfold shift in Hu; repeat rewrite map_map_comp' in Hu; assumption.
  + apply rename_value; assumption.
Qed.

Lemma subst_v_ectx {V : Set} (v₁ v₂ : value V) (R : DiRel elem) E₁ E₂ :
  value_rel R v₁ v₂ →
  ectx_rel R E₁ E₂ →
  ectx_rel (subst_v (rename R)) (subst E₁ v₁) (subst E₂ v₂).
Proof.
  intro Hv; unfold ectx_rel; intro HE.
  apply (Mk_rename swap) in HE.
  apply (Mk_subst_v (shift v₁) (shift v₂)) in HE.
  + term_simpl in HE; repeat rewrite shift_subst; term_simpl.
    unfold shift in HE; repeat rewrite map_map_comp' in HE; assumption.
  + apply rename_value; assumption.
Qed.

Lemma subst_v_value_sub (R S : DiRel elem) {V : Set} (v₁ v₂ : value V) u₁ u₂ :
  value_rel R v₁ v₂ →
  value_rel R u₁ u₂ →
  subst_v (rename R) ⊆ S → value_rel S (subst u₁ v₁) (subst u₂ v₂).
Proof.
  intros Hv Hu Hsub; apply Hsub, subst_v_value; assumption.
Qed.

Lemma subst_v_ectx_sub (R S : DiRel elem) {V : Set} (v₁ v₂ : value V) E₁ E₂ :
  value_rel R v₁ v₂ →
  ectx_rel R E₁ E₂ →
  subst_v (rename R) ⊆ S → ectx_rel S (subst E₁ v₁) (subst E₂ v₂).
Proof.
  intros Hv HE Hsub; apply Hsub, subst_v_ectx; assumption.
Qed.

(** The combo up-to technique is closed wrt substitution in values and contexts. *)

Lemma subst_v_value_combo {V : Set} (v₁ v₂ : value V) u₁ u₂ R :
  value_rel (combo R) v₁ v₂ →
  value_rel (combo R) u₁ u₂ →
  value_rel (combo R) (subst u₁ v₁) (subst u₂ v₂).
Proof.
  intros Hv Hu; eapply subst_v_value_sub; [ eassumption | eassumption | ].
  take subst_v; take rename; reflexivity.
Qed.

Lemma subst_v_ectx_combo {V : Set} (v₁ v₂ : value V) E₁ E₂ R :
  value_rel (combo R) v₁ v₂ →
  ectx_rel (combo R) E₁ E₂ →
  ectx_rel (combo R) (subst E₁ v₁) (subst E₂ v₂).
Proof.
  intros Hv HE; eapply subst_v_ectx_sub; [ eassumption | eassumption | ].
  take subst_v; take rename; reflexivity.
Qed.

(** Combo is closed wrt substitution in open stuck terms. *)

Lemma elem_progress_subst_v_open_v_step {V : Set} {v₁ v₂ : value V}
  {E₁ E₂ : ectx (inc V)} {u₁ u₂ : value (inc V)} {t₁ t₂}
  {Q S : DiRel elem} :
  value_rel (combo S) v₁ v₂ →
  ectx_rel  (combo S) E₁ E₂ →
  value_rel (combo S) u₁ u₂ →
  red     (t_app (shift v₁ : value (inc V)) (v_var VZ)) t₁ →
  red_rtc (t_app (shift v₂ : value (inc V)) (v_var VZ)) t₂ →
  term_rel (combo S) t₁ t₂ →
  elem_progress Q (combo S) (mk_elem V
    (subst (eplug E₁ (t_app (v_var VZ) u₁)) v₁)
    (subst (eplug E₂ (t_app (v_var VZ) u₂)) v₂)).
Proof.
  intros Hv HE Hu Hred₁ Hred₂ Hs.
  apply (red_subst (subst u₁ v₁)) in Hred₁; term_simpl in Hred₁.
  apply (red_rtc_subst (subst u₂ v₂)) in Hred₂; term_simpl in Hred₂.
  apply (red_in_ectx (subst E₁ v₁)) in Hred₁.
  apply (red_rtc_in_ectx (subst E₂ v₂)) in Hred₂.
  eapply progress_step.
  + term_simpl; exact Hred₁.
  + eexists; split; [ term_simpl; exact Hred₂ | ].
    take ectxr; constructor.
    - apply subst_v_ectx_combo; assumption.
    - take subst_v; constructor.
      * apply subst_v_value_combo; assumption.
      * assumption.
Qed.

Lemma elem_progress_subst_v_open_v {V : Set} {v₁ v₂ : value V}
  {E₁ E₂ : ectx (inc V)} {u₁ u₂ : value (inc V)}
  (Q Q' S : DiRel elem) :
  value_rel (combo S) v₁ v₂ →
  ectx_rel  (combo S) E₁ E₂ →
  value_rel (combo S) u₁ u₂ →
  elem_progress Q' (combo S) (mk_elem (inc V)
    (t_app (shift v₁ : value (inc V)) (v_var VZ))
    (t_app (shift v₂ : value (inc V)) (v_var VZ))) →
  elem_progress Q (combo S) (mk_elem V
    (subst (eplug E₁ (t_app (v_var VZ) u₁)) v₁)
    (subst (eplug E₂ (t_app (v_var VZ) u₂)) v₂)).
Proof.
  intros Hv HE Hu Hv'; destruct v₁ as [ x | t₁ ]; term_simpl in Hv'.
  + destruct Hv' as [ F₂ [ w₂ [ Hred₂ [ HF Hw ] ] ] ].
    term_simpl; apply progress_open.
    apply (red_rtc_subst (subst u₂ v₂)) in Hred₂; term_simpl in Hred₂.
    apply (red_rtc_in_ectx (subst E₂ v₂)) in Hred₂.
    rewrite <- eplug_ecomp in Hred₂.
    eapply subst_v_value_combo in Hu; [ | exact Hv ].
    eapply subst_v_ectx_combo in HE; [ | exact Hv ].
    eapply subst_v_ectx_combo in HF; [ | exact Hu ]; term_simpl in HF.
    eapply ectxr_ectx_rel in HF;
      [ | take rename; apply rename_ectx; exact HE ].
    take in HF; term_simpl in HF.
    eapply subst_v_value_combo in Hw; [ | exact Hu ]; term_simpl in Hw.
    eexists; eexists; split; [ exact Hred₂ | ]; split.
    - assumption.
    - assumption.
  + destruct Hv' as [ t₂' [ Hred₂ Ht' ] ].
    eapply (elem_progress_subst_v_open_v_step Hv HE Hu);
      [ | eassumption | eassumption ].
    constructor.
Qed.

(** Proving substᵥ correct. *)

Instance Evolution_subst_v :
  subst_v !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take subst_v; take_done; apply Hpr
    | take subst_v; take_done; apply Hpr | ].
  intros e [ V v₁ v₂ t₁ t₂ Hv Ht ].
  apply (progress_rel Hpr) in Ht; simpl in Ht.
  destruct (classify t₁) as [ t₁ t₁' Hred₁ | u₁ | E₁ x u₁ ].
  + destruct Ht as [ t₂' [ Hred₂ Ht' ] ].
    eapply progress_step; [ eapply red_subst; eassumption | ].
    exists (subst (Inc:=inc) t₂' v₂).
    split; [ apply red_rtc_subst; assumption | ].
    take subst_v; constructor; take_done; pr_assumption.
  + destruct Ht as [ u₂ [ Hred₂ Hu ] ]; simpl.
    exists (subst (Inc:=inc) u₂ v₂); split; [ apply (red_rtc_subst v₂ Hred₂) | ].
    apply (subst_v_value_sub Q); [ pr_assumption | pr_assumption | ].
    take subst_v; take rename; take_done; reflexivity.
  + destruct Ht as [ E₂ [ u₂ [ Hred₂ [ HE Hu ] ] ] ].
    apply (red_rtc_subst v₂) in Hred₂.
    destruct x as [ | x ].
    - eapply elem_progress_red_rtc_r; [ exact Hred₂ | ].
      eapply elem_progress_subst_v_open_v.
      * take_done; pr_assumption.
      * take_done; pr_assumption.
      * take_done; pr_assumption.
      * apply (progress_rel Hpr) in Hv.
        eapply elem_progress_monotone; [ reflexivity | | exact Hv ].
        take_done; reflexivity.
    - term_simpl; apply progress_open.
      exists (subst (Inc:=inc) E₂ v₂); exists (subst (Inc:=inc) u₂ v₂).
      split; [ | split ].
      * term_simpl in Hred₂; assumption.
      * apply (subst_v_ectx_sub S); [ pr_assumption | pr_assumption | ].
        take subst_v; take rename; take_done; reflexivity.
      * apply (subst_v_value_sub S); [ pr_assumption | pr_assumption | ].
        take subst_v; take rename; take_done; reflexivity.
Qed.