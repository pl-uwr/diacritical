(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file corresponds to Lemma 3.20                                  *)
(* and it contains the correctness proof for ectx.                      *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import Lambda.Lang.Lib.
Require Import Lambda.Simulation.Core.
Require Import Lambda.Simulation.UpToTechniques.Core.
Require Import Lambda.Simulation.UpToTechniques.Continuity.
Require Import Lambda.Simulation.UpToTechniques.Monotonicity.

Require Import Lambda.Simulation.UpToTechniques.Rename.
Require Import Lambda.Simulation.UpToTechniques.SubstV.

(** Proving up-to evaluation context correct. *)

Instance Evolution_ectxr :
  ectxr ↝ combo_weak & combo.
Proof.
  intros R S Hpr; split;
    [ take ectxr; take_done; reflexivity
    | take ectxr; take_done; apply Hpr | ].
  intros e [ V E₁ E₂ t₁ t₂ HE Ht ].
  apply (progress_rel Hpr) in Ht; simpl in Ht.
  destruct (classify t₁) as [ t₁ t₁' Hred₁ | v₁ | F₁ x v₁ ].
  + destruct Ht as [ t₂' [ Hred₂ Ht' ] ].
    eapply progress_step; [ apply red_in_ectx; eassumption | ].
    exists (eplug E₂ t₂').
    split; [ apply red_rtc_in_ectx; assumption | ].
    take ectxr; constructor; take_done; pr_assumption.
  + destruct Ht as [ v₂ [ Hred₂ Hv ] ].
    eapply elem_progress_red_rtc_r; [ apply red_rtc_in_ectx; eassumption | ].
    unfold ectx_rel in HE.
    apply (Mk_subst_v v₁ v₂) in HE; [ | assumption ]; term_simpl in HE.
    apply Evolution_subst_v in Hpr; apply (progress_rel Hpr) in HE.
    eapply elem_progress_monotone; [ | reflexivity | eassumption ].
    apply (combo_str_in_combo_weak _ _).
  + destruct Ht as [ F₂ [ v₂ [ Hred₂ [ HF Hv ] ] ] ].
    rewrite <- eplug_ecomp; apply progress_open.
    exists (ecomp E₂ F₂); exists v₂; split; [ | split ].
    - rewrite eplug_ecomp; apply red_rtc_in_ectx; assumption.
    - take ectxr; apply ectxr_ectx_rel; [ | take_done; pr_assumption ].
      take rename; apply rename_ectx; take_done; pr_assumption.
    - take_done; assumption.
Qed.
