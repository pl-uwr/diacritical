(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains the correctness proof for refl.                   *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import Lambda.Lang.Lib.
Require Import Lambda.Simulation.Core.
Require Import Lambda.Simulation.UpToTechniques.Core.
Require Import Lambda.Simulation.UpToTechniques.Continuity.
Require Import Lambda.Simulation.UpToTechniques.Monotonicity.

(** Proving correctness of refl. *)

Instance Evolution_refl :
  refl !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take refl; take_done; apply Hpr
    | take refl; take_done; apply Hpr | ].
  intros e [ V t ]; simpl.
  destruct (classify t) as [ t t' Hred | v | E x v ].
  + exists t'; split; [ econstructor; [ eassumption | reflexivity ] | ].
    take refl; constructor.
  + exists v; split; [ reflexivity | ].
    take refl; constructor.
  + exists E; exists v; split; [ reflexivity | ].
    split; take refl; constructor.
Qed.