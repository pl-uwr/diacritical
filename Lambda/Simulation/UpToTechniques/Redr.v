(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file corresponds to Example 3.11                                *)
(* and it contains the correctness proof for redr (red in the paper).   *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import Lambda.Lang.Lib.
Require Import Lambda.Simulation.Core.
Require Import Lambda.Simulation.UpToTechniques.Core.
Require Import Lambda.Simulation.UpToTechniques.Continuity.
Require Import Lambda.Simulation.UpToTechniques.Monotonicity.

(** Proving up to reduction correct. *)

Instance Evolution_redr :
  redr !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take redr; take_done; apply Hpr
    | take redr; take_done; apply Hpr | ].
  intros e [ V t₁ t₁' t₂ t₂' Hred₁ Hred₂ Ht ].
  inversion Hred₁ as [ | t₀ ? Hred₀ Hrtc ]; subst; clear Hred₁.
  + eapply elem_progress_red_rtc_r; [ eassumption | ].
    eapply elem_progress_monotone; [ | | eapply (progress_rel Hpr); assumption ].
    - take_done; reflexivity.
    - take_done; reflexivity.
  + eapply progress_step; [ eassumption | ].
    exists t₂; split; [ reflexivity | ].
    take redr; econstructor; [ eassumption | eassumption | ].
    take_done; pr_assumption.
Qed.