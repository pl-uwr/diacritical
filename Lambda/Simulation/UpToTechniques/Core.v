(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file defines the up-to techniques of Figure 1.                  *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import Lambda.Lang.Lib.
Require Import Lambda.Simulation.Core.

Require List.
Import List.ListNotations.

(** Up to reflexivity. *)

Inductive refl (R : DiRel elem) : DiRel elem :=
| Mk_refl : ∀ (V : Set) (t : term V),
    term_rel (refl R) t t.

(** Up to λ-abstraction. *)

Inductive lam (R : DiRel elem) : DiRel elem :=
| Mk_lam : ∀ (V : Set) (t₁ t₂ : term (inc V)),
    term_rel R t₁ t₂ →
    term_rel (lam R) (v_lam t₁) (v_lam t₂).

(** Up to reduction. *)

Inductive redr (R : DiRel elem) : DiRel elem :=
| Mk_redr : ∀ (V : Set) (t₁ t₁' t₂ t₂' : term V),
    red_rtc t₁ t₁' → red_rtc t₂ t₂' →
    term_rel R t₁' t₂' →
    term_rel (redr R) t₁ t₂.

(** Up to (evaluation) context. *)

Inductive ectxr (R : DiRel elem) : DiRel elem :=
| Mk_ectxr : ∀ (V : Set) (E₁ E₂ : ectx V) (t₁ t₂ : term V),
    ectx_rel R E₁ E₂ →
    term_rel R t₁ t₂ →
    term_rel (ectxr R) (eplug E₁ t₁) (eplug E₂ t₂).

Arguments Mk_ectxr {R V}.

(** Up to variable renaming (administrative) *)

Inductive rename (R : DiRel elem) : DiRel elem :=
| Mk_rename : ∀ (A B : Set) (f : A [→] B) (t₁ t₂ : term A),
    term_rel R t₁ t₂ →
    term_rel (rename R) (fmap f t₁) (fmap f t₂).

Arguments Mk_rename {R A B}.

(** Up to substitution. *)

Inductive subst_v (R : DiRel elem) : DiRel elem :=
| Mk_subst_v : ∀ (V : Set) (v₁ v₂ : value V) (t₁ t₂ : term (inc V)),
    value_rel R v₁ v₂ →
    term_rel R t₁ t₂ →
    term_rel (subst_v R) (subst t₁ v₁) (subst t₂ v₂).

Arguments Mk_subst_v {R V}.

Notation strong_ut := ([refl;lam;redr;rename;subst_v]%list).
Notation weak_ut := ([ectxr]%list).

(** combo (defined in terms of di_combo from Diacritical/Operations.v)
    represents the closure of the set F = {refl, lam, red, subst, ectx}
    wrt iterated union and composition of functions from F
    (F^ω in Section 3.3).
*)

Notation combo      := (di_combo      strong_ut weak_ut).
Notation combo_str  := (di_combo_str  strong_ut).
Notation combo_weak := (di_combo_weak strong_ut weak_ut).