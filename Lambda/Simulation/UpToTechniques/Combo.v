(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains a proof of the compatibility of combo             *)
(* (iterated union and composition of functions                         *)
(* from set F of Section 3.3) wrt application.                          *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import Lambda.Lang.Lib.
Require Import Lambda.Simulation.Core.
Require Import Lambda.Simulation.UpToTechniques.Core.
Require Import Lambda.Simulation.UpToTechniques.Continuity.
Require Import Lambda.Simulation.UpToTechniques.Monotonicity.

(* The combo up-to technique is compatible wrt to application. *)

Lemma combo_app {V : Set} (R : DiRel elem) (t₁ t₂ s₁ s₂ : term V) :
  term_rel (combo R) t₁ t₂ →
  term_rel (combo R) s₁ s₂ →
  term_rel (combo R) (t_app t₁ s₁) (t_app t₂ s₂).
Proof.
  intros Ht Hs.
  take ectxr.
  apply (Mk_ectxr (ectx_app1 ectx_hole s₁) (ectx_app1 ectx_hole s₂));
    [ | assumption ].
  take ectxr; simpl.
  apply (Mk_ectxr (ectx_app2 _ ectx_hole) (ectx_app2 _ ectx_hole));
    [ take refl; constructor | ].
  take rename; constructor; assumption.
Qed.
