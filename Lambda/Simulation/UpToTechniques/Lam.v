(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file corresponds to Lemma 3.18                                  *)
(* and it contains the correctness proof for lam.                       *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import Lambda.Lang.Lib.
Require Import Lambda.Simulation.Core.
Require Import Lambda.Simulation.UpToTechniques.Core.
Require Import Lambda.Simulation.UpToTechniques.Continuity.
Require Import Lambda.Simulation.UpToTechniques.Monotonicity.

(** Proving up to λ-abstraction correct. *)

Instance Evolution_lam :
  lam !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take lam; take_done; apply Hpr
    | take lam; take_done; apply Hpr | ].
  intros e [ V t₁ t₂ Ht ]; simpl.
  exists (v_lam t₂); split; [ reflexivity | ].
  unfold value_rel.
  apply (combo_str_take redr); apply Mk_redr with (t₁' := t₁) (t₂' := t₂).
  + term_simpl; econstructor; [ constructor | ]; term_simpl.
    unfold subst; rewrite map_to_bind, bind_bind_comp'.
    rewrite bind_pure; [ reflexivity | ].
    intros [ | x ]; reflexivity.
  + term_simpl; econstructor; [ constructor | ]; term_simpl.
    unfold subst; rewrite map_to_bind, bind_bind_comp'.
    rewrite bind_pure; [ reflexivity | ].
    intros [ | x ]; reflexivity.
  + take_done; pr_assumption.
Qed.