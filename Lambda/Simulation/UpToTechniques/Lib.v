(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file gathers and exports the notions related                    *)
(* to the up-to techniques for the CBV λ-calculus.                      *)
(************************************************************************)

Require Export Lambda.Simulation.UpToTechniques.Core.
Require Export Lambda.Simulation.UpToTechniques.Continuity.
Require Export Lambda.Simulation.UpToTechniques.Monotonicity.

Require Export Lambda.Simulation.UpToTechniques.Ectxr.
Require Export Lambda.Simulation.UpToTechniques.Lam.
Require Export Lambda.Simulation.UpToTechniques.Redr.
Require Export Lambda.Simulation.UpToTechniques.Refl.
Require Export Lambda.Simulation.UpToTechniques.Rename.
Require Export Lambda.Simulation.UpToTechniques.SubstV.

Require Export Lambda.Simulation.UpToTechniques.Combo.