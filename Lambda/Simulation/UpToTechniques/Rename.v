(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file contains a correctness proof                               *)
(* for the rename up-to technique that is related                       *)
(* to the binding representation, and therefore                         *)
(* not mentioned in the article.                                        *)
(************************************************************************)

Require Import Utf8.
Require Import Diacritical.Lib.
Require Import Lambda.Lang.Lib.
Require Import Lambda.Simulation.Core.
Require Import Lambda.Simulation.UpToTechniques.Core.
Require Import Lambda.Simulation.UpToTechniques.Continuity.
Require Import Lambda.Simulation.UpToTechniques.Monotonicity.

(** Extending the rename up-to technique from terms to contexts and values. *)

Lemma rename_ectx {A B : Set} (f : A [→] B) (R : DiRel elem) E₁ E₂ :
  ectx_rel R E₁ E₂ → ectx_rel (rename R) (fmap f E₁) (fmap f E₂).
Proof.
  unfold ectx_rel; intro HE.
  apply (Mk_rename (liftA f)) in HE; term_simpl in HE.
  assumption.
Qed.

Lemma rename_value {A B : Set} (f : A [→] B) (R : DiRel elem) v₁ v₂ :
  value_rel R v₁ v₂ → value_rel (rename R) (fmap f v₁) (fmap f v₂).
Proof.
  unfold value_rel; intro Hv.
  apply (Mk_rename (liftA f)) in Hv; term_simpl in Hv.
assumption.
Qed.

(** Proving correctness of rename. *)

Instance Evolution_rename :
  rename !↝ combo_str & combo.
Proof.
  intros R Q S Hpr; split;
    [ take rename; take_done; apply Hpr
    | take rename; take_done; apply Hpr | ].
  intros e [ A B f t₁ t₂ Ht ].
  apply (progress_rel Hpr) in Ht; simpl in Ht.
  destruct (classify t₁) as [ t₁ t₁' Hred₁ | v₁ | E₁ x v₁ ].
  + destruct Ht as [ t₂' [ Hred₂ Ht' ] ].
    eapply progress_step; [ eapply red_fmap; eassumption | ].
    exists (fmap f t₂'); split; [ apply red_rtc_fmap; assumption | ].
    take rename; constructor; take_done; pr_assumption.
  + destruct Ht as [ v₂ [ Hred₂ Hv ] ]; simpl.
    exists (fmap f v₂); split.
    - apply (red_rtc_fmap f) in Hred₂; assumption.
    - take rename; apply rename_value; take_done; pr_assumption.
  + destruct Ht as [ E₂ [ v₂ [ Hred₂ [ HE Hv ] ] ] ].
    term_simpl; apply progress_open.
    exists (fmap f E₂); exists (fmap f v₂).
    split; [ | split ].
    - apply (red_rtc_fmap f) in Hred₂; term_simpl in Hred₂; assumption.
    - take rename; apply rename_ectx;  take_done; pr_assumption.
    - take rename; apply rename_value; take_done; pr_assumption.
Qed.