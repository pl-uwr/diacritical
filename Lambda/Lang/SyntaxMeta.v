(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This files contains administrative lemmas related to the binding     *)
(* representation chosen for this formalization.                        *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib Binding.Set Binding.Auto.
Require Import Lambda.Lang.Syntax.

(** Administrative lemmas showing that the meta operations
    on term syntax (tmap and tbind) are well-behaved with context
    plugging and context composition.
*)

Lemma tmap_eplug {A B : Set} (f : A [→] B) (E : ectx A) (t : term A) :
  tmap f (eplug E t) = eplug (emap f E) (tmap f t).
Proof.
  revert t; induction E; simpl; intro; try (rewrite IHE); reflexivity.
Qed.

Lemma tbind_eplug {A B : Set} (f : A [⇒] B) (E : ectx A) (t : term A) :
  tbind f (eplug E t) = eplug (ebind f E) (tbind f t).
Proof.
  revert t; induction E; simpl; intro; try (rewrite IHE); reflexivity.
Qed.

Hint Rewrite -> @tmap_eplug  : term_simpl_raw.
Hint Rewrite -> @tbind_eplug : term_simpl_raw.

(** Basic properties of context plugging. *)

Lemma ecomp_hole_r {V : Set} (E : ectx V) :
  ecomp E ectx_hole = E.
Proof.
  induction E; simpl; try rewrite IHE; reflexivity.
Qed.

Hint Rewrite -> @ecomp_hole_r : term_simpl_raw.

Lemma eplug_ecomp {V : Set} (E E' : ectx V) (t : term V) :
  eplug (ecomp E E') t = eplug E (eplug E' t).
Proof.
  induction E; simpl; try (rewrite IHE); reflexivity.
Qed.

(** emap is well-behaved with context composition. *)

Lemma emap_ecomp {A B : Set} (f : A [→] B) (E E' : ectx A) :
  emap f (ecomp E E') = ecomp (emap f E) (emap f E').
Proof.
  induction E; simpl; try rewrite IHE; reflexivity.
Qed.

Hint Rewrite -> @emap_ecomp : term_simpl_raw.

Instance SetPure_value : SetPure value.
Proof. split; intros; reflexivity. Qed.

Fixpoint tmap_id {A : Set} (f : A [→] A) (t : term A) :
  arrow_eq f arrow_id → tmap f t = t
with vmap_id {A : Set} (f : A [→] A) (v : value A) :
  arrow_eq f arrow_id → vmap f v = v.
Proof.
  + auto_map_id.
  + auto_map_id.
Qed.

(** Administrative properties of the meta operations on syntax. *)

Fixpoint tmap_map_comp {A B C : Set}
         (f : B [→] C) (g : A [→] B) h (t : term A) :
  arrow_eq (arrow_comp f g) h → tmap f (tmap g t) = tmap h t
with vmap_map_comp {A B C : Set}
     (f : B [→] C) (g : A [→] B) h (v : value A) :
  arrow_eq (arrow_comp f g) h → vmap f (vmap g v) = vmap h v.
Proof.
  + auto_map_map_comp.
  + auto_map_map_comp.
Qed.

Instance FMap_vmap : FMap (@vmap).
Proof.
  split.
  + exact @vmap_id.
  + exact @vmap_map_comp.
Qed.

Instance FMap_tmap : FMap (@tmap).
Proof.
  split.
  + exact @tmap_id.
  + exact @tmap_map_comp.
Qed.

Fixpoint emap_id {A : Set}
         (f : A [→] A) (E : ectx A) :
  arrow_eq f arrow_id → emap f E = E.
Proof. auto_map_id. Qed.

Fixpoint emap_map_comp {A B C : Set}
         (f : B [→] C) (g : A [→] B) h (E : ectx A) :
  arrow_eq (arrow_comp f g) h → emap f (emap g E) = emap h E.
Proof. auto_map_map_comp. Qed.

Instance FMap_emap : FMap (@emap).
Proof.
  split.
  + exact @emap_id.
  + exact @emap_map_comp.
Qed.

Fixpoint tbind_map_comp {A B B' C : Set}
         (f : B [⇒] C) (g : A [→] B) (g' : B' [→] C) (f' : A [⇒] B')
         (t : term A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  tbind f (tmap g t) = tmap g' (tbind f' t)
with vbind_map_comp {A B B' C : Set}
     (f : B [⇒] C) (g : A [→] B) (g' : B' [→] C) (f' : A [⇒] B')
     (v : value A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  vbind f (vmap g v) = vmap g' (vbind f' v).
Proof.
  + auto_bind_map_comp.
  + auto_bind_map_comp.
Qed.

Instance BindMap_vmap_vbind : BindMap (@vmap) (@vbind).
Proof.
  split.
  + exact @vbind_map_comp.
Qed.

Instance BindMap_tmap_tbind : BindMap (@tmap) (@tbind).
Proof.
  split.
  + exact @tbind_map_comp.
Qed.

Fixpoint ebind_map_comp {A B B' C : Set}
         (f : B [⇒] C) (g : A [→] B) (g' : B' [→] C) (f' : A [⇒] B')
         (E : ectx A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  ebind f (emap g E) = emap g' (ebind f' E).
Proof. auto_bind_map_comp. Qed.

Instance BindMap_emap_ebind : BindMap (@emap) (@ebind).
Proof.
  split.
  + exact @ebind_map_comp.
Qed.

Fixpoint tbind_pure {A : Set} (f : A [⇒] A) (t : term A) :
  subst_eq f subst_pure → tbind f t = t
with vbind_pure {A : Set} (f : A [⇒] A) (v : value A) :
  subst_eq f subst_pure → vbind f v = v.
Proof.
  + auto_bind_pure.
  + auto_bind_pure.
Qed.

Fixpoint tbind_bind_comp {A B C : Set}
         (f : B [⇒] C) (g : A [⇒] B) h (t : term A) :
  subst_eq (subst_comp f g) h → tbind f (tbind g t) = tbind h t
with vbind_bind_comp {A B C : Set}
     (f : B [⇒] C) (g : A [⇒] B) h (v : value A) :
  subst_eq (subst_comp f g) h → vbind f (vbind g v) = vbind h v.
Proof.
  + auto_bind_bind_comp.
  + auto_bind_bind_comp.
Qed.

Instance Bind_vbind : Bind (@vbind).
Proof.
  split.
  + exact @vbind_pure.
  + exact @vbind_bind_comp.
Qed.

Instance Bind_tbind : Bind (@tbind).
Proof.
  split.
  + exact @tbind_pure.
  + exact @tbind_bind_comp.
Qed.

Fixpoint ebind_pure {A : Set}
         (f : A [⇒] A) (E : ectx A) :
  subst_eq f subst_pure → ebind f E = E.
Proof. auto_bind_pure. Qed.

Fixpoint ebind_bind_comp {A B C : Set}
         (f : B [⇒] C) (g : A [⇒] B) h (E : ectx A) :
  subst_eq (subst_comp f g) h → ebind f (ebind g E) = ebind h E.
Proof. auto_bind_bind_comp. Qed.

Instance Bind_ebind : Bind (@ebind).
Proof.
  split.
  + exact @ebind_pure.
  + exact @ebind_bind_comp.
Qed.