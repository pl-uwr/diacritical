(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file gathers and exports the notions related to the syntax      *)
(* and reduction semantics of the calculus.                             *)
(************************************************************************)

Require Export Binding.Lib Binding.Set.

Require Export Lambda.Lang.Classify.
Require Export Lambda.Lang.Reduction.
Require Export Lambda.Lang.ReductionProperties.
Require Export Lambda.Lang.Syntax.
Require Export Lambda.Lang.SyntaxMeta.