(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file presents an SOS formulation                                *)
(* of the reduction semantics of Section 3.1.                           *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib.
Require Import Lambda.Lang.Syntax.

(** One-step reduction relation. *)

Inductive red {V : Set} : term V → term V → Prop :=
| red_beta : ∀ (t : term (inc V)) (v : value V),
    red (t_app (v_lam t) v) (subst t v)
| red_app1 : ∀ (t t' s : term V),
    red t t' →
    red (t_app t s) (t_app t' s)
| red_app2 : ∀ (v : value V) (t t' : term V),
    red t t' →
    red (t_app v t) (t_app v t').

(** Reflexive transitive closure of red. *)

Require Import Relation_Operators.
Require Import RelationClasses.

Notation red_rtc := (@clos_refl_trans_1n _ (@red _)).

Instance Reflexive_red_rtc {V : Set} :
  Reflexive (clos_refl_trans_1n _ (@red V)).
Proof.
  constructor 1.
Qed.

Instance Transitive_red_rtc {V : Set} :
  Transitive (clos_refl_trans_1n _ (@red V)).
Proof.
  intros t₁ t₂ t₃; induction 1; [ auto | ]; intro H3.
  econstructor 2; [ eassumption | ]; auto.
Qed.
