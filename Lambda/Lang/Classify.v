(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file provides a classification of CBV λ-terms.                  *)
(************************************************************************)

Require Import Utf8.
Require Import Lambda.Lang.Syntax.
Require Import Lambda.Lang.Reduction.

(** A λ-term either makes a reduction step,
    or it is a value (canonical form),
    or it is an open stuck term.
*)

Inductive class {V : Set} : term V → Type :=
| c_step  : ∀ t t', red t t' → class t
| c_value : ∀ v : value V, class v
| c_open  : ∀ E x (v : value V), class (eplug E (t_app (v_var x) v)).

Fixpoint classify {V : Set} (t : term V) : class t.
Proof.
  destruct t as [ v | t s ].
  + apply c_value.
  + destruct (classify _ t) as [ t t' Hred | v | E x v ].
    - eapply c_step, red_app1, Hred.
    - destruct (classify _ s) as [ s s' Hred | u | E x u ].
      * eapply c_step, red_app2, Hred.
      * { destruct v as [ x | t ].
          + apply (c_open ectx_hole).
          + eapply c_step, red_beta.
        }
      * apply (c_open (ectx_app2 v E)).
    - apply (c_open (ectx_app1 E s)).
Defined.
