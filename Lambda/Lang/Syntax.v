(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file formalizes the syntax of the CBV λ-claculus                *)
(* presented in Section 3.1 of the paper.                               *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib Binding.Set.

(** CBV λ-terms *)

Inductive term (V : Set) : Type :=
| t_value : value V → term V
| t_app   : term V → term V → term V
with value (V : Set) : Type :=
| v_var    : V → value V
| v_lam    : term (inc V) → value V.

Arguments t_value {V}.
Arguments t_app   {V}.

Arguments v_var    {V}.
Arguments v_lam    {V}.

Coercion t_value : value >-> term.

Instance SetPureCore_value : SetPureCore value :=
  { set_pure := @v_var }.

Arguments SetPureCore_value {A} /.

Fixpoint tmap {A B : Set} (f : A [→] B) (t : term A) : term B :=
  match t with
  | t_value v   => vmap f v
  | t_app   t s => t_app (tmap f t) (tmap f s)
  end
with vmap {A B : Set} (f : A [→] B) (v : value A) : value B :=
  match v with
  | v_var x  => v_var (f x)
  | v_lam t  => v_lam (tmap (liftA f) t)
  end.

Instance IsMapCore_tmap : IsMapCore (@tmap).
Instance IsMapCore_vmap : IsMapCore (@vmap).

Fixpoint tbind {A B : Set} (f : A [⇒] B) (t : term A) : term B :=
  match t with
  | t_value v   => vbind f v
  | t_app   t s => t_app (tbind f t) (tbind f s)
  end
with vbind {A B : Set} (f : A [⇒] B) (v : value A) : value B :=
  match v with
  | v_var x  => f x
  | v_lam t  => v_lam (tbind (liftS f) t)
  end.

Instance IsBindCore_tbind : IsBindCore (@tbind).
Instance IsBindCore_vbind : IsBindCore (@vbind).

(** CBV evaluation contexts *)

Inductive ectx (V : Set) : Type :=
| ectx_hole : ectx V
| ectx_app1 : ectx V  → term V → ectx V
| ectx_app2 : value V → ectx V → ectx V.

Arguments ectx_hole {V}.
Arguments ectx_app1 {V}.
Arguments ectx_app2 {V}.

Fixpoint eplug {V : Set} (E : ectx V) (t : term V) : term V :=
  match E with
  | ectx_hole     => t
  | ectx_app1 E s => t_app (eplug E t) s
  | ectx_app2 v E => t_app v (eplug E t)
  end.

Fixpoint ecomp {V : Set} (E E' : ectx V) : ectx V :=
  match E with
  | ectx_hole     => E'
  | ectx_app1 E t => ectx_app1 (ecomp E E') t
  | ectx_app2 v E => ectx_app2 v (ecomp E E')
  end.

Fixpoint emap {A B : Set} (f : A [→] B) (E : ectx A) : ectx B :=
  match E with
  | ectx_hole     => ectx_hole
  | ectx_app1 E t => ectx_app1 (emap f E) (fmap f t)
  | ectx_app2 v E => ectx_app2 (fmap f v) (emap f E)
  end.

Instance IsMapCore_fmap : IsMapCore (@emap).

Fixpoint ebind {A B : Set} (f : A [⇒] B) (E : ectx A) : ectx B :=
  match E with
  | ectx_hole     => ectx_hole
  | ectx_app1 E t => ectx_app1 (ebind f E) (bind f t)
  | ectx_app2 v E => ectx_app2 (bind f v) (ebind f E)
  end.

Instance IsBindCore_ebind : IsBindCore (@ebind).