(************************************************************************)
(* Copyright 2017 Piotr Polesiuk                                        *)
(* CC BY 4.0                                                            *)
(*                                                                      *)
(* This file formalizes some basic properties of the reduction          *)
(* relation, tacitly assumed in Section 3.                              *)
(************************************************************************)

Require Import Utf8.
Require Import Binding.Lib Binding.Set.
Require Import Lambda.Lang.Syntax Lambda.Lang.SyntaxMeta.
Require Import Lambda.Lang.Reduction.

Ltac discr_red :=
  match goal with
  | H: red (t_value _) _ |- _ => inversion H
  end.

(** An open stuck term is irreducible. *)

Lemma open_stuck_is_stuck {V : Set} (E : ectx V) (x : V) (v : value V) t :
  red (eplug E (t_app (v_var x) v)) t → False.
Proof.
  revert t; induction E as [ | E IHE s | u E IHE ]; simpl; intro t.
  + inversion 1; subst; discr_red.
  + inversion 1; subst.
    - destruct E; discriminate.
    - eapply IHE; eassumption.
    - destruct E; discriminate.
  + inversion 1; subst.
    - destruct E; discriminate.
    - discr_red.
    - eapply IHE; eassumption.
Qed.

(** An open stuck term decomposes uniquely
    into an evaluation context and a potential redex.
*)

Lemma open_stuck_unique {V : Set}
    (E₁ E₂ : ectx V) (x₁ x₂ : V) (v₁ v₂ : value V) :
  eplug E₁ (t_app (v_var x₁) v₁) = eplug E₂ (t_app (v_var x₂) v₂) →
  E₁ = E₂ ∧ x₁ = x₂ ∧ v₁ = v₂.
Proof.
  revert E₂; induction E₁ as [ | E₁ IHE s₁ | u₁ E₁ IHE ]; simpl; intros E₂ Heq.
  + destruct E₂ as [ | E₂ s₂ | u₂ E₂ ]; simpl in Heq.
    - injection Heq as []; auto.
    - destruct E₂; discriminate.
    - destruct E₂; discriminate.
  + destruct E₂ as [ | E₂ s₂ | u₂ E₂ ]; simpl in Heq.
    - destruct E₁; discriminate.
    - injection Heq; intros Hs HE.
      destruct (IHE _ HE) as [ ? [] ]; subst; auto.
    - destruct E₁; discriminate.
  + destruct E₂ as [ | E₂ s₂ | u₂ E₂ ]; simpl in Heq.
    - destruct E₁; discriminate.
    - destruct E₂; discriminate.
    - injection Heq; intros HE Hu.
      destruct (IHE _ HE) as [ ? [] ]; subst; auto.
Qed.

(** One-step reduction relation is deterministic. *)

Lemma red_determ {V : Set} {t t₁ t₂ : term V} :
  red t t₁ → red t t₂ → t₁ = t₂.
Proof.
  intro Hred₁; revert t₂.
  induction Hred₁ as [ t v | t t' s Hred₁ IH | v t t' Hred₁ IH ]; intro t₂.
  + inversion 1; try discr_red.
    reflexivity.
  + inversion 1; subst; try discr_red.
    erewrite IH; [ reflexivity | ]; assumption.
  + inversion 1; subst; try discr_red.
    erewrite IH; [ reflexivity | ]; assumption.
Qed.

(** One-step reduction relation is compatible wrt evaluation contexts. *)

Lemma red_in_ectx {V : Set} (E : ectx V) {t t' : term V} :
  red t t' → red (eplug E t) (eplug E t').
Proof.
  intro Hred; induction E as [ | E IHE s | v E IHE ]; simpl.
  + assumption.
  + apply red_app1; assumption.
  + apply red_app2; assumption.
Qed.

(** And so is its relexive-transitive closure. *)

Lemma red_rtc_in_ectx {V : Set} (E : ectx V) {t t' : term V} :
  red_rtc t t' → red_rtc (eplug E t) (eplug E t').
Proof.
  induction 1; [ reflexivity | ].
  econstructor 2; [ | eassumption ].
  apply red_in_ectx; assumption.
Qed.

(** One-step reduction relation is compatible wrt substitution. *)

Lemma red_fmap {A B : Set} (f : A [→] B) {t t' : term A} :
  red t t' → red (fmap f t) (fmap f t').
Proof.
  induction 1 as [ t v | t t' s Hred IH | v t t' Hred IH ]; term_simpl.
  + apply red_beta.
  + apply red_app1, IH.
  + apply red_app2, IH.
Qed.

Lemma red_bind {A B : Set} (f : A [⇒] B) {t t' : term A} :
  red t t' → red (bind f t) (bind f t').
Proof.
  induction 1 as [ t v | t t' s Hred IH | v t t' Hred IH ]; term_simpl.
  + apply red_beta.
  + apply red_app1, IH.
  + apply red_app2, IH.
Qed.

Lemma red_subst {V : Set} (v : value V) {t t' : term (inc V)} :
  red t t' → red (subst t v) (subst t' v).
Proof.
  apply red_bind.
Qed.

(** And so is its relexive-transitive closure. *)

Lemma red_rtc_fmap {A B : Set} (f : A [→] B) {t t' : term A} :
  red_rtc t t' → red_rtc (fmap f t) (fmap f t').
Proof.
  induction 1; [ reflexivity | ].
  econstructor 2; [ | eassumption ].
  apply red_fmap; assumption.
Qed.

Lemma red_rtc_bind {A B : Set} (f : A [⇒] B) {t t' : term A} :
  red_rtc t t' → red_rtc (bind f t) (bind f t').
Proof.
  induction 1; [ reflexivity | ].
  econstructor 2; [ | eassumption ].
  apply red_bind; assumption.
Qed.

Lemma red_rtc_subst {V : Set} (v : value V) {t t' : term (inc V)} :
  red_rtc t t' → red_rtc (subst t v) (subst t' v).
Proof.
  apply red_rtc_bind.
Qed.
