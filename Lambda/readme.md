## An extensional normal-form bisimilarity for the pure CBV λ-calculus

This directory contains a formalization of Sections 3.1–3.3, and it is
structured as follows:

* The [Lang](Lang/) directory contains a formalization of the
  calculus (Section 3.1), using the library [Binding](../Binding/).

* The [Simulation](Simulation/) directory contains a formalization of
  simulations for the pure lambda calculus (Section 3.2 and 3.3),
  using the library [Diacritical](../Diacritical/).
